// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  hostName: 'https://api.c050ygx6-obeikanin2-d1-public.model-t.cc.commerce.ondemand.com/',
  headerEndpoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=headerpage',
  logoUrl: 'medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aDUyL2gxNi84Nzk2NzE3MjUyNjM4L3NpdGUtbG9nby5wbmd8MTJiMzZkYjBlZDNkMTVjMWMwOGMyMTU0NTlkZGI3NjE3NGU3MGFkZmVhNjg2M2VkY2E2ZGRkODA5MDZmNjMyOQ',
  homePageEndPoint: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=homepage',
  oAuthAPI: 'authorizationserver/oauth/token',
  registerUserAPI: 'osanedcommercewebservices/v2/osaned/users',
  footerEndPoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=footerPage',
  tokenEndpoint: 'authorizationserver/oauth/token',
  loginEndpoint: 'osanedcommercewebservices/v2/osaned/users/'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
