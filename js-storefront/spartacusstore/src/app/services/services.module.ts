import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { CommonUtilityService } from './common-utility-service';
import { HomePageService } from './home-page.service';
import { HomeResolverService } from './home-resolver.service';
import { RegisterUserService } from './register-user.service';
import { LoginService } from './login.service'

@NgModule({
    imports: [
        CommonModule,
        HttpClientModule
    ],
    exports: [],
    declarations: [],
    providers: [
        CommonUtilityService,
        HomePageService,
        HomeResolverService,
        RegisterUserService,
        LoginService
    ],
})
export class ServicesModule { }
