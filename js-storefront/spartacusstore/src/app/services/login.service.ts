import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
@Injectable()

export class LoginService {

    private userContextData = new BehaviorSubject<UserContext>(
        new UserContext('', ''));
      userContext = this.userContextData.asObservable();
    
    constructor(
        private http: HttpClient,
        private utilityService: CommonUtilityService
      ) {}

    changeUserContext(data: UserContext) {
    this.userContextData.next(data);
    }

  

  userLogin(req) {

     const request  = new HttpParams()
        .set('client_id', 'trusted_client')
        .set('client_secret', 'secret')
        .set('grant_type', 'client_credentials')
        .set('scope', 'extended')
        .set('username', req.username)
        .set('password', req.password);

     let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/x-www-form-urlencoded')
        .set('Accept-Charset', 'UTF-8'); 

     let options = {
         headers: httpHeaders
    };
    let tokenUrl = environment.tokenEndpoint

    return this.utilityService.postRequest(tokenUrl, request, options)
  }

  fetchUserDetails = (token, username) => {

    let httpHeaders = new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer${token}`)

    let options = {
        headers: httpHeaders
    };
    let loginUrl = environment.loginEndpoint + username + '?'

    return this.utilityService.getRequest(loginUrl, options)
  }
}

export class UserContext {
    constructor(public displayUID: string, public displayName: string) {
      this.displayUID = displayUID;
      this.displayName = displayName;
    }
  }
