import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'cx-header-nav-molecule',
  templateUrl: './header-nav-molecule.component.html',
  styleUrls: ['./header-nav-molecule.component.css']
})
export class HeaderNavMoleculeComponent implements OnInit {

  @Input() headerNavLinks: String[]
  @Input() logoUrl: String

  ngOnInit() {
    console.log(this.headerNavLinks)
    console.log(this.logoUrl)
  }

  closeNav = () => {
    document.getElementById("mySidenav").style.width = "0";
  }

  openNav = () => {
    document.getElementById("mySidenav").style.width = "150px";
  }
}
