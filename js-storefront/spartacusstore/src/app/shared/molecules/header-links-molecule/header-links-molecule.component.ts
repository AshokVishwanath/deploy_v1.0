import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../../../services/login.service'

@Component({
  selector: 'app-cx-header-links-molecule',
  templateUrl: './header-links-molecule.component.html',
  styleUrls: ['./header-links-molecule.component.scss']
})
export class HeaderLinksMoleculeComponent implements OnInit {

  @Input() headerLinks: string[];
  displayName: any = '';
  isLoggedIn: any

  constructor(private loginService: LoginService) {
    this.isLoggedIn = false
  }

  ngOnInit() {
    this.loginService.userContext.subscribe(context => {
      this.displayName = context.displayName;
      this.isLoggedIn = context.displayName ? true : false
    });
  }

}
