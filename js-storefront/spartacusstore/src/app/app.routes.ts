import { Routes } from '@angular/router';

import { HomeOrganismComponent } from './shared/organisms/home-organism/home-organism.component';
import { NotFoundOrganismComponent } from './shared/organisms/not-found-organism/not-found-organism.component';
import { HomeResolverService } from './services/home-resolver.service';
import { RegisterMoleculeComponent } from './shared/molecules/register-molecule/register-molecule.component';
import { LoginOrganismComponent } from './shared/organisms/login-organism/login-organism.component'

export const routes: Routes = [{
    path: '',
    component: HomeOrganismComponent,
    data: [{
      pageName: 'Home Page',
    }],
    resolve: {
      homeData: HomeResolverService
    }
  },
  {
    path: 'register',
    component: RegisterMoleculeComponent
  },
  {
    path: 'login',
    component: LoginOrganismComponent,
    data: [{
      pageName: 'Login Page',
    }],
  },
  {
    path: 'not-found',
    component: NotFoundOrganismComponent,
    pathMatch: 'full',
    data: [{
      pageName: 'Not Found',
    }]
  },
  {
    path: '**',
    redirectTo: 'not-found',
    data: [{
      pageName: 'Not Found',
    }]
  },
  {
    path: 'products',
    component: LoginOrganismComponent,
    pathMatch: 'full',
    data: [{
      pageName: 'Products Listing',
    }]
  }];
