import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule, BrowserTransferStateModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { CookieModule } from 'ngx-cookie';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { ServicesModule } from './services/services.module';
import { AppComponent } from './app.component';
import { B2cStorefrontModule } from '@spartacus/storefront';
import { translations, translationChunksConfig } from '@spartacus/assets';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule.withServerTransition({appId: 'serverApp'}),
    B2cStorefrontModule.withConfig({
      backend: {
      occ: {baseUrl: 'https://api.c050ygx6-obeikanin2-s1-public.model-t.cc.commerce.ondemand.com',
      prefix: '/rest/v2/'
      }
      },
      context: {
      baseSite: ['electronics-spa']
      },
      i18n: {
      resources: translations,
      chunks: translationChunksConfig,
      fallbackLang: 'en'
      },
      features: {
      level: '1.2'
      }
      }),
    HttpClientModule,
    BrowserTransferStateModule,
    BrowserAnimationsModule,
    SharedModule,
    ServicesModule,
    AppRoutingModule,
    CookieModule.forRoot()
  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
