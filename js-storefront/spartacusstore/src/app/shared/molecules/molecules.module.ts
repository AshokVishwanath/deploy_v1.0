import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../../material.module';
import { ServicesModule } from '../../services/services.module';
import { DirectivesModule } from '../directives/directives.module';
import { PipesModule } from '../pipes/pipes.module';
import { HeaderLinksMoleculeComponent } from './header-links-molecule/header-links-molecule.component';
import { HeaderNavMoleculeComponent } from './header-nav-molecule/header-nav-molecule.component';
import { CarousalMoleculeComponent } from './carousal-molecule/carousal-molecule.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LogoMoleculeComponent } from './logo-molecule/logo-molecule.component';
import { ContactUsMoleculeComponent } from './contact-us-molecule/contact-us-molecule.component';
import { ClientLogoComponent } from './client-logo-molecule/client-logo.component';
import { SanedSectionMoleculeComponent } from './saned-section-molecule/saned-section-molecule.component';
import { RegisterMoleculeComponent } from './register-molecule/register-molecule.component';
import { SignUpComponent } from './sign-up-molecule/sign-up.component';
import { FooterComponent } from './footer-molecule/footer.component';
import { LoginFormMoleculeComponent } from './login-form-molecule/login-form-molecule.component';
import { AppRoutingModule } from '../../app-routing.module';

const components = [
    HeaderLinksMoleculeComponent,
    HeaderNavMoleculeComponent,
    CarousalMoleculeComponent,
    LogoMoleculeComponent,
    ContactUsMoleculeComponent,
    ClientLogoComponent,
    SanedSectionMoleculeComponent,
    RegisterMoleculeComponent,
    SignUpComponent,
    FooterComponent,
    LoginFormMoleculeComponent
];

@NgModule({
  imports: [
ServicesModule,
    DirectivesModule,
    PipesModule,
    NgbModule,
    CommonModule,
    RouterModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    AppRoutingModule
  ],
  exports: [...components],
  declarations: [...components],
  providers: [],
})

export class MoleculesModule {}
