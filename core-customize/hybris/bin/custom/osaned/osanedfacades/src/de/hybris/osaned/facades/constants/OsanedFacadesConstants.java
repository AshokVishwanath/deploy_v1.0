/*
 * Copyright (c) 2019 SAP SE or an SAP affiliate company. All rights reserved.
 */
package de.hybris.osaned.facades.constants;

/**
 * Global class for all OsanedFacades constants.
 */
public class OsanedFacadesConstants extends GeneratedOsanedFacadesConstants
{
	public static final String EXTENSIONNAME = "osanedfacades";

	private OsanedFacadesConstants()
	{
		//empty
	}
}
