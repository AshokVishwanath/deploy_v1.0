(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"], {
  /***/
  "./$$_lazy_route_resource lazy recursive":
  /*!******************************************************!*\
    !*** ./$$_lazy_route_resource lazy namespace object ***!
    \******************************************************/

  /*! no static exports found */

  /***/
  function $$_lazy_route_resourceLazyRecursive(module, exports) {
    function webpackEmptyAsyncContext(req) {
      // Here Promise.resolve().then() is used instead of new Promise() to prevent
      // uncaught exception popping up in devtools
      return Promise.resolve().then(function () {
        var e = new Error("Cannot find module '" + req + "'");
        e.code = 'MODULE_NOT_FOUND';
        throw e;
      });
    }

    webpackEmptyAsyncContext.keys = function () {
      return [];
    };

    webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
    module.exports = webpackEmptyAsyncContext;
    webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
  /*!**************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppAppComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = " <!-- Header Section Start -->\r\n <app-header-organism></app-header-organism>\r\n  <!-- Header Section End -->\r\n\r\n  <!-- Body Content start -->\r\n<router-outlet></router-outlet>\r\n <!-- Body Content end -->\r\n\r\n <!-- Footer Section Start -->\r\n <app-footer-organism></app-footer-organism>\r\n <!-- Footer Section End -->\r\n\r\n\r\n\r\n\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.html":
  /*!***************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.html ***!
    \***************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesCarousalMoleculeCarousalMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div id=\"carouselExampleCaptions\" class=\"carousel slide\" data-ride=\"carousel\">\r\n    <!-- <ol class=\"carousel-indicators\">\r\n      <li data-target=\"#carouselExampleCaptions\" data-slide-to=\"0\" class=\"active\"></li>\r\n      <li data-target=\"#carouselExampleCaptions\" data-slide-to=\"1\"></li>\r\n      <li data-target=\"#carouselExampleCaptions\" data-slide-to=\"2\"></li>\r\n    </ol> -->\r\n    <ol class=\"carousel-indicators\">\r\n      <li data-target=\"#carouselExampleCaptions\" *ngFor=\"let x of homeContent;let i = index\" [attr.data-slide-to]=\"i\" ngClass=\"i == 0 ? 'active' : ''\"></li>\r\n    </ol>\r\n    <div class=\"carousel-inner\">\r\n      <div *ngFor=\"let content of homeContent; let i = index\" class=\"carousel-item\" [ngClass]=\"{'active': i === 0 }\" >\r\n        <img [src]=\"hostName + content.media.url\" class=\"d-block w-100 img-fluid\" [alt]=\"content.media.code\">\r\n      </div>\r\n    </div>\r\n    <a class=\"carousel-control-prev\" href=\"#carouselExampleCaptions\" role=\"button\" data-slide=\"prev\">\r\n      <span class=\"carousel-control-prev-icon\" aria-hidden=\"true\"></span>\r\n      <span class=\"sr-only\">Previous</span>\r\n    </a>\r\n    <a class=\"carousel-control-next\" href=\"#carouselExampleCaptions\" role=\"button\" data-slide=\"next\">\r\n      <span class=\"carousel-control-next-icon\" aria-hidden=\"true\"></span>\r\n      <span class=\"sr-only\">Next</span>\r\n    </a>\r\n  </div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/client-logo-molecule/client-logo.component.html":
  /*!************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/client-logo-molecule/client-logo.component.html ***!
    \************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesClientLogoMoleculeClientLogoComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class=\"container\">\r\n    <div class=\"logo-section mb-30 clearfix\">\r\n      <h2 class=\"title text-center mt-30\">OUR VALUABLE CLIENTS</h2>\r\n      <div class=\"row clearfix client-logos col-12 mt-50\">\r\n        <div *ngFor=\"let logo of clientLogos\" class=\"col-xs-4 col-md-2 mx-auto\">\r\n          <img  class=\"img-fluid mx-auto img-grayscale\" src= \"{{hostName +logo.media.url}}\" alt=\"{{logo.name}}\" />\r\n      </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.html":
  /*!*******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.html ***!
    \*******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesContactUsMoleculeContactUsMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card class=\"container\">\r\n  <form class=\"contact-us-form\" [formGroup]=\"userDetailsForm\" (ngSubmit)=\"onSubmitUserDetails(userDetailsForm.value)\">\r\n    <mat-card-title class=\"title\">CONTACT US </mat-card-title>\r\n    <mat-form-field class=\"name-field float-md-left\" appearance=\"standard\">\r\n      <mat-label>Name</mat-label>\r\n      <input matInput placeholder=\"\" value=\"\" formControlName=\"name\" required autocomplete=\"off\" />\r\n      <mat-error *ngFor=\"let validation of validationMessages.name\">\r\n        <mat-error class=\"error-message\" *ngIf=\"\r\n            userDetailsForm.get('name').hasError(validation.type) &&\r\n            (userDetailsForm.get('name').dirty ||\r\n              userDetailsForm.get('name').touched)\r\n          \">{{ validation.message }}</mat-error>\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"email-field float-md-right\" appearance=\"standard\">      \r\n      <mat-label>Email</mat-label>\r\n      <input matInput placeholder=\"\" value=\"\" formControlName=\"email\" required autocomplete=\"off\" />\r\n      <mat-error *ngFor=\"let validation of validationMessages.email\">\r\n        <mat-error class=\"error-message\" *ngIf=\"\r\n            userDetailsForm.get('email').hasError(validation.type) &&\r\n            (userDetailsForm.get('email').dirty ||\r\n              userDetailsForm.get('email').touched)\r\n          \">{{ validation.message }}</mat-error>\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <mat-form-field class=\"comment-field\" appearance=\"standard\">\r\n      <mat-label>Message</mat-label>\r\n      <textarea matInput   placeholder=\"\" formControlName=\"message\"\r\n        required></textarea>\r\n      <mat-error *ngFor=\"let validation of validationMessages.message\">\r\n        <mat-error class=\"error-message\" *ngIf=\"\r\n              userDetailsForm.get('message').hasError(validation.type) &&\r\n              (userDetailsForm.get('message').dirty ||\r\n                userDetailsForm.get('message').touched)\r\n            \">{{ validation.message }}</mat-error>\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <button mat-raised-button [disabled]=\"!userDetailsForm.valid\" class=\"submit-btn\">\r\n      Submit\r\n    </button>\r\n  </form>\r\n</mat-card>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/footer-molecule/footer.component.html":
  /*!**************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/footer-molecule/footer.component.html ***!
    \**************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesFooterMoleculeFooterComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<footer class=\"secondary-bg footer-container color-scheme-light\">\r\n  <div class=\"container main-footer\">\r\n    <aside class=\"col-xs-12 footer-sidebar widget-area\" role=\"complementary\">\r\n      <div class=\"clearfix visible-lg-block\"></div>\r\n      <div class=\"footer-column footer-column-2 col-md-3 col-sm-6\" *ngFor=\"let columns of footerColumnSlot\">\r\n        <div id=\"text-18\" class=\"footer-widget  footer-widget-collapse widget_text\">\r\n          <h5 class=\"widget-title primary-color\">{{columns.name}}</h5>\r\n          <div class=\"textwidget\">\r\n            <ul class=\"menu list-unstyled\" *ngFor=\"let linkData of columns.components.component\">\r\n              <li><a href=\"{{linkData.url}}\">{{linkData.linkName}}</a></li>\r\n            </ul>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </aside>\r\n  </div>\r\n  <div class=\"copyrights-wrapper text-center\">\r\n    <div class=\"container\">\r\n      <div class=\"min-footer\">\r\n        <div class=\"copyright-text text-center\">{{copyRightSlot.content}} </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.html":
  /*!***********************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.html ***!
    \***********************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesHeaderLinksMoleculeHeaderLinksMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"container-fluid\">\r\n  <div class=\"top_header\">\r\n    <ul class=\"top_section_left list-unstyled no-margin\">\r\n      <li class=\"text-white pr-20\">\r\n        <span class=\"top-icon primary-color\">\r\n          <svg class=\"bi bi-phone\" width=\"1em\" height=\"1em\" viewBox=\"0 0 20 20\" fill=\"currentColor\"\r\n            xmlns=\"http://www.w3.org/2000/svg\">\r\n            <path fill-rule=\"evenodd\"\r\n              d=\"M13 3H7a1 1 0 00-1 1v11a1 1 0 001 1h6a1 1 0 001-1V4a1 1 0 00-1-1zM7 2a2 2 0 00-2 2v11a2 2 0 002 2h6a2 2 0 002-2V4a2 2 0 00-2-2H7z\"\r\n              clip-rule=\"evenodd\"></path>\r\n            <path fill-rule=\"evenodd\" d=\"M10 15a1 1 0 100-2 1 1 0 000 2z\" clip-rule=\"evenodd\"></path>\r\n          </svg>\r\n        </span> {{headerLinks !== undefined ? headerLinks[0].content : ''}}</li>\r\n      <li class=\"text-white\">\r\n        <span class=\"top-icon primary-color\">\r\n          <svg class=\"bi bi-envelope-fill\" width=\"1em\" height=\"1em\" viewBox=\"0 0 20 20\" fill=\"currentColor\"\r\n            xmlns=\"http://www.w3.org/2000/svg\">\r\n            <path\r\n              d=\"M2.05 5.555L10 10.414l7.95-4.859A2 2 0 0016 4H4a2 2 0 00-1.95 1.555zM18 6.697l-5.875 3.59L18 13.743V6.697zm-.168 8.108l-6.675-3.926-1.157.707-1.157-.707-6.675 3.926A2 2 0 004 16h12a2 2 0 001.832-1.195zM2 13.743l5.875-3.456L2 6.697v7.046z\">\r\n            </path>\r\n          </svg>\r\n        </span>\r\n        {{headerLinks !== undefined ? headerLinks[1].content : ''}}</li>\r\n    </ul>\r\n    <ul class=\"top_section_right list-unstyled no-margin no-padding\">\r\n      <li *ngIf=\"!isLoggedIn\"><a [routerLink]=\"'/login'\" class=\"text-uppercase pr-20\">Login</a></li>\r\n      <li *ngIf=\"isLoggedIn\"><a [routerLink]=\"'/'\" class=\"text-uppercase pr-20\">{{displayName}}</a></li>\r\n      <li *ngIf=\"!isLoggedIn\"><a class=\"text-uppercase\" routerLink=\"/register\">Register</a></li>\r\n    </ul>\r\n  </div>\r\n</section>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.html":
  /*!*******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.html ***!
    \*******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesHeaderNavMoleculeHeaderNavMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"container-fluid nav-container\">\r\n  <app-logo-molecule [logoUrl]=\"logoUrl\" displayType=\"logo-container\"></app-logo-molecule>\r\n  <nav>\r\n    <ul>\r\n      <li *ngFor=\"let link of headerNavLinks\"><a [href]=\"link.url\" alt=\"\">{{link.linkName}}</a></li>\r\n    </ul>\r\n  </nav>\r\n</section>\r\n<section class=\"mobile-nav-container\">\r\n    <div id=\"mySidenav\" class=\"mobile-sidenav\">\r\n      <a href=\"javascript:void(0)\" class=\"closebtn\"  (click)=\"closeNav()\">&times;</a>\r\n      <ul>\r\n        <li  *ngFor=\"let link of headerNavLinks\"><a [href]=\"link.url\" alt=\"\">{{link.linkName}}</a></li>\r\n      </ul>\r\n    </div>\r\n    <span class=\"hamburger\" style=\"font-size:30px;cursor:pointer\"  (click)=\"openNav()\">&#9776;</span>\r\n    <app-logo-molecule [logoUrl]=\"logoUrl\" displayType=\"mobile-logo-container\"></app-logo-molecule>\r\n</section>\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.html":
  /*!*******************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.html ***!
    \*******************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesLoginFormMoleculeLoginFormMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card class=\"login-container\">\r\n  <mat-card-title class=\"title\"><h2>Login </h2></mat-card-title>\r\n  <mat-error class=\"error-msg\" *ngIf=\"loginError\">Login Failed. Please try again</mat-error>\r\n  <form class=\"login-us-form\" [formGroup]=\"userLoginForm\" (ngSubmit)=\"onSubmitLoginDetails(userLoginForm.value)\">\r\n    <mat-form-field class=\"email-field\" appearance=\"standard\">\r\n      <mat-label>Email</mat-label>\r\n      <input matInput placeholder=\"\" value=\"\" formControlName=\"email\" required autocomplete=\"off\" />\r\n      <mat-error *ngFor=\"let validation of validationMessages.email\">\r\n          <mat-error class=\"error-message\" *ngIf=\"\r\n          userLoginForm.get('email').hasError(validation.type) &&\r\n              (userLoginForm.get('email').dirty ||\r\n              userLoginForm.get('email').touched)\">{{ validation.message }}</mat-error>\r\n        </mat-error>\r\n    </mat-form-field>\r\n  \r\n    <mat-form-field class=\"password-field \" appearance=\"standard\">      \r\n      <mat-label>Password</mat-label>\r\n      <input type=\"password\" matInput placeholder=\"\" value=\"\" formControlName=\"password\" required autocomplete=\"off\" />\r\n      <mat-error *ngFor=\"let validation of validationMessages.password\">\r\n        <mat-error class=\"error-message\" *ngIf=\"\r\n        userLoginForm.get('password').hasError(validation.type) &&\r\n            (userLoginForm.get('password').dirty ||\r\n            userLoginForm.get('password').touched)\">{{ validation.message }}</mat-error>\r\n      </mat-error>\r\n    </mat-form-field>\r\n    <div class=\"forgot-pwd\"><a href=\"/\">Forgot Password?</a></div>\r\n    <button mat-raised-button [disabled]=\"!userLoginForm.valid\" class=\"submit-btn\">\r\n      Sign In\r\n    </button>\r\n    <div class=\"register-link\"><p>Don't have an account</p></div>\r\n    <button mat-raised-button class=\"register-btn\" routerLink = '/register'>\r\n      Register\r\n    </button>\r\n  </form>\r\n  </mat-card>\r\n  ";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/logo-molecule/logo-molecule.component.html":
  /*!*******************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/logo-molecule/logo-molecule.component.html ***!
    \*******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesLogoMoleculeLogoMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<div class={{displayType}}>\r\n    <a class=\"logo-img\" routerLink=\"/\" title=\"\" *ngIf=\"logoUrl !== undefined\">\r\n        <img class=\"img-fluid\" [src]=\"hostName + logoUrl\" alt=\"logo\">\r\n    </a>\r\n</div>\r\n\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/register-molecule/register-molecule.component.html":
  /*!***************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/register-molecule/register-molecule.component.html ***!
    \***************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesRegisterMoleculeRegisterMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\r\n<div class=\"col-lg-12 register-title\">\r\n    <nav>\r\n        <span><a routerLink='/'>Home</a></span>\r\n    </nav>\r\n    <h1>Create an Account</h1>\r\n</div>\r\n\r\n\r\n<div class=\"col-md-6 reg-form-wrapper\">\r\n    <form [formGroup]=\"registerForm\" (ngSubmit)=\"registerUser(registerForm.value)\" autocomplete=\"off\">\r\n        <p>\r\n            <mat-form-field appearance=\"standard\">\r\n                <mat-label>Name</mat-label>\r\n                <input matInput placeholder=\"Name\" formControlName=\"name\" required >\r\n                <mat-error *ngFor=\"let validation of validationMessages.name\">\r\n                    <mat-error class=\"error-message\" *ngIf=\"\r\n                    registerForm.get('name').hasError(validation.type) &&\r\n                        (registerForm.get('name').dirty ||\r\n                        registerForm.get('name').touched)\r\n                      \">{{ validation.message }}</mat-error>\r\n                  </mat-error>\r\n            </mat-form-field>\r\n        </p>\r\n        <p>\r\n            <mat-form-field appearance=\"standard\">\r\n                <mat-label>Email</mat-label>\r\n                <input matInput placeholder=\"Email\" formControlName=\"email\" required >\r\n                <mat-error *ngFor=\"let validation of validationMessages.email\">\r\n                    <mat-error class=\"error-message\" *ngIf=\"\r\n                    registerForm.get('email').hasError(validation.type) &&\r\n                        (registerForm.get('email').dirty ||\r\n                        registerForm.get('email').touched)\r\n                      \">{{ validation.message }}</mat-error>\r\n                  </mat-error>\r\n            </mat-form-field>\r\n        </p>\r\n        <p>\r\n            <mat-form-field appearance=\"standard\">\r\n                <mat-label>Mobile</mat-label>\r\n                <input matInput placeholder=\"Mobile\" formControlName=\"mobile\" required >\r\n                <mat-error *ngFor=\"let validation of validationMessages.mobile\">\r\n                    <mat-error class=\"error-message\" *ngIf=\"\r\n                    registerForm.get('mobile').hasError(validation.type) &&\r\n                        (registerForm.get('mobile').dirty ||\r\n                        registerForm.get('mobile').touched)\r\n                      \">{{ validation.message }}</mat-error>\r\n                  </mat-error>\r\n            </mat-form-field>\r\n        </p>\r\n        <p>\r\n            <mat-form-field appearance=\"standard\">\r\n                <mat-label>Company</mat-label>\r\n                <input matInput placeholder=\"Company\" formControlName=\"company\" required >\r\n                <mat-error *ngFor=\"let validation of validationMessages.company\">\r\n                    <mat-error class=\"error-message\" *ngIf=\"\r\n                    registerForm.get('company').hasError(validation.type) &&\r\n                        (registerForm.get('company').dirty ||\r\n                        registerForm.get('company').touched)\r\n                      \">{{ validation.message }}</mat-error>\r\n                  </mat-error>\r\n            </mat-form-field>\r\n        </p>\r\n        <p>\r\n            <mat-form-field appearance=\"standard\">\r\n                <mat-label>Select a size range</mat-label>\r\n                <mat-select disableRipple formControlName=\"size\" required>\r\n                  <mat-option value=\"1-25\">1-25</mat-option>\r\n                  <mat-option value=\"25-50\">25-50</mat-option>\r\n                  <mat-option value=\"50-100\">50-100</mat-option>\r\n                  <mat-option value=\"100-150\">100-150</mat-option>\r\n                  <mat-option value=\"150-200\">150-200</mat-option>\r\n                  <mat-option value=\"200-300\">200-300</mat-option>\r\n                </mat-select>\r\n            </mat-form-field>\r\n        </p>\r\n        <p>\r\n            <mat-form-field appearance=\"standard\">\r\n                <mat-label>Commercial Registration</mat-label>\r\n                <input matInput placeholder=\"Commercial Registration\" formControlName=\"commercial\" required>\r\n                <mat-error *ngFor=\"let validation of validationMessages.commercial\">\r\n                    <mat-error class=\"error-message\" *ngIf=\"\r\n                    registerForm.get('commercial').hasError(validation.type) &&\r\n                        (registerForm.get('commercial').dirty ||\r\n                        registerForm.get('commercial').touched)\r\n                      \">{{ validation.message }}</mat-error>\r\n                  </mat-error>\r\n            </mat-form-field>\r\n        </p>\r\n        <p>\r\n            <button  mat-raised-button [disabled]=\"!registerForm.valid\"  class=\"reg-btn\" >Register</button>\r\n        </p>\r\n        <a class=\"btn-link\" routerLink=\"/login\">I already have an account. Sign In</a>\r\n    </form>\r\n\r\n\r\n</div>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.html":
  /*!*************************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.html ***!
    \*************************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesSanedSectionMoleculeSanedSectionMoleculeComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<mat-card class=\"col-12 saned-section-wrapper\">\r\n    <mat-card-title class=\"title text-center\">SANED SOLUTIONS </mat-card-title>\r\n    <ul class=\"domains col-12 mt-50\">\r\n        <li *ngFor=\"let img of images\" class=\"col-sm-6 col-md-3 text-center\">\r\n            <a href=\"/\">\r\n                <img src=\"{{hostName +img.media.url}}\">\r\n                <p>{{img.headline}}</p>\r\n            </a>\r\n        </li>\r\n    </ul>\r\n\r\n    <div *ngFor=\"let logo of clientLogos\" class=\"col-xs-4 col-md-2 mx-auto\">\r\n        <img  class=\"img-fluid mx-auto img-grayscale\" src= \"{{hostName +logo.media.url}}\" alt=\"{{logo.name}}\" />\r\n    </div>\r\n</mat-card>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/sign-up-molecule/sign-up.component.html":
  /*!****************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/sign-up-molecule/sign-up.component.html ***!
    \****************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedMoleculesSignUpMoleculeSignUpComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<section class=\"news-letter-section\">\r\n  <form class=\"example-form\">\r\n    <mat-card-title class=\"title\">SINGUP FOR NEWS LETTER </mat-card-title>\r\n    <div class=\"row\">\r\n      <div class=\"sign-up-input-field col-12 mx-auto mt-50\">\r\n        <input id=\"email\" type=\"email\" class=\"validate\">\r\n        <button class=\"signup-btn\" type=\"submit\" name=\"action\">Submit\r\n        </button>\r\n      </div>\r\n    </div>\r\n  </form>\r\n  </section>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/footer-organism/footer-organism.component.html":
  /*!***********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/footer-organism/footer-organism.component.html ***!
    \***********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedOrganismsFooterOrganismFooterOrganismComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "  <app-cx-footer [footerColumnSlot]=\"footerColumnSlot\" [copyRightSlot]=\"this.copyRightSlot\"></app-cx-footer>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/header-organism/header-organism.component.html":
  /*!***********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/header-organism/header-organism.component.html ***!
    \***********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedOrganismsHeaderOrganismHeaderOrganismComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<header class=\"container-fluid\">\r\n  <app-cx-header-links-molecule [headerLinks]=\"headerLinks\"></app-cx-header-links-molecule>\r\n  <cx-header-nav-molecule [headerNavLinks]=\"headerNavLinks\" [logoUrl]=\"logoUrl\"></cx-header-nav-molecule>\r\n</header>\r\n\r\n";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/home-organism/home-organism.component.html":
  /*!*******************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/home-organism/home-organism.component.html ***!
    \*******************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedOrganismsHomeOrganismHomeOrganismComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<cx-carousal-molecule [homeContent]=\"homeContent\"></cx-carousal-molecule>\r\n<app-saned-section-molecule></app-saned-section-molecule>\r\n<app-cx-client-logo></app-cx-client-logo>\r\n<app-cx-sign-up></app-cx-sign-up>\r\n<app-contact-us-molecule></app-contact-us-molecule>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/login-organism/login-organism.component.html":
  /*!*********************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/login-organism/login-organism.component.html ***!
    \*********************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedOrganismsLoginOrganismLoginOrganismComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<app-login-form-molecule></app-login-form-molecule>";
    /***/
  },

  /***/
  "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/not-found-organism/not-found-organism.component.html":
  /*!*****************************************************************************************************************************!*\
    !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/not-found-organism/not-found-organism.component.html ***!
    \*****************************************************************************************************************************/

  /*! exports provided: default */

  /***/
  function node_modulesRawLoaderDistCjsJsSrcAppSharedOrganismsNotFoundOrganismNotFoundOrganismComponentHtml(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "<p>\r\n  not-found-organism works!\r\n</p>\r\n";
    /***/
  },

  /***/
  "./node_modules/tslib/tslib.es6.js":
  /*!*****************************************!*\
    !*** ./node_modules/tslib/tslib.es6.js ***!
    \*****************************************/

  /*! exports provided: __extends, __assign, __rest, __decorate, __param, __metadata, __awaiter, __generator, __exportStar, __values, __read, __spread, __spreadArrays, __await, __asyncGenerator, __asyncDelegator, __asyncValues, __makeTemplateObject, __importStar, __importDefault */

  /***/
  function node_modulesTslibTslibEs6Js(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__extends", function () {
      return __extends;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__assign", function () {
      return _assign;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__rest", function () {
      return __rest;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__decorate", function () {
      return __decorate;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__param", function () {
      return __param;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__metadata", function () {
      return __metadata;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__awaiter", function () {
      return __awaiter;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__generator", function () {
      return __generator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__exportStar", function () {
      return __exportStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__values", function () {
      return __values;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__read", function () {
      return __read;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spread", function () {
      return __spread;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__spreadArrays", function () {
      return __spreadArrays;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__await", function () {
      return __await;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncGenerator", function () {
      return __asyncGenerator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncDelegator", function () {
      return __asyncDelegator;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__asyncValues", function () {
      return __asyncValues;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__makeTemplateObject", function () {
      return __makeTemplateObject;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importStar", function () {
      return __importStar;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "__importDefault", function () {
      return __importDefault;
    });
    /*! *****************************************************************************
    Copyright (c) Microsoft Corporation. All rights reserved.
    Licensed under the Apache License, Version 2.0 (the "License"); you may not use
    this file except in compliance with the License. You may obtain a copy of the
    License at http://www.apache.org/licenses/LICENSE-2.0
    
    THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
    KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
    WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
    MERCHANTABLITY OR NON-INFRINGEMENT.
    
    See the Apache Version 2.0 License for specific language governing permissions
    and limitations under the License.
    ***************************************************************************** */

    /* global Reflect, Promise */


    var _extendStatics = function extendStatics(d, b) {
      _extendStatics = Object.setPrototypeOf || {
        __proto__: []
      } instanceof Array && function (d, b) {
        d.__proto__ = b;
      } || function (d, b) {
        for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
      };

      return _extendStatics(d, b);
    };

    function __extends(d, b) {
      _extendStatics(d, b);

      function __() {
        this.constructor = d;
      }

      d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    }

    var _assign = function __assign() {
      _assign = Object.assign || function __assign(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
          s = arguments[i];

          for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p)) t[p] = s[p];
        }

        return t;
      };

      return _assign.apply(this, arguments);
    };

    function __rest(s, e) {
      var t = {};

      for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0) t[p] = s[p];

      if (s != null && typeof Object.getOwnPropertySymbols === "function") for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
        if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i])) t[p[i]] = s[p[i]];
      }
      return t;
    }

    function __decorate(decorators, target, key, desc) {
      var c = arguments.length,
          r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc,
          d;
      if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
      return c > 3 && r && Object.defineProperty(target, key, r), r;
    }

    function __param(paramIndex, decorator) {
      return function (target, key) {
        decorator(target, key, paramIndex);
      };
    }

    function __metadata(metadataKey, metadataValue) {
      if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(metadataKey, metadataValue);
    }

    function __awaiter(thisArg, _arguments, P, generator) {
      return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) {
          try {
            step(generator.next(value));
          } catch (e) {
            reject(e);
          }
        }

        function rejected(value) {
          try {
            step(generator["throw"](value));
          } catch (e) {
            reject(e);
          }
        }

        function step(result) {
          result.done ? resolve(result.value) : new P(function (resolve) {
            resolve(result.value);
          }).then(fulfilled, rejected);
        }

        step((generator = generator.apply(thisArg, _arguments || [])).next());
      });
    }

    function __generator(thisArg, body) {
      var _ = {
        label: 0,
        sent: function sent() {
          if (t[0] & 1) throw t[1];
          return t[1];
        },
        trys: [],
        ops: []
      },
          f,
          y,
          t,
          g;
      return g = {
        next: verb(0),
        "throw": verb(1),
        "return": verb(2)
      }, typeof Symbol === "function" && (g[Symbol.iterator] = function () {
        return this;
      }), g;

      function verb(n) {
        return function (v) {
          return step([n, v]);
        };
      }

      function step(op) {
        if (f) throw new TypeError("Generator is already executing.");

        while (_) try {
          if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
          if (y = 0, t) op = [op[0] & 2, t.value];

          switch (op[0]) {
            case 0:
            case 1:
              t = op;
              break;

            case 4:
              _.label++;
              return {
                value: op[1],
                done: false
              };

            case 5:
              _.label++;
              y = op[1];
              op = [0];
              continue;

            case 7:
              op = _.ops.pop();

              _.trys.pop();

              continue;

            default:
              if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) {
                _ = 0;
                continue;
              }

              if (op[0] === 3 && (!t || op[1] > t[0] && op[1] < t[3])) {
                _.label = op[1];
                break;
              }

              if (op[0] === 6 && _.label < t[1]) {
                _.label = t[1];
                t = op;
                break;
              }

              if (t && _.label < t[2]) {
                _.label = t[2];

                _.ops.push(op);

                break;
              }

              if (t[2]) _.ops.pop();

              _.trys.pop();

              continue;
          }

          op = body.call(thisArg, _);
        } catch (e) {
          op = [6, e];
          y = 0;
        } finally {
          f = t = 0;
        }

        if (op[0] & 5) throw op[1];
        return {
          value: op[0] ? op[1] : void 0,
          done: true
        };
      }
    }

    function __exportStar(m, exports) {
      for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
    }

    function __values(o) {
      var m = typeof Symbol === "function" && o[Symbol.iterator],
          i = 0;
      if (m) return m.call(o);
      return {
        next: function next() {
          if (o && i >= o.length) o = void 0;
          return {
            value: o && o[i++],
            done: !o
          };
        }
      };
    }

    function __read(o, n) {
      var m = typeof Symbol === "function" && o[Symbol.iterator];
      if (!m) return o;
      var i = m.call(o),
          r,
          ar = [],
          e;

      try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
      } catch (error) {
        e = {
          error: error
        };
      } finally {
        try {
          if (r && !r.done && (m = i["return"])) m.call(i);
        } finally {
          if (e) throw e.error;
        }
      }

      return ar;
    }

    function __spread() {
      for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));

      return ar;
    }

    function __spreadArrays() {
      for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;

      for (var r = Array(s), k = 0, i = 0; i < il; i++) for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++) r[k] = a[j];

      return r;
    }

    ;

    function __await(v) {
      return this instanceof __await ? (this.v = v, this) : new __await(v);
    }

    function __asyncGenerator(thisArg, _arguments, generator) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var g = generator.apply(thisArg, _arguments || []),
          i,
          q = [];
      return i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i;

      function verb(n) {
        if (g[n]) i[n] = function (v) {
          return new Promise(function (a, b) {
            q.push([n, v, a, b]) > 1 || resume(n, v);
          });
        };
      }

      function resume(n, v) {
        try {
          step(g[n](v));
        } catch (e) {
          settle(q[0][3], e);
        }
      }

      function step(r) {
        r.value instanceof __await ? Promise.resolve(r.value.v).then(fulfill, reject) : settle(q[0][2], r);
      }

      function fulfill(value) {
        resume("next", value);
      }

      function reject(value) {
        resume("throw", value);
      }

      function settle(f, v) {
        if (f(v), q.shift(), q.length) resume(q[0][0], q[0][1]);
      }
    }

    function __asyncDelegator(o) {
      var i, p;
      return i = {}, verb("next"), verb("throw", function (e) {
        throw e;
      }), verb("return"), i[Symbol.iterator] = function () {
        return this;
      }, i;

      function verb(n, f) {
        i[n] = o[n] ? function (v) {
          return (p = !p) ? {
            value: __await(o[n](v)),
            done: n === "return"
          } : f ? f(v) : v;
        } : f;
      }
    }

    function __asyncValues(o) {
      if (!Symbol.asyncIterator) throw new TypeError("Symbol.asyncIterator is not defined.");
      var m = o[Symbol.asyncIterator],
          i;
      return m ? m.call(o) : (o = typeof __values === "function" ? __values(o) : o[Symbol.iterator](), i = {}, verb("next"), verb("throw"), verb("return"), i[Symbol.asyncIterator] = function () {
        return this;
      }, i);

      function verb(n) {
        i[n] = o[n] && function (v) {
          return new Promise(function (resolve, reject) {
            v = o[n](v), settle(resolve, reject, v.done, v.value);
          });
        };
      }

      function settle(resolve, reject, d, v) {
        Promise.resolve(v).then(function (v) {
          resolve({
            value: v,
            done: d
          });
        }, reject);
      }
    }

    function __makeTemplateObject(cooked, raw) {
      if (Object.defineProperty) {
        Object.defineProperty(cooked, "raw", {
          value: raw
        });
      } else {
        cooked.raw = raw;
      }

      return cooked;
    }

    ;

    function __importStar(mod) {
      if (mod && mod.__esModule) return mod;
      var result = {};
      if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
      result.default = mod;
      return result;
    }

    function __importDefault(mod) {
      return mod && mod.__esModule ? mod : {
        default: mod
      };
    }
    /***/

  },

  /***/
  "./src/app/app-routing.module.ts":
  /*!***************************************!*\
    !*** ./src/app/app-routing.module.ts ***!
    \***************************************/

  /*! exports provided: AppRoutingModule */

  /***/
  function srcAppAppRoutingModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function () {
      return AppRoutingModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _app_routes__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app.routes */
    "./src/app/app.routes.ts");

    let AppRoutingModule = class AppRoutingModule {};
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(_app_routes__WEBPACK_IMPORTED_MODULE_3__["routes"])],
      exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })], AppRoutingModule);
    /***/
  },

  /***/
  "./src/app/app.component.scss":
  /*!************************************!*\
    !*** ./src/app/app.component.scss ***!
    \************************************/

  /*! exports provided: default */

  /***/
  function srcAppAppComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/app.component.ts":
  /*!**********************************!*\
    !*** ./src/app/app.component.ts ***!
    \**********************************/

  /*! exports provided: AppComponent */

  /***/
  function srcAppAppComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppComponent", function () {
      return AppComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./services/common-utility-service */
    "./src/app/services/common-utility-service.ts");

    let AppComponent = class AppComponent {
      constructor(utilityService) {
        this.utilityService = utilityService;
        this.title = 'ng-Freelance';
      }

      ngOnInit() {}

    };

    AppComponent.ctorParameters = () => [{
      type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"]
    }];

    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-root',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./app.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./app.component.scss */
      "./src/app/app.component.scss")).default]
    })], AppComponent);
    /***/
  },

  /***/
  "./src/app/app.module.ts":
  /*!*******************************!*\
    !*** ./src/app/app.module.ts ***!
    \*******************************/

  /*! exports provided: AppModule */

  /***/
  function srcAppAppModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "AppModule", function () {
      return AppModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/platform-browser/animations */
    "./node_modules/@angular/platform-browser/fesm2015/animations.js");
    /* harmony import */


    var ngx_cookie__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ngx-cookie */
    "./node_modules/ngx-cookie/fesm2015/ngx-cookie.js");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./app-routing.module */
    "./src/app/app-routing.module.ts");
    /* harmony import */


    var _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./shared/shared.module */
    "./src/app/shared/shared.module.ts");
    /* harmony import */


    var _services_services_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./services/services.module */
    "./src/app/services/services.module.ts");
    /* harmony import */


    var _app_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./app.component */
    "./src/app/app.component.ts");
    /* harmony import */


    var _spartacus_storefront__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! @spartacus/storefront */
    "./node_modules/@spartacus/storefront/fesm2015/spartacus-storefront.js");
    /* harmony import */


    var _spartacus_assets__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! @spartacus/assets */
    "./node_modules/@spartacus/assets/fesm2015/spartacus-assets.js");

    let AppModule = class AppModule {};
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      declarations: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]],
      imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"].withServerTransition({
        appId: 'serverApp'
      }), _spartacus_storefront__WEBPACK_IMPORTED_MODULE_10__["B2cStorefrontModule"].withConfig({
        backend: {
          occ: {
            baseUrl: 'https://api.c050ygx6-obeikanin2-s1-public.model-t.cc.commerce.ondemand.com',
            prefix: '/rest/v2/'
          }
        },
        context: {
          baseSite: ['electronics-spa']
        },
        i18n: {
          resources: _spartacus_assets__WEBPACK_IMPORTED_MODULE_11__["translations"],
          chunks: _spartacus_assets__WEBPACK_IMPORTED_MODULE_11__["translationChunksConfig"],
          fallbackLang: 'en'
        },
        features: {
          level: '1.2'
        }
      }), _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserTransferStateModule"], _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_4__["BrowserAnimationsModule"], _shared_shared_module__WEBPACK_IMPORTED_MODULE_7__["SharedModule"], _services_services_module__WEBPACK_IMPORTED_MODULE_8__["ServicesModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_6__["AppRoutingModule"], ngx_cookie__WEBPACK_IMPORTED_MODULE_5__["CookieModule"].forRoot()],
      providers: [],
      bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_9__["AppComponent"]]
    })], AppModule);
    /***/
  },

  /***/
  "./src/app/app.routes.ts":
  /*!*******************************!*\
    !*** ./src/app/app.routes.ts ***!
    \*******************************/

  /*! exports provided: routes */

  /***/
  function srcAppAppRoutesTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "routes", function () {
      return routes;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _shared_organisms_home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! ./shared/organisms/home-organism/home-organism.component */
    "./src/app/shared/organisms/home-organism/home-organism.component.ts");
    /* harmony import */


    var _shared_organisms_not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./shared/organisms/not-found-organism/not-found-organism.component */
    "./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts");
    /* harmony import */


    var _services_home_resolver_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./services/home-resolver.service */
    "./src/app/services/home-resolver.service.ts");
    /* harmony import */


    var _shared_molecules_register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./shared/molecules/register-molecule/register-molecule.component */
    "./src/app/shared/molecules/register-molecule/register-molecule.component.ts");
    /* harmony import */


    var _shared_organisms_login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./shared/organisms/login-organism/login-organism.component */
    "./src/app/shared/organisms/login-organism/login-organism.component.ts");

    const routes = [{
      path: '',
      component: _shared_organisms_home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_1__["HomeOrganismComponent"],
      data: [{
        pageName: 'Home Page'
      }],
      resolve: {
        homeData: _services_home_resolver_service__WEBPACK_IMPORTED_MODULE_3__["HomeResolverService"]
      }
    }, {
      path: 'register',
      component: _shared_molecules_register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_4__["RegisterMoleculeComponent"]
    }, {
      path: 'login',
      component: _shared_organisms_login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_5__["LoginOrganismComponent"],
      data: [{
        pageName: 'Login Page'
      }]
    }, {
      path: 'not-found',
      component: _shared_organisms_not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_2__["NotFoundOrganismComponent"],
      pathMatch: 'full',
      data: [{
        pageName: 'Not Found'
      }]
    }, {
      path: '**',
      redirectTo: 'not-found',
      data: [{
        pageName: 'Not Found'
      }]
    }, {
      path: 'products',
      component: _shared_organisms_login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_5__["LoginOrganismComponent"],
      pathMatch: 'full',
      data: [{
        pageName: 'Products Listing'
      }]
    }];
    /***/
  },

  /***/
  "./src/app/material.module.ts":
  /*!************************************!*\
    !*** ./src/app/material.module.ts ***!
    \************************************/

  /*! exports provided: MaterialModule */

  /***/
  function srcAppMaterialModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MaterialModule", function () {
      return MaterialModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_material__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/material */
    "./node_modules/@angular/material/esm2015/material.js");

    const modules = [_angular_material__WEBPACK_IMPORTED_MODULE_2__["MatButtonModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatInputModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatRippleModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatIconModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDividerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCardModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSelectModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatFormFieldModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatDatepickerModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatNativeDateModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatCheckboxModule"], _angular_material__WEBPACK_IMPORTED_MODULE_2__["MatSnackBarModule"]];
    let MaterialModule = class MaterialModule {};
    MaterialModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [...modules],
      exports: [...modules]
    })], MaterialModule);
    /***/
  },

  /***/
  "./src/app/services/common-utility-service.ts":
  /*!****************************************************!*\
    !*** ./src/app/services/common-utility-service.ts ***!
    \****************************************************/

  /*! exports provided: CommonUtilityService */

  /***/
  function srcAppServicesCommonUtilityServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CommonUtilityService", function () {
      return CommonUtilityService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");
    /**
     * Utility service for the application
     * Common utility functions that can be used throughout the application
     * @author Mohamed Omar Farook
     */


    let CommonUtilityService = class CommonUtilityService {
      constructor(plateformId, http, state) {
        this.plateformId = plateformId;
        this.http = http;
        this.state = state;
      }
      /**
       * @param input object to be validated
       * @returns true if object is undefined or empty, otherwise false
       */


      isUndefinedOrEmpty(input) {
        if (undefined !== input && '' !== input) {
          return false;
        } else {
          return true;
        }
      }
      /**
       * @param input object to be validated
       * @returns true if object is undefined or null, otherwise false
       */


      isNullOrUndefined(input) {
        if (undefined !== input && null !== input) {
          return false;
        } else {
          return true;
        }
      }
      /**
       * Checks if application is running in browser
       */


      isBrowser() {
        if (Object(_angular_common__WEBPACK_IMPORTED_MODULE_2__["isPlatformBrowser"])(this.plateformId)) {
          return true;
        } else {
          return false;
        }
      }
      /**
       * @param Endpoint url
       * @returns Get request response from server
       */


      getRequest(url, header) {
        let api = this.getApi(url);
        return this.http.get(api, header);
      }
      /**
       * @param Endpoint url
       * @returns Post request response from server
       */


      postRequest(url, request, options) {
        let api = this.getApi(url);
        return this.http.post(api, request, options);
      }
      /**
       * @param url
       * @returns generated endpoint url
       */


      getApi(url) {
        if (url === '../../assets/mockApi/home.json') {
          return url;
        }

        return _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].hostName + url;
      }

      getAuth() {
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpHeaders"]({
          'Content-Type': 'application/x-www-form-urlencoded;'
        });
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].hostName + _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].oAuthAPI;
        const request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpParams"]().set('client_id', 'trusted_client').set('client_secret', 'secret').set('grant_type', 'client_credentials').set('scope', 'extended');
        return this.http.post(url, request.toString(), {
          headers: httpHeaders
        });
      }

    };

    CommonUtilityService.ctorParameters = () => [{
      type: Object,
      decorators: [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"],
        args: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"]]
      }]
    }, {
      type: _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClient"]
    }, {
      type: _angular_platform_browser__WEBPACK_IMPORTED_MODULE_4__["TransferState"]
    }];

    CommonUtilityService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(), tslib__WEBPACK_IMPORTED_MODULE_0__["__param"](0, Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Inject"])(_angular_core__WEBPACK_IMPORTED_MODULE_1__["PLATFORM_ID"]))], CommonUtilityService);
    /***/
  },

  /***/
  "./src/app/services/home-page.service.ts":
  /*!***********************************************!*\
    !*** ./src/app/services/home-page.service.ts ***!
    \***********************************************/

  /*! exports provided: HomePageService */

  /***/
  function srcAppServicesHomePageServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomePageService", function () {
      return HomePageService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../services/common-utility-service */
    "./src/app/services/common-utility-service.ts");

    let HomePageService = class HomePageService {
      constructor(http, utilityService) {
        this.http = http;
        this.utilityService = utilityService;
      }

      postUserMessage(req) {
        const url = '';
        return this.http.post(url, req);
      }

    };

    HomePageService.ctorParameters = () => [{
      type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
    }, {
      type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_3__["CommonUtilityService"]
    }];

    HomePageService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], HomePageService);
    /***/
  },

  /***/
  "./src/app/services/home-resolver.service.ts":
  /*!***************************************************!*\
    !*** ./src/app/services/home-resolver.service.ts ***!
    \***************************************************/

  /*! exports provided: HomeResolverService */

  /***/
  function srcAppServicesHomeResolverServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeResolverService", function () {
      return HomeResolverService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ./common-utility-service */
    "./src/app/services/common-utility-service.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");

    let HomeResolverService = class HomeResolverService {
      constructor(utilityService) {
        this.utilityService = utilityService;
      }

      resolve() {
        return this.utilityService.getRequest(_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].homePageEndPoint, '');
      }

    };

    HomeResolverService.ctorParameters = () => [{
      type: _common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"]
    }];

    HomeResolverService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], HomeResolverService);
    /***/
  },

  /***/
  "./src/app/services/login.service.ts":
  /*!*******************************************!*\
    !*** ./src/app/services/login.service.ts ***!
    \*******************************************/

  /*! exports provided: LoginService, UserContext */

  /***/
  function srcAppServicesLoginServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginService", function () {
      return LoginService;
    });
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "UserContext", function () {
      return UserContext;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! rxjs */
    "./node_modules/rxjs/_esm2015/index.js");
    /* harmony import */


    var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../services/common-utility-service */
    "./src/app/services/common-utility-service.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");

    let LoginService = class LoginService {
      constructor(http, utilityService) {
        this.http = http;
        this.utilityService = utilityService;
        this.userContextData = new rxjs__WEBPACK_IMPORTED_MODULE_3__["BehaviorSubject"](new UserContext('', ''));
        this.userContext = this.userContextData.asObservable();

        this.fetchUserDetails = (token, username) => {
          let httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json').set('Authorization', "Bearer".concat(token));
          let options = {
            headers: httpHeaders
          };
          let loginUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].loginEndpoint + username + '?';
          return this.utilityService.getRequest(loginUrl, options);
        };
      }

      changeUserContext(data) {
        this.userContextData.next(data);
      }

      userLogin(req) {
        const request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set('client_id', 'trusted_client').set('client_secret', 'secret').set('grant_type', 'client_credentials').set('scope', 'extended').set('username', req.username).set('password', req.password);
        let httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/x-www-form-urlencoded').set('Accept-Charset', 'UTF-8');
        let options = {
          headers: httpHeaders
        };
        let tokenUrl = _environments_environment__WEBPACK_IMPORTED_MODULE_5__["environment"].tokenEndpoint;
        return this.utilityService.postRequest(tokenUrl, request, options);
      }

    };

    LoginService.ctorParameters = () => [{
      type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
    }, {
      type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__["CommonUtilityService"]
    }];

    LoginService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    }), Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])()], LoginService);

    class UserContext {
      constructor(displayUID, displayName) {
        this.displayUID = displayUID;
        this.displayName = displayName;
        this.displayUID = displayUID;
        this.displayName = displayName;
      }

    }
    /***/

  },

  /***/
  "./src/app/services/register-user.service.ts":
  /*!***************************************************!*\
    !*** ./src/app/services/register-user.service.ts ***!
    \***************************************************/

  /*! exports provided: RegisterUserService */

  /***/
  function srcAppServicesRegisterUserServiceTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterUserService", function () {
      return RegisterUserService;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../environments/environment */
    "./src/environments/environment.ts");

    let RegisterUserService = class RegisterUserService {
      constructor(http) {
        this.http = http;
      }

      registerUserDetails(token, req) {
        const httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
          'Content-Type': 'application/x-www-form-urlencoded;',
          'Authorization': token
        });
        const request = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set('name', req.name).set('email', req.email).set('contact', req.mobile).set('company', req.company).set('sizeRange', req.size).set('commercialRegistration', req.commercial);
        const url = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName + _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].registerUserAPI;
        return this.http.post(url, request.toString(), {
          headers: httpHeaders
        });
      }

    };

    RegisterUserService.ctorParameters = () => [{
      type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]
    }];

    RegisterUserService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
      providedIn: 'root'
    })], RegisterUserService);
    /***/
  },

  /***/
  "./src/app/services/services.module.ts":
  /*!*********************************************!*\
    !*** ./src/app/services/services.module.ts ***!
    \*********************************************/

  /*! exports provided: ServicesModule */

  /***/
  function srcAppServicesServicesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ServicesModule", function () {
      return ServicesModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/common/http */
    "./node_modules/@angular/common/fesm2015/http.js");
    /* harmony import */


    var _common_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./common-utility-service */
    "./src/app/services/common-utility-service.ts");
    /* harmony import */


    var _home_page_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./home-page.service */
    "./src/app/services/home-page.service.ts");
    /* harmony import */


    var _home_resolver_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./home-resolver.service */
    "./src/app/services/home-resolver.service.ts");
    /* harmony import */


    var _register_user_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./register-user.service */
    "./src/app/services/register-user.service.ts");
    /* harmony import */


    var _login_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./login.service */
    "./src/app/services/login.service.ts");

    let ServicesModule = class ServicesModule {};
    ServicesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"]],
      exports: [],
      declarations: [],
      providers: [_common_utility_service__WEBPACK_IMPORTED_MODULE_4__["CommonUtilityService"], _home_page_service__WEBPACK_IMPORTED_MODULE_5__["HomePageService"], _home_resolver_service__WEBPACK_IMPORTED_MODULE_6__["HomeResolverService"], _register_user_service__WEBPACK_IMPORTED_MODULE_7__["RegisterUserService"], _login_service__WEBPACK_IMPORTED_MODULE_8__["LoginService"]]
    })], ServicesModule);
    /***/
  },

  /***/
  "./src/app/shared/directives/directives.module.ts":
  /*!********************************************************!*\
    !*** ./src/app/shared/directives/directives.module.ts ***!
    \********************************************************/

  /*! exports provided: DirectivesModule */

  /***/
  function srcAppSharedDirectivesDirectivesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "DirectivesModule", function () {
      return DirectivesModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");

    let DirectivesModule = class DirectivesModule {};
    DirectivesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
      exports: [],
      declarations: [],
      providers: []
    })], DirectivesModule);
    /***/
  },

  /***/
  "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.scss":
  /*!*************************************************************************************!*\
    !*** ./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.scss ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesCarousalMoleculeCarousalMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\nimg {\n  width: 100% !important;\n}\n.carousel-inner {\n  margin: 0;\n}\n.carousel-item {\n  height: 500px;\n}\n.carousel-indicators > .active {\n  background-color: #a7f108;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jYXJvdXNhbC1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXNzZXRzXFxjc3NcXHZhcmlhYmxlLnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY2Fyb3VzYWwtbW9sZWN1bGUvY2Fyb3VzYWwtbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY2Fyb3VzYWwtbW9sZWN1bGUvQzpcXGRlcGxveV92MS4wXFxqcy1zdG9yZWZyb250XFxzcGFydGFjdXNzdG9yZS9zcmNcXGFwcFxcc2hhcmVkXFxtb2xlY3VsZXNcXGNhcm91c2FsLW1vbGVjdWxlXFxjYXJvdXNhbC1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDSSxzQkFBQTtBRGlESjtBQy9DQTtFQUNJLFNBQUE7QURrREo7QUNoREE7RUFDSSxhQUFBO0FEbURKO0FDakRBO0VBQ0kseUJGUlk7QUM0RGhCIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jYXJvdXNhbC1tb2xlY3VsZS9jYXJvdXNhbC1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuaW1nIHtcbiAgd2lkdGg6IDEwMCUgIWltcG9ydGFudDtcbn1cblxuLmNhcm91c2VsLWlubmVyIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4uY2Fyb3VzZWwtaXRlbSB7XG4gIGhlaWdodDogNTAwcHg7XG59XG5cbi5jYXJvdXNlbC1pbmRpY2F0b3JzID4gLmFjdGl2ZSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhN2YxMDg7XG59IiwiQGltcG9ydCBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jc3MvY29uc3RhbnRzLnNjc3NcIjtcclxuaW1nIHtcclxuICAgIHdpZHRoOiAxMDAlICFpbXBvcnRhbnQ7XHJcbn1cclxuLmNhcm91c2VsLWlubmVyIHtcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG4uY2Fyb3VzZWwtaXRlbSB7XHJcbiAgICBoZWlnaHQ6IDUwMHB4O1xyXG59XHJcbi5jYXJvdXNlbC1pbmRpY2F0b3JzID4gLmFjdGl2ZSB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcclxufSJdfQ== */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: CarousalMoleculeComponent */

  /***/
  function srcAppSharedMoleculesCarousalMoleculeCarousalMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "CarousalMoleculeComponent", function () {
      return CarousalMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../environments/environment */
    "./src/environments/environment.ts");

    let CarousalMoleculeComponent = class CarousalMoleculeComponent {
      constructor() {
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].hostName;
      }

    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], CarousalMoleculeComponent.prototype, "homeContent", void 0);
    CarousalMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'cx-carousal-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./carousal-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./carousal-molecule.component.scss */
      "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.scss")).default]
    })], CarousalMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/client-logo-molecule/client-logo.component.scss":
  /*!**********************************************************************************!*\
    !*** ./src/app/shared/molecules/client-logo-molecule/client-logo.component.scss ***!
    \**********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesClientLogoMoleculeClientLogoComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.logo-section .title {\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n}\n.logo-section .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.logo-section .client-logos .img-grayscale {\n  filter: gray;\n  /* IE6-9 */\n  -webkit-filter: grayscale(1);\n  /* Google Chrome, Safari 6+ & Opera 15+ */\n  filter: grayscale(1);\n  /* Microsoft Edge and Firefox 35+ */\n  cursor: pointer;\n}\n.logo-section .client-logos .img-grayscale:hover {\n  opacity: 1;\n  -webkit-filter: grayscale(0);\n          filter: grayscale(0);\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jbGllbnQtbG9nby1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXNzZXRzXFxjc3NcXHZhcmlhYmxlLnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY2xpZW50LWxvZ28tbW9sZWN1bGUvY2xpZW50LWxvZ28uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY2xpZW50LWxvZ28tbW9sZWN1bGUvQzpcXGRlcGxveV92MS4wXFxqcy1zdG9yZWZyb250XFxzcGFydGFjdXNzdG9yZS9zcmNcXGFwcFxcc2hhcmVkXFxtb2xlY3VsZXNcXGNsaWVudC1sb2dvLW1vbGVjdWxlXFxjbGllbnQtbG9nby5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM3Q0k7RUFDSSxlQUFBO0VBQ0EsNEJBQUE7QURnRFI7QUMvQ1E7RUFDSSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQ0FBQTtBRGlEWjtBQzdDSTtFQUNJLFlBQUE7RUFDQSxVQUFBO0VBQ0EsNEJBQUE7RUFDQSx5Q0FBQTtFQUNBLG9CQUFBO0VBQ0EsbUNBQUE7RUFDQSxlQUFBO0FEK0NSO0FDOUNRO0VBQ1EsVUFBQTtFQUNBLDRCQUFBO1VBQUEsb0JBQUE7QURnRGhCIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jbGllbnQtbG9nby1tb2xlY3VsZS9jbGllbnQtbG9nby5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLmxvZ28tc2VjdGlvbiAudGl0bGUge1xuICBmb250LXNpemU6IDM0cHg7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1NZWRpdW1cIjtcbn1cbi5sb2dvLXNlY3Rpb24gLnRpdGxlOjphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDAgYXV0bztcbiAgd2lkdGg6IDglO1xuICBwYWRkaW5nLXRvcDogMTVweDtcbiAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkICNhN2YxMDg7XG59XG4ubG9nby1zZWN0aW9uIC5jbGllbnQtbG9nb3MgLmltZy1ncmF5c2NhbGUge1xuICBmaWx0ZXI6IGdyYXk7XG4gIC8qIElFNi05ICovXG4gIC13ZWJraXQtZmlsdGVyOiBncmF5c2NhbGUoMSk7XG4gIC8qIEdvb2dsZSBDaHJvbWUsIFNhZmFyaSA2KyAmIE9wZXJhIDE1KyAqL1xuICBmaWx0ZXI6IGdyYXlzY2FsZSgxKTtcbiAgLyogTWljcm9zb2Z0IEVkZ2UgYW5kIEZpcmVmb3ggMzUrICovXG4gIGN1cnNvcjogcG9pbnRlcjtcbn1cbi5sb2dvLXNlY3Rpb24gLmNsaWVudC1sb2dvcyAuaW1nLWdyYXlzY2FsZTpob3ZlciB7XG4gIG9wYWNpdHk6IDE7XG4gIGZpbHRlcjogZ3JheXNjYWxlKDApO1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvY3NzL2NvbnN0YW50cy5zY3NzXCI7XHJcbi5sb2dvLXNlY3Rpb257XHJcbiAgICAudGl0bGUge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzRweDtcclxuICAgICAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xyXG4gICAgICAgICY6OmFmdGVye1xyXG4gICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgICAgICAgIHdpZHRoOiA4JTtcclxuICAgICAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgICAgICAgICAgIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAjYTdmMTA4O1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4uY2xpZW50LWxvZ29zIHtcclxuICAgIC5pbWctZ3JheXNjYWxlIHtcclxuICAgICAgICBmaWx0ZXI6IGdyYXk7XHJcbiAgICAgICAgLyogSUU2LTkgKi9cclxuICAgICAgICAtd2Via2l0LWZpbHRlcjogZ3JheXNjYWxlKDEpO1xyXG4gICAgICAgIC8qIEdvb2dsZSBDaHJvbWUsIFNhZmFyaSA2KyAmIE9wZXJhIDE1KyAqL1xyXG4gICAgICAgIGZpbHRlcjogZ3JheXNjYWxlKDEpO1xyXG4gICAgICAgIC8qIE1pY3Jvc29mdCBFZGdlIGFuZCBGaXJlZm94IDM1KyAqL1xyXG4gICAgICAgIGN1cnNvcjogcG9pbnRlcjtcclxuICAgICAgICAmOmhvdmVye1xyXG4gICAgICAgICAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICAgICAgICAgIGZpbHRlcjogZ3JheXNjYWxlKDApO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG59XHJcbn1cclxuIl19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/client-logo-molecule/client-logo.component.ts":
  /*!********************************************************************************!*\
    !*** ./src/app/shared/molecules/client-logo-molecule/client-logo.component.ts ***!
    \********************************************************************************/

  /*! exports provided: ClientLogoComponent */

  /***/
  function srcAppSharedMoleculesClientLogoMoleculeClientLogoComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ClientLogoComponent", function () {
      return ClientLogoComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../environments/environment */
    "./src/environments/environment.ts");

    let ClientLogoComponent = class ClientLogoComponent {
      constructor(route) {
        this.route = route;
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName;
      }

      ngOnInit() {
        this.route.data.subscribe(data => {
          const response = data.homeData;

          if (response && response.contentSlots && response.contentSlots.contentSlot) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = response.contentSlots.contentSlot[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                const content = _step.value;

                if (content.slotId === 'ValuableClientSlot') {
                  this.clientLogos = content.components.component;
                }
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return != null) {
                  _iterator.return();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }
          }
        });
      }

    };

    ClientLogoComponent.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
    }];

    ClientLogoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cx-client-logo',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./client-logo.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/client-logo-molecule/client-logo.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./client-logo.component.scss */
      "./src/app/shared/molecules/client-logo-molecule/client-logo.component.scss")).default]
    })], ClientLogoComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.scss":
  /*!*****************************************************************************************!*\
    !*** ./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.scss ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesContactUsMoleculeContactUsMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.contact-us-form {\n  width: 100%;\n  padding: 5% 10%;\n  text-align: center;\n}\n.contact-us-form .title {\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n  margin: 0 auto;\n}\n.contact-us-form .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.contact-us-form .name-field,\n.contact-us-form .email-field {\n  width: 45%;\n}\n.contact-us-form .comment-field {\n  width: 100%;\n}\n.contact-us-form .error-message {\n  font-size: 12px;\n}\n.contact-us-form .submit-btn {\n  background-color: #a7f108;\n  border-radius: 24px;\n  width: 150px;\n  font-size: 17px;\n  padding: 8px;\n  font-weight: bold;\n  box-shadow: none;\n}\n::ng-deep .mat-form-field {\n  padding: 10px 0;\n}\n@media only screen and (max-width: 768px) {\n  .contact-us-form .name-field,\n.contact-us-form .email-field {\n    width: 100%;\n  }\n}\n.mat-card:not([class*=mat-elevation-z]) {\n  box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jb250YWN0LXVzLW1vbGVjdWxlL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhc3NldHNcXGNzc1xcdmFyaWFibGUuc2NzcyIsInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9jb250YWN0LXVzLW1vbGVjdWxlL2NvbnRhY3QtdXMtbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvY29udGFjdC11cy1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXBwXFxzaGFyZWRcXG1vbGVjdWxlc1xcY29udGFjdC11cy1tb2xlY3VsZVxcY29udGFjdC11cy1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FEaURGO0FDaERFO0VBQ0UsZUFBQTtFQUNBLDRCQUFBO0VBQ0EsY0FBQTtBRGtESjtBQ2pESTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGdDQUFBO0FEbUROO0FDaERFOztFQUVFLFVBQUE7QURrREo7QUNoREU7RUFDRSxXQUFBO0FEa0RKO0FDaERFO0VBQ0UsZUFBQTtBRGtESjtBQ2hERTtFQUNFLHlCRjFCWTtFRTJCWixtQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QURrREo7QUMvQ0E7RUFDRSxlQUFBO0FEa0RGO0FDOUNBO0VBRUk7O0lBRUUsV0FBQTtFRGdESjtBQUNGO0FDN0NBO0VBQ0UsZ0JBQUE7QUQrQ0YiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2NvbnRhY3QtdXMtbW9sZWN1bGUvY29udGFjdC11cy1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLmNvbnRhY3QtdXMtZm9ybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiA1JSAxMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5jb250YWN0LXVzLWZvcm0gLnRpdGxlIHtcbiAgZm9udC1zaXplOiAzNHB4O1xuICBmb250LWZhbWlseTogXCJSb2JvdG8tTWVkaXVtXCI7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuLmNvbnRhY3QtdXMtZm9ybSAudGl0bGU6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB3aWR0aDogOCU7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2E3ZjEwODtcbn1cbi5jb250YWN0LXVzLWZvcm0gLm5hbWUtZmllbGQsXG4uY29udGFjdC11cy1mb3JtIC5lbWFpbC1maWVsZCB7XG4gIHdpZHRoOiA0NSU7XG59XG4uY29udGFjdC11cy1mb3JtIC5jb21tZW50LWZpZWxkIHtcbiAgd2lkdGg6IDEwMCU7XG59XG4uY29udGFjdC11cy1mb3JtIC5lcnJvci1tZXNzYWdlIHtcbiAgZm9udC1zaXplOiAxMnB4O1xufVxuLmNvbnRhY3QtdXMtZm9ybSAuc3VibWl0LWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhN2YxMDg7XG4gIGJvcmRlci1yYWRpdXM6IDI0cHg7XG4gIHdpZHRoOiAxNTBweDtcbiAgZm9udC1zaXplOiAxN3B4O1xuICBwYWRkaW5nOiA4cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBib3gtc2hhZG93OiBub25lO1xufVxuXG46Om5nLWRlZXAgLm1hdC1mb3JtLWZpZWxkIHtcbiAgcGFkZGluZzogMTBweCAwO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIC5jb250YWN0LXVzLWZvcm0gLm5hbWUtZmllbGQsXG4uY29udGFjdC11cy1mb3JtIC5lbWFpbC1maWVsZCB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cbi5tYXQtY2FyZDpub3QoW2NsYXNzKj1tYXQtZWxldmF0aW9uLXpdKSB7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59IiwiQGltcG9ydCBcIi4uLy4uLy4uLy4uL2Fzc2V0cy9jc3MvY29uc3RhbnRzLnNjc3NcIjtcclxuLmNvbnRhY3QtdXMtZm9ybSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZzogNSUgMTAlO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAudGl0bGUge1xyXG4gICAgZm9udC1zaXplOiAzNHB4O1xyXG4gICAgZm9udC1mYW1pbHk6ICdSb2JvdG8tTWVkaXVtJzsgIFxyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAmOjphZnRlcntcclxuICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICB3aWR0aDogOCU7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAxNXB4O1xyXG4gICAgICBib3JkZXItYm90dG9tOiA0cHggc29saWQgJHByaW1hcnktY29sb3I7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5uYW1lLWZpZWxkLFxyXG4gIC5lbWFpbC1maWVsZCB7XHJcbiAgICB3aWR0aDogNDUlO1xyXG4gIH1cclxuICAuY29tbWVudC1maWVsZCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgLmVycm9yLW1lc3NhZ2Uge1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gIH1cclxuICAuc3VibWl0LWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAkcHJpbWFyeS1jb2xvcjtcclxuICAgIGJvcmRlci1yYWRpdXM6IDI0cHg7XHJcbiAgICB3aWR0aDogMTUwcHg7XHJcbiAgICBmb250LXNpemU6IDE3cHg7XHJcbiAgICBwYWRkaW5nOiA4cHg7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIGJveC1zaGFkb3c6IG5vbmU7XHJcbiAgfVxyXG59XHJcbjo6bmctZGVlcCAubWF0LWZvcm0tZmllbGR7XHJcbiAgcGFkZGluZzogMTBweCAwO1xyXG59XHJcblxyXG5cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA3NjhweCkge1xyXG4gIC5jb250YWN0LXVzLWZvcm0ge1xyXG4gICAgLm5hbWUtZmllbGQsXHJcbiAgICAuZW1haWwtZmllbGQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgIH1cclxuICB9XHJcbn1cclxuLm1hdC1jYXJkOm5vdChbY2xhc3MqPW1hdC1lbGV2YXRpb24tel0pIHtcclxuICBib3gtc2hhZG93OiBub25lO1xyXG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.ts":
  /*!***************************************************************************************!*\
    !*** ./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.ts ***!
    \***************************************************************************************/

  /*! exports provided: ContactUsMoleculeComponent */

  /***/
  function srcAppSharedMoleculesContactUsMoleculeContactUsMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "ContactUsMoleculeComponent", function () {
      return ContactUsMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _services_home_page_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../services/home-page.service */
    "./src/app/services/home-page.service.ts");

    let ContactUsMoleculeComponent = class ContactUsMoleculeComponent {
      constructor(fb, homePageService) {
        this.fb = fb;
        this.homePageService = homePageService;
        this.validationMessages = {
          name: [{
            type: 'required',
            message: 'Full name is required'
          }],
          email: [{
            type: 'required',
            message: 'Email is required'
          }, {
            type: 'email',
            message: 'Enter a valid email'
          }],
          message: [{
            type: 'required',
            message: 'Please enter some message'
          }]
        };
      }

      ngOnInit() {
        // user details form validations
        this.userDetailsForm = this.fb.group({
          name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
          email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
          message: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
      }

      onSubmitUserDetails(userMsg) {
        console.log('userValue', userMsg);

        if (userMsg) {
          this.homePageService.postUserMessage(userMsg).subscribe(() => {});
        }
      }

    };

    ContactUsMoleculeComponent.ctorParameters = () => [{
      type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
    }, {
      type: _services_home_page_service__WEBPACK_IMPORTED_MODULE_3__["HomePageService"]
    }];

    ContactUsMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-contact-us-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./contact-us-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./contact-us-molecule.component.scss */
      "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.scss")).default]
    })], ContactUsMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/footer-molecule/footer.component.scss":
  /*!************************************************************************!*\
    !*** ./src/app/shared/molecules/footer-molecule/footer.component.scss ***!
    \************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesFooterMoleculeFooterComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "@charset \"UTF-8\";\n/********************Custom code **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.demo-video {\n  position: relative;\n}\n.play-img {\n  position: absolute;\n  left: 40%;\n  top: 40%;\n}\n.text-uppercase {\n  text-transform: uppercase;\n}\n.btn-primary {\n  background-color: #a7f108;\n  color: #000;\n  border: 1px solid #a7f108;\n  outline: none;\n  border-radius: 20px;\n  padding: 10px 40px;\n  font-weight: bold;\n  vertical-align: middle;\n  text-align: center;\n}\n.icon_section h2 {\n  margin: 40px;\n}\n.icon_section h2:after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.icon_section .icon-box a {\n  display: block;\n  text-decoration: none;\n}\n.icon_section .icon-box a .icon-box-icon {\n  display: inline-flex;\n  align-items: center;\n  justify-content: center;\n  margin-bottom: 2rem;\n}\n.icon_section .icon-box a .icon-box-icon img {\n  width: 100px;\n  height: 100px;\n}\n.icon_section .icon-box a .icon-box-content h3 {\n  font-size: 1.4rem;\n}\n.item blockquote p {\n  font-size: 1.4em;\n  color: #555555;\n  padding: 1.2em 0px 1.2em 60px;\n  line-height: 1.6;\n  position: relative;\n  border: none;\n}\n.item blockquote p:before {\n  content: \"“\";\n  color: #a7f108;\n  font-size: 10em;\n  margin-right: 10px;\n  font-size: 4em;\n  position: absolute;\n  left: 10px;\n  top: -10px;\n}\n.item .testimonial_img {\n  align-items: center;\n  justify-content: center;\n}\n.copyrights-wrapper .min-footer {\n  border-top: 1px solid rgba(255, 255, 255, 0.6);\n}\n.copyrights-wrapper .copyright-text {\n  color: rgba(255, 255, 255, 0.6);\n  font-size: 0.9em;\n  padding: 30px 0;\n}\n.contact-form .form-field {\n  background: #f7f7f7;\n  /*border-radius: 15px;*/\n  border-left: 4px solid #a7f108;\n  width: 95%;\n  margin: 0 auto;\n  padding: 20px;\n}\n/***********Download app *****************/\n.app_store_dn {\n  background: url \"../img/app_store_dn.jpg\";\n}\n/********************Footer **************************/\n.footer-column .footer-sidebar {\n  padding: 50px 10px;\n}\n.footer-column h5 {\n  font-size: 1.1em;\n  font-weight: bold;\n}\n.footer-column ul li a {\n  color: #fff;\n  display: block;\n  padding: 5px 0;\n  text-decoration: none;\n}\n.footer-column .footer-column-5 ul li a {\n  display: block;\n  padding: 10px 0;\n}\n.news-letter-section #search-form_3 {\n  background: #fff;\n  /* Fallback color for non-css3 browsers */\n  width: 40%;\n  margin: 30px auto;\n  border-radius: 17px;\n  -webkit-border-radius: 17px;\n  -moz-border-radius: 17px;\n}\n.news-letter-section .search_3 {\n  background: #fafafa;\n  /* Fallback color for non-css3 browsers */\n  border: 0;\n  font-size: 16px;\n  padding: 9px;\n  width: 80%;\n  border-radius: 17px;\n  -webkit-border-radius: 17px;\n  -moz-border-radius: 17px;\n}\n.news-letter-section .search_3:focus {\n  outline: none;\n  background: #fff;\n  /* Fallback color for non-css3 browsers */\n}\n.news-letter-section .submit_3 {\n  background: #a7f108;\n  /* Fallback color for non-css3 browsers */\n  border: 0;\n  color: #000;\n  cursor: pointer;\n  float: right;\n  font: 16px \"Raleway\", sans-serif;\n  font-weight: bold;\n  height: 40px;\n  width: 95px;\n  outline: none;\n  border-radius: 30px;\n  -webkit-border-radius: 30px;\n  -moz-border-radius: 30px;\n}\n.news-letter-section .submit_3:hover {\n  background: #a7f108;\n  /* Fallback color for non-css3 browsers */\n}\n.news-letter-section .submit_3:active {\n  background: #a7f108;\n  /* Fallback color for non-css3 browsers */\n}\n.client-logos {\n  justify-content: space-between;\n  align-items: center;\n  display: flex;\n}\n.client-logos .img-grayscale {\n  filter: gray;\n  /* IE6-9 */\n  -webkit-filter: grayscale(1);\n  /* Google Chrome, Safari 6+ & Opera 15+ */\n  filter: grayscale(1);\n  /* Microsoft Edge and Firefox 35+ */\n  cursor: pointer;\n}\n.client-logos .img-grayscale:hover {\n  opacity: 1;\n  -webkit-filter: grayscale(0);\n          filter: grayscale(0);\n}\n.widget-area {\n  display: flex;\n}\n.navbar.navbar-inverse {\n  background-color: #000;\n  border-color: #000;\n}\n.navbar .navbar-nav li a {\n  color: #fff;\n  font-size: 0.9em;\n}\n.navbar .navbar-nav li.active a:after {\n  border-bottom: 2px solid #a7f108;\n  content: \"\";\n  display: block;\n  padding-top: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9mb290ZXItbW9sZWN1bGUvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2Zvb3Rlci1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXBwXFxzaGFyZWRcXG1vbGVjdWxlc1xcZm9vdGVyLW1vbGVjdWxlXFxmb290ZXIuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCLDJEQUFBO0FBQ0E7RUFDSSxXQUFBO0FERUo7QUNBRTtFQUNFLGdCQUFBO0FER0o7QUNERTtFQUNFLGNBQUE7QURJSjtBQ0ZFO0VBQ0UsU0FBQTtBREtKO0FDSEU7RUFDRSxVQUFBO0FETUo7QUNKRTtFQUNFLGNBQUE7QURPSjtBQ0xFO0VBQ0UsZ0JBQUE7QURRSjtBQ05FO0VBQ0UsZ0JBQUE7QURTSjtBQ1BFO0VBQ0UsbUJBQUE7QURVSjtBQ1JFO0VBQ0UsbUJBQUE7QURXSjtBQ1RFO0VBQ0UsbUJBQUE7QURZSjtBQ1ZFO0VBQ0UsZ0JBQUE7QURhSjtBQ1hFO0VBQ0Usa0JBQUE7QURjSjtBQ1pFO0VBQ0Usa0JBQUE7RUFDQSxTQUFBO0VBQ0EsUUFBQTtBRGVKO0FDYkU7RUFDRSx5QkFBQTtBRGdCSjtBQ2RFO0VBQ0UseUJBQUE7RUFDQSxXQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxrQkFBQTtBRGlCSjtBQ2RJO0VBQ0UsWUFBQTtBRGlCTjtBQ2hCTTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGdDQUFBO0FEa0JSO0FDZE07RUFDRSxjQUFBO0VBQ0EscUJBQUE7QURnQlI7QUNmUTtFQUNFLG9CQUFBO0VBQ0EsbUJBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0FEaUJWO0FDaEJVO0VBQ0UsWUFBQTtFQUNBLGFBQUE7QURrQlo7QUNkVTtFQUNFLGlCQUFBO0FEZ0JaO0FDUk07RUFDRSxnQkFBQTtFQUNBLGNBQUE7RUFDQSw2QkFBQTtFQUNBLGdCQUFBO0VBQ0Esa0JBQUE7RUFDQSxZQUFBO0FEV1I7QUNWUTtFQUNFLFlBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFVBQUE7QURZVjtBQ1JJO0VBQ0UsbUJBQUE7RUFDQSx1QkFBQTtBRFVOO0FDTkk7RUFDRSw4Q0FBQTtBRFNOO0FDUEk7RUFDRSwrQkFBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtBRFNOO0FDTEk7RUFDRSxtQkFBQTtFQUNBLHVCQUFBO0VBQ0EsOEJBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGFBQUE7QURRTjtBQ0xFLDBDQUFBO0FBQ0E7RUFDRSx5Q0FBQTtBRFFKO0FDTEUsc0RBQUE7QUFFRTtFQUNFLGtCQUFBO0FET047QUNMSTtFQUNFLGdCQUFBO0VBQ0EsaUJBQUE7QURPTjtBQ0hRO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0EscUJBQUE7QURLVjtBQ0VVO0VBQ0UsY0FBQTtFQUNBLGVBQUE7QURBWjtBQ09JO0VBQ0UsZ0JBQUE7RUFBa0IseUNBQUE7RUFDbEIsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSwyQkFBQTtFQUNBLHdCQUFBO0FESE47QUNLSTtFQUNFLG1CQUFBO0VBQXFCLHlDQUFBO0VBQ3JCLFNBQUE7RUFDQSxlQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxtQkFBQTtFQUNBLDJCQUFBO0VBQ0Esd0JBQUE7QURGTjtBQ0dNO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBQWtCLHlDQUFBO0FEQTFCO0FDR0k7RUFDRSxtQkFBQTtFQUFxQix5Q0FBQTtFQUNyQixTQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0NBQUE7RUFDQSxpQkFBQTtFQUNBLFlBQUE7RUFDQSxXQUFBO0VBQ0EsYUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSx3QkFBQTtBREFOO0FDQ007RUFDRSxtQkFBQTtFQUFxQix5Q0FBQTtBREU3QjtBQ0FNO0VBQ0UsbUJBQUE7RUFBcUIseUNBQUE7QURHN0I7QUNDRTtFQUNFLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxhQUFBO0FERUo7QUNESTtFQUNFLFlBQUE7RUFBYyxVQUFBO0VBQ2QsNEJBQUE7RUFBOEIseUNBQUE7RUFDOUIsb0JBQUE7RUFBc0IsbUNBQUE7RUFDdEIsZUFBQTtBRE1OO0FDTE07RUFDRSxVQUFBO0VBQ0EsNEJBQUE7VUFBQSxvQkFBQTtBRE9SO0FDRkU7RUFDSSxhQUFBO0FES047QUNESTtFQUNFLHNCQUFBO0VBQ0Esa0JBQUE7QURJTjtBQ0FRO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0FERVY7QUNFWTtFQUNFLGdDQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBREFkIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9mb290ZXItbW9sZWN1bGUvZm9vdGVyLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIGNvZGUgKioqKioqKioqKioqKioqKioqKioqKioqKiovXG4udGV4dC13aGl0ZSB7XG4gIGNvbG9yOiAjZmZmO1xufVxuXG4uc2Vjb25kYXJ5LWJnIHtcbiAgYmFja2dyb3VuZDogIzAwMDtcbn1cblxuLnByaW1hcnktY29sb3Ige1xuICBjb2xvcjogI2E3ZjEwODtcbn1cblxuLm5vLW1hcmdpbiB7XG4gIG1hcmdpbjogMDtcbn1cblxuLm5vLXBhZGRpbmcge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ubWFyZ2luLWF1dG8ge1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmZvbnQtYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA4MDA7XG59XG5cbi5mb250LXNlbWktYm9sZCB7XG4gIGZvbnQtd2VpZ2h0OiA2MDA7XG59XG5cbi5tYi0zMCB7XG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XG59XG5cbi5tYi01MCB7XG4gIG1hcmdpbi1ib3R0b206IDUwcHg7XG59XG5cbi5wci0yMCB7XG4gIHBhZGRpbmctcmlnaHQ6IDIwcHg7XG59XG5cbi5iZy13aGl0ZSB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG59XG5cbi5kZW1vLXZpZGVvIHtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4ucGxheS1pbWcge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGxlZnQ6IDQwJTtcbiAgdG9wOiA0MCU7XG59XG5cbi50ZXh0LXVwcGVyY2FzZSB7XG4gIHRleHQtdHJhbnNmb3JtOiB1cHBlcmNhc2U7XG59XG5cbi5idG4tcHJpbWFyeSB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNhN2YxMDg7XG4gIGNvbG9yOiAjMDAwO1xuICBib3JkZXI6IDFweCBzb2xpZCAjYTdmMTA4O1xuICBvdXRsaW5lOiBub25lO1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xuICBwYWRkaW5nOiAxMHB4IDQwcHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5pY29uX3NlY3Rpb24gaDIge1xuICBtYXJnaW46IDQwcHg7XG59XG4uaWNvbl9zZWN0aW9uIGgyOmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB3aWR0aDogOCU7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2E3ZjEwODtcbn1cbi5pY29uX3NlY3Rpb24gLmljb24tYm94IGEge1xuICBkaXNwbGF5OiBibG9jaztcbiAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xufVxuLmljb25fc2VjdGlvbiAuaWNvbi1ib3ggYSAuaWNvbi1ib3gtaWNvbiB7XG4gIGRpc3BsYXk6IGlubGluZS1mbGV4O1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgbWFyZ2luLWJvdHRvbTogMnJlbTtcbn1cbi5pY29uX3NlY3Rpb24gLmljb24tYm94IGEgLmljb24tYm94LWljb24gaW1nIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuLmljb25fc2VjdGlvbiAuaWNvbi1ib3ggYSAuaWNvbi1ib3gtY29udGVudCBoMyB7XG4gIGZvbnQtc2l6ZTogMS40cmVtO1xufVxuXG4uaXRlbSBibG9ja3F1b3RlIHAge1xuICBmb250LXNpemU6IDEuNGVtO1xuICBjb2xvcjogIzU1NTU1NTtcbiAgcGFkZGluZzogMS4yZW0gMHB4IDEuMmVtIDYwcHg7XG4gIGxpbmUtaGVpZ2h0OiAxLjY7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYm9yZGVyOiBub25lO1xufVxuLml0ZW0gYmxvY2txdW90ZSBwOmJlZm9yZSB7XG4gIGNvbnRlbnQ6IFwi4oCcXCI7XG4gIGNvbG9yOiAjYTdmMTA4O1xuICBmb250LXNpemU6IDEwZW07XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbiAgZm9udC1zaXplOiA0ZW07XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgbGVmdDogMTBweDtcbiAgdG9wOiAtMTBweDtcbn1cbi5pdGVtIC50ZXN0aW1vbmlhbF9pbWcge1xuICBhbGlnbi1pdGVtczogY2VudGVyO1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbn1cblxuLmNvcHlyaWdodHMtd3JhcHBlciAubWluLWZvb3RlciB7XG4gIGJvcmRlci10b3A6IDFweCBzb2xpZCByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG59XG4uY29weXJpZ2h0cy13cmFwcGVyIC5jb3B5cmlnaHQtdGV4dCB7XG4gIGNvbG9yOiByZ2JhKDI1NSwgMjU1LCAyNTUsIDAuNik7XG4gIGZvbnQtc2l6ZTogMC45ZW07XG4gIHBhZGRpbmc6IDMwcHggMDtcbn1cblxuLmNvbnRhY3QtZm9ybSAuZm9ybS1maWVsZCB7XG4gIGJhY2tncm91bmQ6ICNmN2Y3Zjc7XG4gIC8qYm9yZGVyLXJhZGl1czogMTVweDsqL1xuICBib3JkZXItbGVmdDogNHB4IHNvbGlkICNhN2YxMDg7XG4gIHdpZHRoOiA5NSU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nOiAyMHB4O1xufVxuXG4vKioqKioqKioqKipEb3dubG9hZCBhcHAgKioqKioqKioqKioqKioqKiovXG4uYXBwX3N0b3JlX2RuIHtcbiAgYmFja2dyb3VuZDogdXJsIFwiLi4vaW1nL2FwcF9zdG9yZV9kbi5qcGdcIjtcbn1cblxuLyoqKioqKioqKioqKioqKioqKioqRm9vdGVyICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLmZvb3Rlci1jb2x1bW4gLmZvb3Rlci1zaWRlYmFyIHtcbiAgcGFkZGluZzogNTBweCAxMHB4O1xufVxuLmZvb3Rlci1jb2x1bW4gaDUge1xuICBmb250LXNpemU6IDEuMWVtO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cbi5mb290ZXItY29sdW1uIHVsIGxpIGEge1xuICBjb2xvcjogI2ZmZjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDVweCAwO1xuICB0ZXh0LWRlY29yYXRpb246IG5vbmU7XG59XG4uZm9vdGVyLWNvbHVtbiAuZm9vdGVyLWNvbHVtbi01IHVsIGxpIGEge1xuICBkaXNwbGF5OiBibG9jaztcbiAgcGFkZGluZzogMTBweCAwO1xufVxuXG4ubmV3cy1sZXR0ZXItc2VjdGlvbiAjc2VhcmNoLWZvcm1fMyB7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIC8qIEZhbGxiYWNrIGNvbG9yIGZvciBub24tY3NzMyBicm93c2VycyAqL1xuICB3aWR0aDogNDAlO1xuICBtYXJnaW46IDMwcHggYXV0bztcbiAgYm9yZGVyLXJhZGl1czogMTdweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAxN3B4O1xuICAtbW96LWJvcmRlci1yYWRpdXM6IDE3cHg7XG59XG4ubmV3cy1sZXR0ZXItc2VjdGlvbiAuc2VhcmNoXzMge1xuICBiYWNrZ3JvdW5kOiAjZmFmYWZhO1xuICAvKiBGYWxsYmFjayBjb2xvciBmb3Igbm9uLWNzczMgYnJvd3NlcnMgKi9cbiAgYm9yZGVyOiAwO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHBhZGRpbmc6IDlweDtcbiAgd2lkdGg6IDgwJTtcbiAgYm9yZGVyLXJhZGl1czogMTdweDtcbiAgLXdlYmtpdC1ib3JkZXItcmFkaXVzOiAxN3B4O1xuICAtbW96LWJvcmRlci1yYWRpdXM6IDE3cHg7XG59XG4ubmV3cy1sZXR0ZXItc2VjdGlvbiAuc2VhcmNoXzM6Zm9jdXMge1xuICBvdXRsaW5lOiBub25lO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICAvKiBGYWxsYmFjayBjb2xvciBmb3Igbm9uLWNzczMgYnJvd3NlcnMgKi9cbn1cbi5uZXdzLWxldHRlci1zZWN0aW9uIC5zdWJtaXRfMyB7XG4gIGJhY2tncm91bmQ6ICNhN2YxMDg7XG4gIC8qIEZhbGxiYWNrIGNvbG9yIGZvciBub24tY3NzMyBicm93c2VycyAqL1xuICBib3JkZXI6IDA7XG4gIGNvbG9yOiAjMDAwO1xuICBjdXJzb3I6IHBvaW50ZXI7XG4gIGZsb2F0OiByaWdodDtcbiAgZm9udDogMTZweCBcIlJhbGV3YXlcIiwgc2Fucy1zZXJpZjtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGhlaWdodDogNDBweDtcbiAgd2lkdGg6IDk1cHg7XG4gIG91dGxpbmU6IG5vbmU7XG4gIGJvcmRlci1yYWRpdXM6IDMwcHg7XG4gIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMzBweDtcbiAgLW1vei1ib3JkZXItcmFkaXVzOiAzMHB4O1xufVxuLm5ld3MtbGV0dGVyLXNlY3Rpb24gLnN1Ym1pdF8zOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2E3ZjEwODtcbiAgLyogRmFsbGJhY2sgY29sb3IgZm9yIG5vbi1jc3MzIGJyb3dzZXJzICovXG59XG4ubmV3cy1sZXR0ZXItc2VjdGlvbiAuc3VibWl0XzM6YWN0aXZlIHtcbiAgYmFja2dyb3VuZDogI2E3ZjEwODtcbiAgLyogRmFsbGJhY2sgY29sb3IgZm9yIG5vbi1jc3MzIGJyb3dzZXJzICovXG59XG5cbi5jbGllbnQtbG9nb3Mge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4uY2xpZW50LWxvZ29zIC5pbWctZ3JheXNjYWxlIHtcbiAgZmlsdGVyOiBncmF5O1xuICAvKiBJRTYtOSAqL1xuICAtd2Via2l0LWZpbHRlcjogZ3JheXNjYWxlKDEpO1xuICAvKiBHb29nbGUgQ2hyb21lLCBTYWZhcmkgNisgJiBPcGVyYSAxNSsgKi9cbiAgZmlsdGVyOiBncmF5c2NhbGUoMSk7XG4gIC8qIE1pY3Jvc29mdCBFZGdlIGFuZCBGaXJlZm94IDM1KyAqL1xuICBjdXJzb3I6IHBvaW50ZXI7XG59XG4uY2xpZW50LWxvZ29zIC5pbWctZ3JheXNjYWxlOmhvdmVyIHtcbiAgb3BhY2l0eTogMTtcbiAgZmlsdGVyOiBncmF5c2NhbGUoMCk7XG59XG5cbi53aWRnZXQtYXJlYSB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG5cbi5uYXZiYXIubmF2YmFyLWludmVyc2Uge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xuICBib3JkZXItY29sb3I6ICMwMDA7XG59XG4ubmF2YmFyIC5uYXZiYXItbmF2IGxpIGEge1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1zaXplOiAwLjllbTtcbn1cbi5uYXZiYXIgLm5hdmJhci1uYXYgbGkuYWN0aXZlIGE6YWZ0ZXIge1xuICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2E3ZjEwODtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmctdG9wOiA1cHg7XG59IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIGNvZGUgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9XHJcbiAgLmRlbW8tdmlkZW8ge1xyXG4gICAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIH1cclxuICAucGxheS1pbWcge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogNDAlO1xyXG4gICAgdG9wOiA0MCU7XHJcbiAgfVxyXG4gIC50ZXh0LXVwcGVyY2FzZSB7XHJcbiAgICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG4gIH1cclxuICAuYnRuLXByaW1hcnkge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2E3ZjEwODtcclxuICAgIGNvbG9yOiAjMDAwO1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgI2E3ZjEwODtcclxuICAgIG91dGxpbmU6IG5vbmU7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgcGFkZGluZzogMTBweCA0MHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgICB2ZXJ0aWNhbC1hbGlnbjogbWlkZGxlO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIH1cclxuICAuaWNvbl9zZWN0aW9uIHtcclxuICAgIGgyIHtcclxuICAgICAgbWFyZ2luOiA0MHB4O1xyXG4gICAgICAmOmFmdGVyIHtcclxuICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIHdpZHRoOiA4JTtcclxuICAgICAgICBwYWRkaW5nLXRvcDogMTVweDtcclxuICAgICAgICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2E3ZjEwODtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmljb24tYm94IHtcclxuICAgICAgYSB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgIC5pY29uLWJveC1pY29uIHtcclxuICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1mbGV4O1xyXG4gICAgICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxuICAgICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAxMDBweDtcclxuICAgICAgICAgICAgaGVpZ2h0OiAxMDBweDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLmljb24tYm94LWNvbnRlbnQge1xyXG4gICAgICAgICAgaDMge1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuNHJlbTtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLml0ZW0ge1xyXG4gICAgYmxvY2txdW90ZSB7XHJcbiAgICAgIHAge1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS40ZW07XHJcbiAgICAgICAgY29sb3I6ICM1NTU1NTU7XHJcbiAgICAgICAgcGFkZGluZzogMS4yZW0gMHB4IDEuMmVtIDYwcHg7XHJcbiAgICAgICAgbGluZS1oZWlnaHQ6IDEuNjtcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgYm9yZGVyOiBub25lO1xyXG4gICAgICAgICY6YmVmb3JlIHtcclxuICAgICAgICAgIGNvbnRlbnQ6IFwiXFwyMDFDXCI7XHJcbiAgICAgICAgICBjb2xvcjogI2E3ZjEwODtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMTBlbTtcclxuICAgICAgICAgIG1hcmdpbi1yaWdodDogMTBweDtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogNGVtO1xyXG4gICAgICAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgICAgICAgbGVmdDogMTBweDtcclxuICAgICAgICAgIHRvcDogLTEwcHg7XHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAudGVzdGltb25pYWxfaW1nIHtcclxuICAgICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jb3B5cmlnaHRzLXdyYXBwZXIge1xyXG4gICAgLm1pbi1mb290ZXIge1xyXG4gICAgICBib3JkZXItdG9wOiAxcHggc29saWQgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xyXG4gICAgfVxyXG4gICAgLmNvcHlyaWdodC10ZXh0IHtcclxuICAgICAgY29sb3I6IHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuICAgICAgZm9udC1zaXplOiAwLjllbTtcclxuICAgICAgcGFkZGluZzogMzBweCAwO1xyXG4gICAgfVxyXG4gIH1cclxuICAuY29udGFjdC1mb3JtIHtcclxuICAgIC5mb3JtLWZpZWxkIHtcclxuICAgICAgYmFja2dyb3VuZDogI2Y3ZjdmNztcclxuICAgICAgLypib3JkZXItcmFkaXVzOiAxNXB4OyovXHJcbiAgICAgIGJvcmRlci1sZWZ0OiA0cHggc29saWQgI2E3ZjEwODtcclxuICAgICAgd2lkdGg6IDk1JTtcclxuICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIHBhZGRpbmc6IDIwcHg7XHJcbiAgICB9XHJcbiAgfVxyXG4gIC8qKioqKioqKioqKkRvd25sb2FkIGFwcCAqKioqKioqKioqKioqKioqKi9cclxuICAuYXBwX3N0b3JlX2RuIHtcclxuICAgIGJhY2tncm91bmQ6IHVybCAoXCIuLi9pbWcvYXBwX3N0b3JlX2RuLmpwZ1wiKTtcclxuICB9XHJcbiBcclxuICAvKioqKioqKioqKioqKioqKioqKipGb290ZXIgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiAgLmZvb3Rlci1jb2x1bW4ge1xyXG4gICAgLmZvb3Rlci1zaWRlYmFyIHtcclxuICAgICAgcGFkZGluZzogNTBweCAxMHB4O1xyXG4gICAgfVxyXG4gICAgaDUge1xyXG4gICAgICBmb250LXNpemU6IDEuMWVtO1xyXG4gICAgICBmb250LXdlaWdodDogYm9sZDtcclxuICAgIH1cclxuICAgIHVsIHtcclxuICAgICAgbGkge1xyXG4gICAgICAgIGEge1xyXG4gICAgICAgICAgY29sb3I6ICNmZmY7XHJcbiAgICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICAgIHBhZGRpbmc6IDVweCAwO1xyXG4gICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgLmZvb3Rlci1jb2x1bW4tNSB7XHJcbiAgICAgIHVsIHtcclxuICAgICAgICBsaSB7XHJcbiAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgICAgIHBhZGRpbmc6IDEwcHggMDtcclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgLm5ld3MtbGV0dGVyLXNlY3Rpb24ge1xyXG4gICAgI3NlYXJjaC1mb3JtXzMge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjZmZmOyAvKiBGYWxsYmFjayBjb2xvciBmb3Igbm9uLWNzczMgYnJvd3NlcnMgKi9cclxuICAgICAgd2lkdGg6IDQwJTtcclxuICAgICAgbWFyZ2luOiAzMHB4IGF1dG87XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE3cHg7XHJcbiAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTdweDtcclxuICAgICAgLW1vei1ib3JkZXItcmFkaXVzOiAxN3B4O1xyXG4gICAgfVxyXG4gICAgLnNlYXJjaF8zIHtcclxuICAgICAgYmFja2dyb3VuZDogI2ZhZmFmYTsgLyogRmFsbGJhY2sgY29sb3IgZm9yIG5vbi1jc3MzIGJyb3dzZXJzICovXHJcbiAgICAgIGJvcmRlcjogMDtcclxuICAgICAgZm9udC1zaXplOiAxNnB4O1xyXG4gICAgICBwYWRkaW5nOiA5cHg7XHJcbiAgICAgIHdpZHRoOiA4MCU7XHJcbiAgICAgIGJvcmRlci1yYWRpdXM6IDE3cHg7XHJcbiAgICAgIC13ZWJraXQtYm9yZGVyLXJhZGl1czogMTdweDtcclxuICAgICAgLW1vei1ib3JkZXItcmFkaXVzOiAxN3B4O1xyXG4gICAgICAmOmZvY3VzIHtcclxuICAgICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNmZmY7IC8qIEZhbGxiYWNrIGNvbG9yIGZvciBub24tY3NzMyBicm93c2VycyAqL1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgICAuc3VibWl0XzMge1xyXG4gICAgICBiYWNrZ3JvdW5kOiAjYTdmMTA4OyAvKiBGYWxsYmFjayBjb2xvciBmb3Igbm9uLWNzczMgYnJvd3NlcnMgKi9cclxuICAgICAgYm9yZGVyOiAwO1xyXG4gICAgICBjb2xvcjogIzAwMDtcclxuICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICBmbG9hdDogcmlnaHQ7XHJcbiAgICAgIGZvbnQ6IDE2cHggXCJSYWxld2F5XCIsIHNhbnMtc2VyaWY7XHJcbiAgICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgICBoZWlnaHQ6IDQwcHg7XHJcbiAgICAgIHdpZHRoOiA5NXB4O1xyXG4gICAgICBvdXRsaW5lOiBub25lO1xyXG4gICAgICBib3JkZXItcmFkaXVzOiAzMHB4O1xyXG4gICAgICAtd2Via2l0LWJvcmRlci1yYWRpdXM6IDMwcHg7XHJcbiAgICAgIC1tb3otYm9yZGVyLXJhZGl1czogMzBweDtcclxuICAgICAgJjpob3ZlciB7XHJcbiAgICAgICAgYmFja2dyb3VuZDogI2E3ZjEwODsgLyogRmFsbGJhY2sgY29sb3IgZm9yIG5vbi1jc3MzIGJyb3dzZXJzICovXHJcbiAgICAgIH1cclxuICAgICAgJjphY3RpdmUge1xyXG4gICAgICAgIGJhY2tncm91bmQ6ICNhN2YxMDg7IC8qIEZhbGxiYWNrIGNvbG9yIGZvciBub24tY3NzMyBicm93c2VycyAqL1xyXG4gICAgICB9XHJcbiAgICB9XHJcbiAgfVxyXG4gIC5jbGllbnQtbG9nb3Mge1xyXG4gICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gICAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICAuaW1nLWdyYXlzY2FsZSB7XHJcbiAgICAgIGZpbHRlcjogZ3JheTsgLyogSUU2LTkgKi9cclxuICAgICAgLXdlYmtpdC1maWx0ZXI6IGdyYXlzY2FsZSgxKTsgLyogR29vZ2xlIENocm9tZSwgU2FmYXJpIDYrICYgT3BlcmEgMTUrICovXHJcbiAgICAgIGZpbHRlcjogZ3JheXNjYWxlKDEpOyAvKiBNaWNyb3NvZnQgRWRnZSBhbmQgRmlyZWZveCAzNSsgKi9cclxuICAgICAgY3Vyc29yOnBvaW50ZXI7XHJcbiAgICAgICY6aG92ZXJ7XHJcbiAgICAgICAgb3BhY2l0eTogMTtcclxuICAgICAgICBmaWx0ZXI6IGdyYXlzY2FsZSgwKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG4gIH1cclxuXHJcbiAgLndpZGdldC1hcmVhIHtcclxuICAgICAgZGlzcGxheTogZmxleDtcclxuICB9XHJcbiAgXHJcbiAgLm5hdmJhciB7XHJcbiAgICAmLm5hdmJhci1pbnZlcnNlIHtcclxuICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzAwMDtcclxuICAgICAgYm9yZGVyLWNvbG9yOiAjMDAwO1xyXG4gICAgfVxyXG4gICAgLm5hdmJhci1uYXYge1xyXG4gICAgICBsaSB7XHJcbiAgICAgICAgYSB7XHJcbiAgICAgICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgICAgIGZvbnQtc2l6ZTogMC45ZW07XHJcbiAgICAgICAgfVxyXG4gICAgICAgICYuYWN0aXZlIHtcclxuICAgICAgICAgIGEge1xyXG4gICAgICAgICAgICAmOmFmdGVyIHtcclxuICAgICAgICAgICAgICBib3JkZXItYm90dG9tOiAycHggc29saWQgI2E3ZjEwODtcclxuICAgICAgICAgICAgICBjb250ZW50OiBcIlwiO1xyXG4gICAgICAgICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgICAgICAgIHBhZGRpbmctdG9wOiA1cHg7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbiAgIl19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/footer-molecule/footer.component.ts":
  /*!**********************************************************************!*\
    !*** ./src/app/shared/molecules/footer-molecule/footer.component.ts ***!
    \**********************************************************************/

  /*! exports provided: FooterComponent */

  /***/
  function srcAppSharedMoleculesFooterMoleculeFooterComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FooterComponent", function () {
      return FooterComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    let FooterComponent = class FooterComponent {
      constructor(route) {
        this.route = route;
      }

      ngOnInit() {}

    };

    FooterComponent.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
    }];

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], FooterComponent.prototype, "footerColumnSlot", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], FooterComponent.prototype, "copyRightSlot", void 0);
    FooterComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cx-footer',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./footer.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/footer-molecule/footer.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./footer.component.scss */
      "./src/app/shared/molecules/footer-molecule/footer.component.scss")).default]
    })], FooterComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.scss":
  /*!*********************************************************************************************!*\
    !*** ./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.scss ***!
    \*********************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesHeaderLinksMoleculeHeaderLinksMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.top_header {\n  padding: 20px;\n  display: flex;\n  justify-content: space-between;\n  vertical-align: middle;\n  align-items: center;\n  font-family: \"Roboto-Regular\";\n  font-size: 0.9em;\n}\n.top_header .top_section_left {\n  display: flex;\n}\n.top_header .top_section_right {\n  justify-content: flex-end;\n  display: flex;\n}\n.top_header .top_section_right li a {\n  font-size: 0.9em;\n  color: #a7f108;\n}\n@media only screen and (max-width: 480px) {\n  .top_header {\n    flex-direction: column;\n    padding: 0 !important;\n  }\n\n  .top_section_left, .top_section_right {\n    display: flex;\n    justify-content: space-between;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9oZWFkZXItbGlua3MtbW9sZWN1bGUvQzpcXGRlcGxveV92MS4wXFxqcy1zdG9yZWZyb250XFxzcGFydGFjdXNzdG9yZS9zcmNcXGFzc2V0c1xcY3NzXFx2YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2hlYWRlci1saW5rcy1tb2xlY3VsZS9oZWFkZXItbGlua3MtbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvaGVhZGVyLWxpbmtzLW1vbGVjdWxlL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhcHBcXHNoYXJlZFxcbW9sZWN1bGVzXFxoZWFkZXItbGlua3MtbW9sZWN1bGVcXGhlYWRlci1saW5rcy1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDRSxhQUFBO0VBQ0EsYUFBQTtFQUNBLDhCQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLDZCQUFBO0VBQ0EsZ0JBQUE7QURpREY7QUNoREU7RUFDRSxhQUFBO0FEa0RKO0FDaERFO0VBQ0UseUJBQUE7RUFDQSxhQUFBO0FEa0RKO0FDaERNO0VBQ0UsZ0JBQUE7RUFDQSxjRmZRO0FDaUVoQjtBQzVDQTtFQUNFO0lBQ0Usc0JBQUE7SUFDQSxxQkFBQTtFRCtDRjs7RUM3Q0E7SUFDRSxhQUFBO0lBQ0EsOEJBQUE7RURnREY7QUFDRiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvaGVhZGVyLWxpbmtzLW1vbGVjdWxlL2hlYWRlci1saW5rcy1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLnRvcF9oZWFkZXIge1xuICBwYWRkaW5nOiAyMHB4O1xuICBkaXNwbGF5OiBmbGV4O1xuICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG4gIGZvbnQtZmFtaWx5OiBcIlJvYm90by1SZWd1bGFyXCI7XG4gIGZvbnQtc2l6ZTogMC45ZW07XG59XG4udG9wX2hlYWRlciAudG9wX3NlY3Rpb25fbGVmdCB7XG4gIGRpc3BsYXk6IGZsZXg7XG59XG4udG9wX2hlYWRlciAudG9wX3NlY3Rpb25fcmlnaHQge1xuICBqdXN0aWZ5LWNvbnRlbnQ6IGZsZXgtZW5kO1xuICBkaXNwbGF5OiBmbGV4O1xufVxuLnRvcF9oZWFkZXIgLnRvcF9zZWN0aW9uX3JpZ2h0IGxpIGEge1xuICBmb250LXNpemU6IDAuOWVtO1xuICBjb2xvcjogI2E3ZjEwODtcbn1cblxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA0ODBweCkge1xuICAudG9wX2hlYWRlciB7XG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcbiAgICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG4gIH1cblxuICAudG9wX3NlY3Rpb25fbGVmdCwgLnRvcF9zZWN0aW9uX3JpZ2h0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogc3BhY2UtYmV0d2VlbjtcbiAgfVxufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvY3NzL2NvbnN0YW50cy5zY3NzXCI7XHJcbi50b3BfaGVhZGVyIHtcclxuICBwYWRkaW5nOiAyMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAganVzdGlmeS1jb250ZW50OiBzcGFjZS1iZXR3ZWVuO1xyXG4gIHZlcnRpY2FsLWFsaWduOiBtaWRkbGU7XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBmb250LWZhbWlseTogJ1JvYm90by1SZWd1bGFyJztcclxuICBmb250LXNpemU6IDAuOWVtO1xyXG4gIC50b3Bfc2VjdGlvbl9sZWZ0e1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICB9XHJcbiAgLnRvcF9zZWN0aW9uX3JpZ2h0IHtcclxuICAgIGp1c3RpZnktY29udGVudDogZmxleC1lbmQ7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgbGl7XHJcbiAgICAgIGF7XHJcbiAgICAgICAgZm9udC1zaXplOiAwLjllbTtcclxuICAgICAgICBjb2xvcjogJHByaW1hcnktY29sb3I7XHJcbiAgICAgIH1cclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNDgwcHgpIHtcclxuICAudG9wX2hlYWRlciB7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICAudG9wX3NlY3Rpb25fbGVmdCwgLnRvcF9zZWN0aW9uX3JpZ2h0IHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IHNwYWNlLWJldHdlZW47XHJcbiAgfVxyXG59XHJcblxyXG5cclxuICAiXX0= */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.ts":
  /*!*******************************************************************************************!*\
    !*** ./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.ts ***!
    \*******************************************************************************************/

  /*! exports provided: HeaderLinksMoleculeComponent */

  /***/
  function srcAppSharedMoleculesHeaderLinksMoleculeHeaderLinksMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HeaderLinksMoleculeComponent", function () {
      return HeaderLinksMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../services/login.service */
    "./src/app/services/login.service.ts");

    let HeaderLinksMoleculeComponent = class HeaderLinksMoleculeComponent {
      constructor(loginService) {
        this.loginService = loginService;
        this.displayName = '';
        this.isLoggedIn = false;
      }

      ngOnInit() {
        this.loginService.userContext.subscribe(context => {
          this.displayName = context.displayName;
          this.isLoggedIn = context.displayName ? true : false;
        });
      }

    };

    HeaderLinksMoleculeComponent.ctorParameters = () => [{
      type: _services_login_service__WEBPACK_IMPORTED_MODULE_2__["LoginService"]
    }];

    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], HeaderLinksMoleculeComponent.prototype, "headerLinks", void 0);
    HeaderLinksMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cx-header-links-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./header-links-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./header-links-molecule.component.scss */
      "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.scss")).default]
    })], HeaderLinksMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.css":
  /*!****************************************************************************************!*\
    !*** ./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.css ***!
    \****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesHeaderNavMoleculeHeaderNavMoleculeComponentCss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".nav-container {\r\n    display: flex;\r\n    flex-direction: row;\r\n    font-family: 'Roboto-Regular';\r\n    text-transform: uppercase;\r\n    font-size: 0.85em;\r\n}\r\n.mobile-sidenav, .hamburger {\r\n    display: none;\r\n}\r\n.nav-container nav {\r\n    width: 80%;\r\n    display: flex;\r\n    align-items: center;\r\n}\r\n.nav-container nav ul {\r\n    list-style: none;\r\n    margin: 0;\r\n    padding: 0;\r\n    display: flex;\r\n}\r\n.nav-container nav ul li {\r\n    padding: 0 10px;\r\n}\r\n.nav-container nav ul li a {\r\n     color: white;\r\n     text-decoration: none;\r\n     border-bottom: 3px solid transparent;\r\n     padding-bottom: 5px;\r\n}\r\n.nav-container nav ul li a:hover {\r\n    border-bottom: 3px solid #A7F108;\r\n}\r\n@media only screen and (max-width: 768px) {\r\n    .nav-container {\r\n        display: none;\r\n    }\r\n    .hamburger {\r\n        display: block;\r\n        width: 20%;\r\n    }\r\n    .mobile-nav-container {\r\n        display: flex;\r\n    }\r\n    .mobile-nav-container ul {\r\n        padding: 0;\r\n        list-style: none;\r\n    }\r\n    .mobile-logo-container {\r\n        display: flex;\r\n        justify-content: center;\r\n        width: 80%\r\n    }\r\n    .mobile-logo-container img {\r\n        width: 200px;\r\n    }\r\n    .mobile-sidenav {\r\n        display: block;\r\n        height: 100%;\r\n        width: 0;\r\n        position: fixed;\r\n        z-index: 9999;\r\n        top: 0;\r\n        left: 0;\r\n        background-color: #111;\r\n        overflow-x: hidden;\r\n        transition: 0.5s;\r\n        padding-top: 60px;\r\n      }\r\n      \r\n      .mobile-sidenav a {\r\n        padding: 8px 8px 8px 32px;\r\n        text-decoration: none;\r\n        font-size: 15px;\r\n        color: white;\r\n        display: block;\r\n        transition: 0.3s;\r\n      }\r\n      \r\n      .mobile-sidenav a:hover {\r\n        color: #f1f1f1;\r\n      }\r\n      \r\n      .mobile-sidenav .closebtn {\r\n        position: absolute;\r\n        top: 0;\r\n        right: 25px;\r\n        font-size: 36px;\r\n        margin-left: 50px;\r\n      }\r\n      \r\n}\r\n@media screen and (max-height: 450px) {\r\n    .mobile-sidenav {padding-top: 15px;}\r\n    .mobile-sidenav a {font-size: 18px;}\r\n  }\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9oZWFkZXItbmF2LW1vbGVjdWxlL2hlYWRlci1uYXYtbW9sZWN1bGUuY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtJQUNJLGFBQWE7SUFDYixtQkFBbUI7SUFDbkIsNkJBQTZCO0lBQzdCLHlCQUF5QjtJQUN6QixpQkFBaUI7QUFDckI7QUFDQTtJQUNJLGFBQWE7QUFDakI7QUFFQTtJQUNJLFVBQVU7SUFDVixhQUFhO0lBQ2IsbUJBQW1CO0FBQ3ZCO0FBRUE7SUFDSSxnQkFBZ0I7SUFDaEIsU0FBUztJQUNULFVBQVU7SUFDVixhQUFhO0FBQ2pCO0FBRUE7SUFDSSxlQUFlO0FBQ25CO0FBQ0M7S0FDSSxZQUFZO0tBQ1oscUJBQXFCO0tBQ3JCLG9DQUFvQztLQUNwQyxtQkFBbUI7QUFDeEI7QUFFQTtJQUNJLGdDQUFnQztBQUNwQztBQUVBO0lBQ0k7UUFDSSxhQUFhO0lBQ2pCO0lBQ0E7UUFDSSxjQUFjO1FBQ2QsVUFBVTtJQUNkO0lBQ0E7UUFDSSxhQUFhO0lBQ2pCO0lBQ0E7UUFDSSxVQUFVO1FBQ1YsZ0JBQWdCO0lBQ3BCO0lBQ0E7UUFDSSxhQUFhO1FBQ2IsdUJBQXVCO1FBQ3ZCO0lBQ0o7SUFDQTtRQUNJLFlBQVk7SUFDaEI7SUFDQTtRQUNJLGNBQWM7UUFDZCxZQUFZO1FBQ1osUUFBUTtRQUNSLGVBQWU7UUFDZixhQUFhO1FBQ2IsTUFBTTtRQUNOLE9BQU87UUFDUCxzQkFBc0I7UUFDdEIsa0JBQWtCO1FBQ2xCLGdCQUFnQjtRQUNoQixpQkFBaUI7TUFDbkI7O01BRUE7UUFDRSx5QkFBeUI7UUFDekIscUJBQXFCO1FBQ3JCLGVBQWU7UUFDZixZQUFZO1FBQ1osY0FBYztRQUNkLGdCQUFnQjtNQUNsQjs7TUFFQTtRQUNFLGNBQWM7TUFDaEI7O01BRUE7UUFDRSxrQkFBa0I7UUFDbEIsTUFBTTtRQUNOLFdBQVc7UUFDWCxlQUFlO1FBQ2YsaUJBQWlCO01BQ25COztBQUVOO0FBQ0U7SUFDRSxpQkFBaUIsaUJBQWlCLENBQUM7SUFDbkMsbUJBQW1CLGVBQWUsQ0FBQztFQUNyQyIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvaGVhZGVyLW5hdi1tb2xlY3VsZS9oZWFkZXItbmF2LW1vbGVjdWxlLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubmF2LWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IHJvdztcclxuICAgIGZvbnQtZmFtaWx5OiAnUm9ib3RvLVJlZ3VsYXInO1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IHVwcGVyY2FzZTtcclxuICAgIGZvbnQtc2l6ZTogMC44NWVtO1xyXG59XHJcbi5tb2JpbGUtc2lkZW5hdiwgLmhhbWJ1cmdlciB7XHJcbiAgICBkaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4ubmF2LWNvbnRhaW5lciBuYXYge1xyXG4gICAgd2lkdGg6IDgwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG59XHJcblxyXG4ubmF2LWNvbnRhaW5lciBuYXYgdWwge1xyXG4gICAgbGlzdC1zdHlsZTogbm9uZTtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIHBhZGRpbmc6IDA7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG59XHJcblxyXG4ubmF2LWNvbnRhaW5lciBuYXYgdWwgbGkge1xyXG4gICAgcGFkZGluZzogMCAxMHB4O1xyXG59XHJcbiAubmF2LWNvbnRhaW5lciBuYXYgdWwgbGkgYSB7XHJcbiAgICAgY29sb3I6IHdoaXRlO1xyXG4gICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxuICAgICBib3JkZXItYm90dG9tOiAzcHggc29saWQgdHJhbnNwYXJlbnQ7XHJcbiAgICAgcGFkZGluZy1ib3R0b206IDVweDtcclxufVxyXG5cclxuLm5hdi1jb250YWluZXIgbmF2IHVsIGxpIGE6aG92ZXIge1xyXG4gICAgYm9yZGVyLWJvdHRvbTogM3B4IHNvbGlkICNBN0YxMDg7XHJcbn1cclxuXHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcclxuICAgIC5uYXYtY29udGFpbmVyIHtcclxuICAgICAgICBkaXNwbGF5OiBub25lO1xyXG4gICAgfVxyXG4gICAgLmhhbWJ1cmdlciB7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgd2lkdGg6IDIwJTtcclxuICAgIH1cclxuICAgIC5tb2JpbGUtbmF2LWNvbnRhaW5lciB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgIH1cclxuICAgIC5tb2JpbGUtbmF2LWNvbnRhaW5lciB1bCB7XHJcbiAgICAgICAgcGFkZGluZzogMDtcclxuICAgICAgICBsaXN0LXN0eWxlOiBub25lO1xyXG4gICAgfVxyXG4gICAgLm1vYmlsZS1sb2dvLWNvbnRhaW5lciB7XHJcbiAgICAgICAgZGlzcGxheTogZmxleDtcclxuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICAgICAgICB3aWR0aDogODAlXHJcbiAgICB9XHJcbiAgICAubW9iaWxlLWxvZ28tY29udGFpbmVyIGltZyB7XHJcbiAgICAgICAgd2lkdGg6IDIwMHB4O1xyXG4gICAgfVxyXG4gICAgLm1vYmlsZS1zaWRlbmF2IHtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBoZWlnaHQ6IDEwMCU7XHJcbiAgICAgICAgd2lkdGg6IDA7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIHotaW5kZXg6IDk5OTk7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogIzExMTtcclxuICAgICAgICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgICAgICAgdHJhbnNpdGlvbjogMC41cztcclxuICAgICAgICBwYWRkaW5nLXRvcDogNjBweDtcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgLm1vYmlsZS1zaWRlbmF2IGEge1xyXG4gICAgICAgIHBhZGRpbmc6IDhweCA4cHggOHB4IDMycHg7XHJcbiAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiBub25lO1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgICAgICBjb2xvcjogd2hpdGU7XHJcbiAgICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICAgICAgdHJhbnNpdGlvbjogMC4zcztcclxuICAgICAgfVxyXG4gICAgICBcclxuICAgICAgLm1vYmlsZS1zaWRlbmF2IGE6aG92ZXIge1xyXG4gICAgICAgIGNvbG9yOiAjZjFmMWYxO1xyXG4gICAgICB9XHJcbiAgICAgIFxyXG4gICAgICAubW9iaWxlLXNpZGVuYXYgLmNsb3NlYnRuIHtcclxuICAgICAgICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgICAgICAgdG9wOiAwO1xyXG4gICAgICAgIHJpZ2h0OiAyNXB4O1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMzZweDtcclxuICAgICAgICBtYXJnaW4tbGVmdDogNTBweDtcclxuICAgICAgfVxyXG4gICAgICBcclxufVxyXG4gIEBtZWRpYSBzY3JlZW4gYW5kIChtYXgtaGVpZ2h0OiA0NTBweCkge1xyXG4gICAgLm1vYmlsZS1zaWRlbmF2IHtwYWRkaW5nLXRvcDogMTVweDt9XHJcbiAgICAubW9iaWxlLXNpZGVuYXYgYSB7Zm9udC1zaXplOiAxOHB4O31cclxuICB9Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.ts":
  /*!***************************************************************************************!*\
    !*** ./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.ts ***!
    \***************************************************************************************/

  /*! exports provided: HeaderNavMoleculeComponent */

  /***/
  function srcAppSharedMoleculesHeaderNavMoleculeHeaderNavMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HeaderNavMoleculeComponent", function () {
      return HeaderNavMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    let HeaderNavMoleculeComponent = class HeaderNavMoleculeComponent {
      constructor() {
        this.closeNav = () => {
          document.getElementById("mySidenav").style.width = "0";
        };

        this.openNav = () => {
          document.getElementById("mySidenav").style.width = "150px";
        };
      }

      ngOnInit() {
        console.log(this.headerNavLinks);
        console.log(this.logoUrl);
      }

    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], HeaderNavMoleculeComponent.prototype, "headerNavLinks", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], HeaderNavMoleculeComponent.prototype, "logoUrl", void 0);
    HeaderNavMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'cx-header-nav-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./header-nav-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./header-nav-molecule.component.css */
      "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.css")).default]
    })], HeaderNavMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.scss":
  /*!*****************************************************************************************!*\
    !*** ./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.scss ***!
    \*****************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesLoginFormMoleculeLoginFormMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".title {\n  display: flex;\n  justify-content: center;\n  padding: 30px;\n  margin: 0;\n  background-color: var(--cx-color-background);\n}\n\n.login-us-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%;\n  display: flex;\n  flex-direction: column;\n  justify-content: center;\n  margin: 40px auto !important;\n}\n\n.email-field, .password-field {\n  width: 100%;\n}\n\n.login-container {\n  padding: 0;\n  box-shadow: none;\n}\n\n.submit-btn {\n  background-color: var(--cx-color-primary) !important;\n  border-color: var(--cx-color-primary) !important;\n  color: white !important;\n  font-size: 20px;\n  font-weight: 400;\n  height: 48px;\n  max-height: 48px;\n}\n\n.register-btn {\n  background-color: rgba(0, 0, 0, 0.12);\n  font-size: 20px;\n  font-weight: 400;\n  height: 48px;\n  max-height: 48px;\n}\n\n.forgot-pwd {\n  padding-bottom: 20px;\n}\n\n.forgot-pwd a {\n  color: black;\n  text-decoration: underline !important;\n}\n\n.forgot-pwd a:hover {\n  color: var(--cx-color-primary) !important;\n}\n\n.register-link {\n  margin-bottom: 10px;\n}\n\n.error-msg {\n  display: flex;\n  justify-content: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9sb2dpbi1mb3JtLW1vbGVjdWxlL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhcHBcXHNoYXJlZFxcbW9sZWN1bGVzXFxsb2dpbi1mb3JtLW1vbGVjdWxlXFxsb2dpbi1mb3JtLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2xvZ2luLWZvcm0tbW9sZWN1bGUvbG9naW4tZm9ybS1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFHQTtFQUNJLGFBQUE7RUFDQSx1QkFBQTtFQUNBLGFBQUE7RUFDQSxTQUFBO0VBQ0EsNENBQUE7QUNGSjs7QUQyQkE7RUFDSSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsV0FBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtFQUNBLHVCQUFBO0VBQ0EsNEJBQUE7QUN4Qko7O0FEMEJFO0VBQ0UsV0FBQTtBQ3ZCSjs7QUR5QkU7RUFDSSxVQUFBO0VBQ0EsZ0JBQUE7QUN0Qk47O0FEd0JFO0VBQ0Usb0RBQUE7RUFDQSxnREFBQTtFQUNBLHVCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDckJKOztBRHdCRTtFQUNFLHFDQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FDckJKOztBRHdCRTtFQUNJLG9CQUFBO0FDckJOOztBRHNCTTtFQUNJLFlBQUE7RUFDQSxxQ0FBQTtBQ3BCVjs7QURxQlU7RUFDSSx5Q0FBQTtBQ25CZDs7QUR1QkU7RUFDSSxtQkFBQTtBQ3BCTjs7QUR1QkU7RUFDRSxhQUFBO0VBQ0EsdUJBQUE7QUNwQkoiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2xvZ2luLWZvcm0tbW9sZWN1bGUvbG9naW4tZm9ybS1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8vIC5sb2dpbi1jb250YWluZXIge1xyXG4vLyAgICAgcGFkZGluZzogMDtcclxuLy8gfVxyXG4udGl0bGUge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMzBweDtcclxuICAgIG1hcmdpbjogMDtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWN4LWNvbG9yLWJhY2tncm91bmQpO1xyXG59XHJcblxyXG4vLyBtYXQtZm9ybS1maWVsZCB7XHJcbi8vICAgICBmbG9hdDogbm9uZSAhaW1wb3J0YW50O1xyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vICAgICBkaXNwbGF5OiBmbGV4O1xyXG4vLyAgICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbi8vIH1cclxuLy8gLmxvZ2luLXVzLWZvcm0ge1xyXG4vLyAgICAgZGlzcGxheTogZmxleDtcclxuLy8gICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbi8vICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuLy8gICAgIHdpZHRoOiAxMDAlO1xyXG4vLyB9XHJcbi8vIC5tYXQtZm9ybS1maWVsZC13cmFwcGVyIHtcclxuLy8gICAgIHdpZHRoOiAxMDAlO1xyXG4vLyB9XHJcbi8vIGlucHV0IHtcclxuLy8gICAgIHdpZHRoOiAxMDAlXHJcbi8vIH1cclxuLy8gLmVtYWlsLWZpZWxkLCAucGFzc3dvcmQtZmllbGQge1xyXG4vLyAgICAgd2lkdGg6IDEwMCU7XHJcbi8vIH1cclxuXHJcbi5sb2dpbi11cy1mb3JtIHtcclxuICAgIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgbWFyZ2luOiA0MHB4IGF1dG8gIWltcG9ydGFudDtcclxuICB9XHJcbiAgLmVtYWlsLWZpZWxkLCAucGFzc3dvcmQtZmllbGQge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIC5sb2dpbi1jb250YWluZXIge1xyXG4gICAgICBwYWRkaW5nOiAwO1xyXG4gICAgICBib3gtc2hhZG93OiBub25lO1xyXG4gIH1cclxuICAuc3VibWl0LWJ0biB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1jeC1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLWNvbG9yOiB2YXIoLS1jeC1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xyXG4gICAgY29sb3I6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICBmb250LXdlaWdodDogNDAwO1xyXG4gICAgaGVpZ2h0OiA0OHB4O1xyXG4gICAgbWF4LWhlaWdodDogNDhweDtcclxuXHJcbiAgfVxyXG4gIC5yZWdpc3Rlci1idG4ge1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogcmdiYSgwLDAsMCwuMTIpO1xyXG4gICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgZm9udC13ZWlnaHQ6IDQwMDtcclxuICAgIGhlaWdodDogNDhweDtcclxuICAgIG1heC1oZWlnaHQ6IDQ4cHg7XHJcblxyXG4gIH1cclxuICAuZm9yZ290LXB3ZCB7XHJcbiAgICAgIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gICAgICBhIHtcclxuICAgICAgICAgIGNvbG9yOiBibGFjaztcclxuICAgICAgICAgIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lICFpbXBvcnRhbnQ7XHJcbiAgICAgICAgICAmOmhvdmVyIHtcclxuICAgICAgICAgICAgICBjb2xvcjogdmFyKC0tY3gtY29sb3ItcHJpbWFyeSkgIWltcG9ydGFudDtcclxuICAgICAgICAgIH1cclxuICAgICAgfVxyXG4gIH1cclxuICAucmVnaXN0ZXItbGluayB7XHJcbiAgICAgIG1hcmdpbi1ib3R0b206IDEwcHg7XHJcbiAgfVxyXG5cclxuICAuZXJyb3ItbXNnIHtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB9IiwiLnRpdGxlIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG4gIHBhZGRpbmc6IDMwcHg7XG4gIG1hcmdpbjogMDtcbiAgYmFja2dyb3VuZC1jb2xvcjogdmFyKC0tY3gtY29sb3ItYmFja2dyb3VuZCk7XG59XG5cbi5sb2dpbi11cy1mb3JtIHtcbiAgbWluLXdpZHRoOiAxNTBweDtcbiAgbWF4LXdpZHRoOiA1MDBweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICBtYXJnaW46IDQwcHggYXV0byAhaW1wb3J0YW50O1xufVxuXG4uZW1haWwtZmllbGQsIC5wYXNzd29yZC1maWVsZCB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4ubG9naW4tY29udGFpbmVyIHtcbiAgcGFkZGluZzogMDtcbiAgYm94LXNoYWRvdzogbm9uZTtcbn1cblxuLnN1Ym1pdC1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1jeC1jb2xvci1wcmltYXJ5KSAhaW1wb3J0YW50O1xuICBib3JkZXItY29sb3I6IHZhcigtLWN4LWNvbG9yLXByaW1hcnkpICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBmb250LXNpemU6IDIwcHg7XG4gIGZvbnQtd2VpZ2h0OiA0MDA7XG4gIGhlaWdodDogNDhweDtcbiAgbWF4LWhlaWdodDogNDhweDtcbn1cblxuLnJlZ2lzdGVyLWJ0biB7XG4gIGJhY2tncm91bmQtY29sb3I6IHJnYmEoMCwgMCwgMCwgMC4xMik7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDQwMDtcbiAgaGVpZ2h0OiA0OHB4O1xuICBtYXgtaGVpZ2h0OiA0OHB4O1xufVxuXG4uZm9yZ290LXB3ZCB7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xufVxuLmZvcmdvdC1wd2QgYSB7XG4gIGNvbG9yOiBibGFjaztcbiAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgIWltcG9ydGFudDtcbn1cbi5mb3Jnb3QtcHdkIGE6aG92ZXIge1xuICBjb2xvcjogdmFyKC0tY3gtY29sb3ItcHJpbWFyeSkgIWltcG9ydGFudDtcbn1cblxuLnJlZ2lzdGVyLWxpbmsge1xuICBtYXJnaW4tYm90dG9tOiAxMHB4O1xufVxuXG4uZXJyb3ItbXNnIHtcbiAgZGlzcGxheTogZmxleDtcbiAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.ts":
  /*!***************************************************************************************!*\
    !*** ./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.ts ***!
    \***************************************************************************************/

  /*! exports provided: LoginFormMoleculeComponent */

  /***/
  function srcAppSharedMoleculesLoginFormMoleculeLoginFormMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginFormMoleculeComponent", function () {
      return LoginFormMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _services_login_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/login.service */
    "./src/app/services/login.service.ts");

    let LoginFormMoleculeComponent = class LoginFormMoleculeComponent {
      constructor(fb, loginService, router) {
        this.fb = fb;
        this.loginService = loginService;
        this.router = router;
        this.loginError = false;
        this.validationMessages = {
          email: [{
            type: 'required',
            message: 'Email is required'
          }, {
            type: 'email',
            message: 'Enter a valid email'
          }],
          password: [{
            type: 'required',
            message: 'Password is required'
          }],
          message: [{
            type: 'required',
            message: 'Please enter some message'
          }]
        };
        this.loginService.userContext.subscribe(context => {
          this.userContext = context;
        });
      }

      ngOnInit() {
        // user details form validations
        this.userLoginForm = this.fb.group({
          email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
          password: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required])]
        });
      }

      onSubmitLoginDetails(userData) {
        console.log('userValue', userData);

        if (!!userData && !!userData.email && !!userData.password) {
          this.loginService.userLogin(userData).subscribe(res => {
            if (!!res && res.access_token) {
              this.loginService.fetchUserDetails(res.access_token, userData.email).subscribe(res => {
                if (res && res.active) {
                  this.userContext.displayUID = res.displayUid;
                  this.userContext.displayName = res.firstName;
                  this.loginService.changeUserContext(this.userContext);
                  this.router.navigate(['/']);
                }
              }, err => {
                this.loginError = true;
              });
            } else {
              this.loginError = true;
            }
          }, err => {
            this.loginError = true;
          });
        }
      }

    };

    LoginFormMoleculeComponent.ctorParameters = () => [{
      type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
    }, {
      type: _services_login_service__WEBPACK_IMPORTED_MODULE_4__["LoginService"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]
    }];

    LoginFormMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login-form-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login-form-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login-form-molecule.component.scss */
      "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.scss")).default]
    })], LoginFormMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/logo-molecule/logo-molecule.component.scss":
  /*!*****************************************************************************!*\
    !*** ./src/app/shared/molecules/logo-molecule/logo-molecule.component.scss ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesLogoMoleculeLogoMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".mobile-logo-container {\n  display: none;\n}\n\n:host {\n  width: 20%;\n  display: block;\n  padding: 0 10px;\n}\n\n.logo-container img {\n  max-width: 80%;\n}\n\n@media only screen and (max-width: 768px) {\n  :host {\n    display: flex;\n    justify-content: center;\n    width: 80%;\n  }\n\n  .mobile-logo-container {\n    display: block;\n  }\n  .mobile-logo-container img {\n    width: 200px;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9sb2dvLW1vbGVjdWxlL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhcHBcXHNoYXJlZFxcbW9sZWN1bGVzXFxsb2dvLW1vbGVjdWxlXFxsb2dvLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL2xvZ28tbW9sZWN1bGUvbG9nby1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUNDSjs7QURDQTtFQUNJLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQ0VKOztBRENBO0VBQ0ksY0FBQTtBQ0VKOztBREFBO0VBQ0k7SUFDSSxhQUFBO0lBQ0EsdUJBQUE7SUFDQSxVQUFBO0VDR047O0VEREU7SUFDSSxjQUFBO0VDSU47RURITTtJQUNJLFlBQUE7RUNLVjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9sb2dvLW1vbGVjdWxlL2xvZ28tbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubW9iaWxlLWxvZ28tY29udGFpbmVyIHtcclxuICAgIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuOmhvc3Qge1xyXG4gICAgd2lkdGg6IDIwJTtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgcGFkZGluZzogMCAxMHB4O1xyXG59XHJcblxyXG4ubG9nby1jb250YWluZXIgaW1nIHtcclxuICAgIG1heC13aWR0aDogODAlO1xyXG59XHJcbkBtZWRpYSBvbmx5IHNjcmVlbiBhbmQgKG1heC13aWR0aDogNzY4cHgpIHtcclxuICAgIDpob3N0IHtcclxuICAgICAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xyXG4gICAgICAgIHdpZHRoOiA4MCVcclxuICAgIH1cclxuICAgIC5tb2JpbGUtbG9nby1jb250YWluZXIge1xyXG4gICAgICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgICAgIGltZyB7XHJcbiAgICAgICAgICAgIHdpZHRoOiAyMDBweDtcclxuICAgICAgICB9XHJcbiAgICB9IFxyXG59IiwiLm1vYmlsZS1sb2dvLWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IG5vbmU7XG59XG5cbjpob3N0IHtcbiAgd2lkdGg6IDIwJTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHBhZGRpbmc6IDAgMTBweDtcbn1cblxuLmxvZ28tY29udGFpbmVyIGltZyB7XG4gIG1heC13aWR0aDogODAlO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDc2OHB4KSB7XG4gIDpob3N0IHtcbiAgICBkaXNwbGF5OiBmbGV4O1xuICAgIGp1c3RpZnktY29udGVudDogY2VudGVyO1xuICAgIHdpZHRoOiA4MCU7XG4gIH1cblxuICAubW9iaWxlLWxvZ28tY29udGFpbmVyIHtcbiAgICBkaXNwbGF5OiBibG9jaztcbiAgfVxuICAubW9iaWxlLWxvZ28tY29udGFpbmVyIGltZyB7XG4gICAgd2lkdGg6IDIwMHB4O1xuICB9XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/logo-molecule/logo-molecule.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/shared/molecules/logo-molecule/logo-molecule.component.ts ***!
    \***************************************************************************/

  /*! exports provided: LogoMoleculeComponent */

  /***/
  function srcAppSharedMoleculesLogoMoleculeLogoMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LogoMoleculeComponent", function () {
      return LogoMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../../environments/environment */
    "./src/environments/environment.ts");

    let LogoMoleculeComponent = class LogoMoleculeComponent {
      constructor() {
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_2__["environment"].hostName;
      }

      ngOnInit() {}

    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], LogoMoleculeComponent.prototype, "displayType", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])()], LogoMoleculeComponent.prototype, "logoUrl", void 0);
    LogoMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-logo-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./logo-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/logo-molecule/logo-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./logo-molecule.component.scss */
      "./src/app/shared/molecules/logo-molecule/logo-molecule.component.scss")).default]
    })], LogoMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/molecules.module.ts":
  /*!******************************************************!*\
    !*** ./src/app/shared/molecules/molecules.module.ts ***!
    \******************************************************/

  /*! exports provided: MoleculesModule */

  /***/
  function srcAppSharedMoleculesMoleculesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "MoleculesModule", function () {
      return MoleculesModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! @angular/platform-browser */
    "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _material_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ../../material.module */
    "./src/app/material.module.ts");
    /* harmony import */


    var _services_services_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ../../services/services.module */
    "./src/app/services/services.module.ts");
    /* harmony import */


    var _directives_directives_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ../directives/directives.module */
    "./src/app/shared/directives/directives.module.ts");
    /* harmony import */


    var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ../pipes/pipes.module */
    "./src/app/shared/pipes/pipes.module.ts");
    /* harmony import */


    var _header_links_molecule_header_links_molecule_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./header-links-molecule/header-links-molecule.component */
    "./src/app/shared/molecules/header-links-molecule/header-links-molecule.component.ts");
    /* harmony import */


    var _header_nav_molecule_header_nav_molecule_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(
    /*! ./header-nav-molecule/header-nav-molecule.component */
    "./src/app/shared/molecules/header-nav-molecule/header-nav-molecule.component.ts");
    /* harmony import */


    var _carousal_molecule_carousal_molecule_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(
    /*! ./carousal-molecule/carousal-molecule.component */
    "./src/app/shared/molecules/carousal-molecule/carousal-molecule.component.ts");
    /* harmony import */


    var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(
    /*! @ng-bootstrap/ng-bootstrap */
    "./node_modules/@ng-bootstrap/ng-bootstrap/fesm2015/ng-bootstrap.js");
    /* harmony import */


    var _logo_molecule_logo_molecule_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(
    /*! ./logo-molecule/logo-molecule.component */
    "./src/app/shared/molecules/logo-molecule/logo-molecule.component.ts");
    /* harmony import */


    var _contact_us_molecule_contact_us_molecule_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(
    /*! ./contact-us-molecule/contact-us-molecule.component */
    "./src/app/shared/molecules/contact-us-molecule/contact-us-molecule.component.ts");
    /* harmony import */


    var _client_logo_molecule_client_logo_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(
    /*! ./client-logo-molecule/client-logo.component */
    "./src/app/shared/molecules/client-logo-molecule/client-logo.component.ts");
    /* harmony import */


    var _saned_section_molecule_saned_section_molecule_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(
    /*! ./saned-section-molecule/saned-section-molecule.component */
    "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.ts");
    /* harmony import */


    var _register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(
    /*! ./register-molecule/register-molecule.component */
    "./src/app/shared/molecules/register-molecule/register-molecule.component.ts");
    /* harmony import */


    var _sign_up_molecule_sign_up_component__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(
    /*! ./sign-up-molecule/sign-up.component */
    "./src/app/shared/molecules/sign-up-molecule/sign-up.component.ts");
    /* harmony import */


    var _footer_molecule_footer_component__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(
    /*! ./footer-molecule/footer.component */
    "./src/app/shared/molecules/footer-molecule/footer.component.ts");
    /* harmony import */


    var _login_form_molecule_login_form_molecule_component__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(
    /*! ./login-form-molecule/login-form-molecule.component */
    "./src/app/shared/molecules/login-form-molecule/login-form-molecule.component.ts");
    /* harmony import */


    var _app_routing_module__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(
    /*! ../../app-routing.module */
    "./src/app/app-routing.module.ts");

    const components = [_header_links_molecule_header_links_molecule_component__WEBPACK_IMPORTED_MODULE_10__["HeaderLinksMoleculeComponent"], _header_nav_molecule_header_nav_molecule_component__WEBPACK_IMPORTED_MODULE_11__["HeaderNavMoleculeComponent"], _carousal_molecule_carousal_molecule_component__WEBPACK_IMPORTED_MODULE_12__["CarousalMoleculeComponent"], _logo_molecule_logo_molecule_component__WEBPACK_IMPORTED_MODULE_14__["LogoMoleculeComponent"], _contact_us_molecule_contact_us_molecule_component__WEBPACK_IMPORTED_MODULE_15__["ContactUsMoleculeComponent"], _client_logo_molecule_client_logo_component__WEBPACK_IMPORTED_MODULE_16__["ClientLogoComponent"], _saned_section_molecule_saned_section_molecule_component__WEBPACK_IMPORTED_MODULE_17__["SanedSectionMoleculeComponent"], _register_molecule_register_molecule_component__WEBPACK_IMPORTED_MODULE_18__["RegisterMoleculeComponent"], _sign_up_molecule_sign_up_component__WEBPACK_IMPORTED_MODULE_19__["SignUpComponent"], _footer_molecule_footer_component__WEBPACK_IMPORTED_MODULE_20__["FooterComponent"], _login_form_molecule_login_form_molecule_component__WEBPACK_IMPORTED_MODULE_21__["LoginFormMoleculeComponent"]];
    let MoleculesModule = class MoleculesModule {};
    MoleculesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_services_services_module__WEBPACK_IMPORTED_MODULE_7__["ServicesModule"], _directives_directives_module__WEBPACK_IMPORTED_MODULE_8__["DirectivesModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_9__["PipesModule"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_13__["NgbModule"], _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"], _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["BrowserModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormsModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["ReactiveFormsModule"], _material_module__WEBPACK_IMPORTED_MODULE_6__["MaterialModule"], _app_routing_module__WEBPACK_IMPORTED_MODULE_22__["AppRoutingModule"]],
      exports: [...components],
      declarations: [...components],
      providers: []
    })], MoleculesModule);
    /***/
  },

  /***/
  "./src/app/shared/molecules/register-molecule/register-molecule.component.scss":
  /*!*************************************************************************************!*\
    !*** ./src/app/shared/molecules/register-molecule/register-molecule.component.scss ***!
    \*************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesRegisterMoleculeRegisterMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = ".register-title {\n  display: flex;\n  flex-direction: column;\n  padding: 20px 0;\n  text-align: center;\n  color: currentcolor;\n  background-color: var(--cx-color-background);\n}\n.register-title a {\n  color: inherit;\n  padding: 5px;\n}\n.reg-form-wrapper {\n  margin: 70px auto;\n}\n.reg-form-wrapper form {\n  width: 300px;\n  margin: 0 auto;\n}\n.reg-form-wrapper form .mat-form-field {\n  width: 100%;\n}\n.reg-form-wrapper form .reg-btn {\n  background-color: #fe5757;\n  border-color: #fe5757;\n  width: 100%;\n  color: #ffffff;\n  font-size: 20px;\n  padding: 6px;\n}\n.reg-form-wrapper form .reg-btn:disabled {\n  opacity: 0.5;\n}\n.reg-form-wrapper form .btn-link {\n  font-weight: 400;\n  color: #fe5757;\n  font-size: 1.125rem;\n  color: #212738;\n  text-decoration: underline !important;\n  cursor: pointer;\n  display: inline-block;\n}\n@media only screen and (min-width: 600px) {\n  form {\n    width: 500px !important;\n    margin: 0 auto;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9yZWdpc3Rlci1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXBwXFxzaGFyZWRcXG1vbGVjdWxlc1xccmVnaXN0ZXItbW9sZWN1bGVcXHJlZ2lzdGVyLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIiwic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL3JlZ2lzdGVyLW1vbGVjdWxlL3JlZ2lzdGVyLW1vbGVjdWxlLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksYUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSw0Q0FBQTtBQ0NKO0FEQUk7RUFDSSxjQUFBO0VBQ0EsWUFBQTtBQ0VSO0FEQ0E7RUFDSSxpQkFBQTtBQ0VKO0FEREk7RUFDSSxZQUFBO0VBQ0EsY0FBQTtBQ0dSO0FERlE7RUFDSSxXQUFBO0FDSVo7QURGTztFQUNDLHlCQUFBO0VBQ0EscUJBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGVBQUE7RUFDQSxZQUFBO0FDSVI7QURIUTtFQUNJLFlBQUE7QUNLWjtBREZRO0VBQ0ksZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxjQUFBO0VBQ0EscUNBQUE7RUFDQSxlQUFBO0VBQ0EscUJBQUE7QUNJWjtBRENBO0VBQ0k7SUFDSSx1QkFBQTtJQUNBLGNBQUE7RUNFTjtBQUNGIiwiZmlsZSI6InNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9yZWdpc3Rlci1tb2xlY3VsZS9yZWdpc3Rlci1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yZWdpc3Rlci10aXRsZXtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG4gICAgcGFkZGluZzogMjBweCAwO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgY29sb3I6IGN1cnJlbnRjb2xvcjtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHZhcigtLWN4LWNvbG9yLWJhY2tncm91bmQpO1xyXG4gICAgYXtcclxuICAgICAgICBjb2xvcjogaW5oZXJpdDtcclxuICAgICAgICBwYWRkaW5nOiA1cHg7XHJcbiAgICB9XHJcbn1cclxuLnJlZy1mb3JtLXdyYXBwZXJ7XHJcbiAgICBtYXJnaW46IDcwcHggYXV0bztcclxuICAgIGZvcm17XHJcbiAgICAgICAgd2lkdGg6IDMwMHB4O1xyXG4gICAgICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gICAgICAgIC5tYXQtZm9ybS1maWVsZHtcclxuICAgICAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgLnJlZy1idG57XHJcbiAgICAgICAgYmFja2dyb3VuZC1jb2xvcjogI2ZlNTc1NztcclxuICAgICAgICBib3JkZXItY29sb3I6ICNmZTU3NTc7XHJcbiAgICAgICAgd2lkdGg6IDEwMCU7XHJcbiAgICAgICAgY29sb3I6ICNmZmZmZmY7XHJcbiAgICAgICAgZm9udC1zaXplOiAyMHB4O1xyXG4gICAgICAgIHBhZGRpbmc6IDZweDtcclxuICAgICAgICAmOmRpc2FibGVke1xyXG4gICAgICAgICAgICBvcGFjaXR5OiAwLjU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAuYnRuLWxpbmt7XHJcbiAgICAgICAgICAgIGZvbnQtd2VpZ2h0OiA0MDA7XHJcbiAgICAgICAgICAgIGNvbG9yOiAjZmU1NzU3O1xyXG4gICAgICAgICAgICBmb250LXNpemU6IDEuMTI1cmVtO1xyXG4gICAgICAgICAgICBjb2xvcjogIzIxMjczODtcclxuICAgICAgICAgICAgdGV4dC1kZWNvcmF0aW9uOiB1bmRlcmxpbmUgIWltcG9ydGFudDtcclxuICAgICAgICAgICAgY3Vyc29yOiBwb2ludGVyO1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XHJcbiAgICBcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuQG1lZGlhIG9ubHkgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA2MDBweCkge1xyXG4gICAgZm9ybXtcclxuICAgICAgICB3aWR0aDogNTAwcHggIWltcG9ydGFudDtcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgIH1cclxufSIsIi5yZWdpc3Rlci10aXRsZSB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XG4gIHBhZGRpbmc6IDIwcHggMDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogY3VycmVudGNvbG9yO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB2YXIoLS1jeC1jb2xvci1iYWNrZ3JvdW5kKTtcbn1cbi5yZWdpc3Rlci10aXRsZSBhIHtcbiAgY29sb3I6IGluaGVyaXQ7XG4gIHBhZGRpbmc6IDVweDtcbn1cblxuLnJlZy1mb3JtLXdyYXBwZXIge1xuICBtYXJnaW46IDcwcHggYXV0bztcbn1cbi5yZWctZm9ybS13cmFwcGVyIGZvcm0ge1xuICB3aWR0aDogMzAwcHg7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuLnJlZy1mb3JtLXdyYXBwZXIgZm9ybSAubWF0LWZvcm0tZmllbGQge1xuICB3aWR0aDogMTAwJTtcbn1cbi5yZWctZm9ybS13cmFwcGVyIGZvcm0gLnJlZy1idG4ge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmU1NzU3O1xuICBib3JkZXItY29sb3I6ICNmZTU3NTc7XG4gIHdpZHRoOiAxMDAlO1xuICBjb2xvcjogI2ZmZmZmZjtcbiAgZm9udC1zaXplOiAyMHB4O1xuICBwYWRkaW5nOiA2cHg7XG59XG4ucmVnLWZvcm0td3JhcHBlciBmb3JtIC5yZWctYnRuOmRpc2FibGVkIHtcbiAgb3BhY2l0eTogMC41O1xufVxuLnJlZy1mb3JtLXdyYXBwZXIgZm9ybSAuYnRuLWxpbmsge1xuICBmb250LXdlaWdodDogNDAwO1xuICBjb2xvcjogI2ZlNTc1NztcbiAgZm9udC1zaXplOiAxLjEyNXJlbTtcbiAgY29sb3I6ICMyMTI3Mzg7XG4gIHRleHQtZGVjb3JhdGlvbjogdW5kZXJsaW5lICFpbXBvcnRhbnQ7XG4gIGN1cnNvcjogcG9pbnRlcjtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrO1xufVxuXG5AbWVkaWEgb25seSBzY3JlZW4gYW5kIChtaW4td2lkdGg6IDYwMHB4KSB7XG4gIGZvcm0ge1xuICAgIHdpZHRoOiA1MDBweCAhaW1wb3J0YW50O1xuICAgIG1hcmdpbjogMCBhdXRvO1xuICB9XG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/register-molecule/register-molecule.component.ts":
  /*!***********************************************************************************!*\
    !*** ./src/app/shared/molecules/register-molecule/register-molecule.component.ts ***!
    \***********************************************************************************/

  /*! exports provided: RegisterMoleculeComponent */

  /***/
  function srcAppSharedMoleculesRegisterMoleculeRegisterMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "RegisterMoleculeComponent", function () {
      return RegisterMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/forms */
    "./node_modules/@angular/forms/fesm2015/forms.js");
    /* harmony import */


    var _services_register_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../services/register-user.service */
    "./src/app/services/register-user.service.ts");
    /* harmony import */


    var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../../../services/common-utility-service */
    "./src/app/services/common-utility-service.ts");
    /* harmony import */


    var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! @angular/material/snack-bar */
    "./node_modules/@angular/material/esm2015/snack-bar.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    let RegisterMoleculeComponent = class RegisterMoleculeComponent {
      constructor(fb, regUserService, router, matSnack, utilService) {
        this.fb = fb;
        this.regUserService = regUserService;
        this.router = router;
        this.matSnack = matSnack;
        this.utilService = utilService;
        this.validationMessages = {
          name: [{
            type: 'required',
            message: 'Full name is required'
          }],
          email: [{
            type: 'required',
            message: 'Email is required'
          }, {
            type: 'email',
            message: 'Enter a valid email'
          }],
          mobile: [{
            type: 'required',
            message: 'Mobile number is required'
          }, //  { type: 'minlength', message: 'Minimum 10 digits' },
          {
            type: 'pattern',
            message: 'Please enter a valid mobile number'
          }],
          company: [{
            type: 'required',
            message: 'Company name is required'
          }],
          size: [{
            type: 'required',
            message: 'Please select any option'
          }],
          commercial: [{
            type: 'required',
            message: 'Commercial registration is required'
          }]
        };
        this.errMsg = 'Sorry ! Please try again later';
        this.succMsg = 'User Registerd successfully !';
      }

      ngOnInit() {
        const MOBILE_PATTERN = '^((\\+91-?)|0)?[0-9]{10}$';
        this.registerForm = this.fb.group({
          name: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
          email: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].email])],
          mobile: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].compose([_angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].pattern(MOBILE_PATTERN)])],
          company: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
          size: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
          commercial: [null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
        });
      }

      registerUser(user) {
        const name = user.name,
              email = user.email,
              mobile = user.mobile,
              company = user.company,
              size = user.size,
              commercial = user.commercial;

        if (name && email && mobile && company && size && commercial) {
          this.utilService.getAuth().subscribe(res => {
            if (res && res.access_token) {
              this.regUserService.registerUserDetails(res.access_token, user).subscribe(result => {
                console.log('User registered successfully!', result);
                this.matSnack.open(this.succMsg, 'close', {
                  duration: 5000
                });
                this.router.navigate(['/']);
              }, err => {
                this.matSnack.open(this.errMsg, 'close', {
                  duration: 5000
                });
              });
            }
          }, err => {
            console.log('O Auth API failed', err);
            this.matSnack.open(this.errMsg, 'close', {
              duration: 5000
            });
          });
        }
      }

    };

    RegisterMoleculeComponent.ctorParameters = () => [{
      type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"]
    }, {
      type: _services_register_user_service__WEBPACK_IMPORTED_MODULE_3__["RegisterUserService"]
    }, {
      type: _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]
    }, {
      type: _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_5__["MatSnackBar"]
    }, {
      type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_4__["CommonUtilityService"]
    }];

    RegisterMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-register-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./register-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/register-molecule/register-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./register-molecule.component.scss */
      "./src/app/shared/molecules/register-molecule/register-molecule.component.scss")).default]
    })], RegisterMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.scss":
  /*!***********************************************************************************************!*\
    !*** ./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.scss ***!
    \***********************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesSanedSectionMoleculeSanedSectionMoleculeComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.saned-section-wrapper {\n  background-color: #000000;\n  padding-bottom: 0;\n}\n.saned-section-wrapper .title {\n  position: relative;\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n  margin: 0 auto;\n  color: #ffffff;\n}\n.saned-section-wrapper .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.saned-section-wrapper .domains {\n  display: inline-block;\n  width: 100%;\n  padding: 0;\n}\n.saned-section-wrapper .domains li {\n  float: left;\n}\n.saned-section-wrapper .domains li a {\n  display: inline-block;\n}\n.saned-section-wrapper .domains li a img {\n  width: 100px;\n  height: 100px;\n}\n.saned-section-wrapper .domains li a p {\n  margin: 30px 0;\n  color: #ffffff;\n  font-size: 20px;\n  font-weight: 600;\n  letter-spacing: 0.5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9zYW5lZC1zZWN0aW9uLW1vbGVjdWxlL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhc3NldHNcXGNzc1xcdmFyaWFibGUuc2NzcyIsInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9zYW5lZC1zZWN0aW9uLW1vbGVjdWxlL3NhbmVkLXNlY3Rpb24tbW9sZWN1bGUuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvc2FuZWQtc2VjdGlvbi1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXBwXFxzaGFyZWRcXG1vbGVjdWxlc1xcc2FuZWQtc2VjdGlvbi1tb2xlY3VsZVxcc2FuZWQtc2VjdGlvbi1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDRSx5QkFBQTtFQUNBLGlCQUFBO0FEaURGO0FDaERFO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxjQUFBO0VBQ0EsY0ZSSTtBQzBEUjtBQ2pESTtFQUNJLFdBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGdDQUFBO0FEbURSO0FDaERFO0VBQ0UscUJBQUE7RUFDQSxXQUFBO0VBQ0EsVUFBQTtBRGtESjtBQ2pEUTtFQUNJLFdBQUE7QURtRFo7QUNsRFk7RUFDSSxxQkFBQTtBRG9EaEI7QUNuRGdCO0VBQ0ksWUFBQTtFQUNBLGFBQUE7QURxRHBCO0FDbkRnQjtFQUNJLGNBQUE7RUFDQSxjRmhDWjtFRWlDWSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxxQkFBQTtBRHFEcEIiLCJmaWxlIjoic3JjL2FwcC9zaGFyZWQvbW9sZWN1bGVzL3NhbmVkLXNlY3Rpb24tbW9sZWN1bGUvc2FuZWQtc2VjdGlvbi1tb2xlY3VsZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAwMDA7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAudGl0bGUge1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGZvbnQtc2l6ZTogMzRweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBtYXJnaW46IDAgYXV0bztcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC50aXRsZTo6YWZ0ZXIge1xuICBjb250ZW50OiBcIlwiO1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHdpZHRoOiA4JTtcbiAgcGFkZGluZy10b3A6IDE1cHg7XG4gIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAjYTdmMTA4O1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAuZG9tYWlucyB7XG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDA7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC5kb21haW5zIGxpIHtcbiAgZmxvYXQ6IGxlZnQ7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC5kb21haW5zIGxpIGEge1xuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7XG59XG4uc2FuZWQtc2VjdGlvbi13cmFwcGVyIC5kb21haW5zIGxpIGEgaW1nIHtcbiAgd2lkdGg6IDEwMHB4O1xuICBoZWlnaHQ6IDEwMHB4O1xufVxuLnNhbmVkLXNlY3Rpb24td3JhcHBlciAuZG9tYWlucyBsaSBhIHAge1xuICBtYXJnaW46IDMwcHggMDtcbiAgY29sb3I6ICNmZmZmZmY7XG4gIGZvbnQtc2l6ZTogMjBweDtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbiAgbGV0dGVyLXNwYWNpbmc6IDAuNXB4O1xufSIsIkBpbXBvcnQgXCIuLi8uLi8uLi8uLi9hc3NldHMvY3NzL2NvbnN0YW50cy5zY3NzXCI7XHJcbi5zYW5lZC1zZWN0aW9uLXdyYXBwZXJ7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogJGJsYWNrO1xyXG4gIHBhZGRpbmctYm90dG9tOiAwO1xyXG4gIC50aXRsZSB7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmb250LXNpemU6IDM0cHg7XHJcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBjb2xvcjogJHdoaXRlO1xyXG4gICAgJjo6YWZ0ZXJ7XHJcbiAgICAgICAgY29udGVudDogXCJcIjtcclxuICAgICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgICBtYXJnaW46IDAgYXV0bztcclxuICAgICAgICB3aWR0aDogOCU7XHJcbiAgICAgICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgICAgICAgYm9yZGVyLWJvdHRvbTogNHB4IHNvbGlkICRwcmltYXJ5LWNvbG9yO1xyXG4gICAgfVxyXG4gIH1cclxuICAuZG9tYWluc3tcclxuICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgIHdpZHRoOjEwMCU7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgICAgIGxpe1xyXG4gICAgICAgICAgICBmbG9hdDpsZWZ0O1xyXG4gICAgICAgICAgICBhIHtcclxuICAgICAgICAgICAgICAgIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICAgICAgICAgICAgICAgIGltZ3tcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDoxMDBweDtcclxuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6MTAwcHg7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBwe1xyXG4gICAgICAgICAgICAgICAgICAgIG1hcmdpbjozMHB4IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sb3I6ICR3aGl0ZTtcclxuICAgICAgICAgICAgICAgICAgICBmb250LXNpemU6IDIwcHg7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgICAgICAgICAgICAgICAgICBsZXR0ZXItc3BhY2luZzogMC41cHg7XHJcbiAgICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgXHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.ts":
  /*!*********************************************************************************************!*\
    !*** ./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.ts ***!
    \*********************************************************************************************/

  /*! exports provided: SanedSectionMoleculeComponent */

  /***/
  function srcAppSharedMoleculesSanedSectionMoleculeSanedSectionMoleculeComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SanedSectionMoleculeComponent", function () {
      return SanedSectionMoleculeComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../environments/environment */
    "./src/environments/environment.ts");

    let SanedSectionMoleculeComponent = class SanedSectionMoleculeComponent {
      constructor(route) {
        this.route = route;
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName;
      }

      ngOnInit() {
        this.route.data.subscribe(data => {
          const response = data.homeData;

          if (response && response.contentSlots && response.contentSlots.contentSlot) {
            var _iteratorNormalCompletion2 = true;
            var _didIteratorError2 = false;
            var _iteratorError2 = undefined;

            try {
              for (var _iterator2 = response.contentSlots.contentSlot[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
                const content = _step2.value;

                if (content.slotId === 'SanedSolutionSlot') {
                  this.images = content.components.component;
                }
              }
            } catch (err) {
              _didIteratorError2 = true;
              _iteratorError2 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion2 && _iterator2.return != null) {
                  _iterator2.return();
                }
              } finally {
                if (_didIteratorError2) {
                  throw _iteratorError2;
                }
              }
            }
          }
        });
      }

    };

    SanedSectionMoleculeComponent.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
    }];

    SanedSectionMoleculeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-saned-section-molecule',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./saned-section-molecule.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./saned-section-molecule.component.scss */
      "./src/app/shared/molecules/saned-section-molecule/saned-section-molecule.component.scss")).default]
    })], SanedSectionMoleculeComponent);
    /***/
  },

  /***/
  "./src/app/shared/molecules/sign-up-molecule/sign-up.component.scss":
  /*!**************************************************************************!*\
    !*** ./src/app/shared/molecules/sign-up-molecule/sign-up.component.scss ***!
    \**************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedMoleculesSignUpMoleculeSignUpComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\n.news-letter-section {\n  background: url('slideimage.jpg');\n  width: 100%;\n  padding: 5% 10%;\n  text-align: center;\n}\n.news-letter-section .title {\n  font-size: 34px;\n  font-family: \"Roboto-Medium\";\n  margin: 0 auto;\n  color: #ffffff;\n}\n.news-letter-section .title::after {\n  content: \"\";\n  display: block;\n  margin: 0 auto;\n  width: 8%;\n  padding-top: 15px;\n  border-bottom: 4px solid #a7f108;\n}\n.sign-up-input-field input {\n  padding: 0.375rem 0.75rem;\n  border-radius: 5px 0px 0px 5px;\n  border: 2px solid #ffffff;\n  width: 50%;\n}\n.sign-up-input-field .signup-btn {\n  color: #000000;\n  padding: 0.375rem 0.75rem;\n  background: #a7f108;\n  border: 2px solid #a7f108;\n  border-radius: 0 5px 5px 0;\n  font-weight: bold;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9zaWduLXVwLW1vbGVjdWxlL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhc3NldHNcXGNzc1xcdmFyaWFibGUuc2NzcyIsInNyYy9hcHAvc2hhcmVkL21vbGVjdWxlcy9zaWduLXVwLW1vbGVjdWxlL3NpZ24tdXAuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvc2lnbi11cC1tb2xlY3VsZS9DOlxcZGVwbG95X3YxLjBcXGpzLXN0b3JlZnJvbnRcXHNwYXJ0YWN1c3N0b3JlL3NyY1xcYXBwXFxzaGFyZWRcXG1vbGVjdWxlc1xcc2lnbi11cC1tb2xlY3VsZVxcc2lnbi11cC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDRSxpQ0FBQTtFQUNBLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QURpREY7QUNoREU7RUFDRSxlQUFBO0VBQ0EsNEJBQUE7RUFDQSxjQUFBO0VBQ0EsY0ZUSTtBQzJEUjtBQ2pESTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtFQUNBLGdDQUFBO0FEbUROO0FDN0NFO0VBQ0UseUJBQUE7RUFDQSw4QkFBQTtFQUNBLHlCQUFBO0VBQ0EsVUFBQTtBRGdESjtBQzlDRTtFQUNFLGNGNUJJO0VFNkJKLHlCQUFBO0VBQ0EsbUJGN0JZO0VFOEJaLHlCQUFBO0VBQ0EsMEJBQUE7RUFDQSxpQkFBQTtBRGdESiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9tb2xlY3VsZXMvc2lnbi11cC1tb2xlY3VsZS9zaWduLXVwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cclxuJHdoaXRlOiAjZmZmZmZmO1xyXG4kYmxhY2s6ICMwMDAwMDA7XHJcbiRwcmltYXJ5LWNvbG9yOiAjYTdmMTA4O1xyXG4kYm9yZGVyLWNvbG9yOiAgcmdiYSgyNTUsIDI1NSwgMjU1LCAwLjYpO1xyXG4kZm9ybS1iZzogI2Y3ZjdmNztcclxuLnRleHQtd2hpdGUge1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gIC5zZWNvbmRhcnktYmcge1xyXG4gICAgYmFja2dyb3VuZDogIzAwMDtcclxuICB9XHJcbiAgLnByaW1hcnktY29sb3Ige1xyXG4gICAgY29sb3I6ICNhN2YxMDg7XHJcbiAgfVxyXG4gIC5uby1tYXJnaW4ge1xyXG4gICAgbWFyZ2luOiAwO1xyXG4gIH1cclxuICAubm8tcGFkZGluZyB7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gIH1cclxuICAubWFyZ2luLWF1dG8ge1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgfVxyXG4gIC5mb250LWJvbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IDgwMDtcclxuICB9XHJcbiAgLmZvbnQtc2VtaS1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgfVxyXG4gIC5tYi0zMCB7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIH1cclxuICAubWItNTAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNTBweDtcclxuICB9XHJcbiAgLnByLTIwIHtcclxuICAgIHBhZGRpbmctcmlnaHQ6IDIwcHg7XHJcbiAgfVxyXG4gIC5iZy13aGl0ZSB7XHJcbiAgICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIH0iLCIvKioqKioqKioqKioqKioqKioqKipDdXN0b20gVmFyaWFibGVzICoqKioqKioqKioqKioqKioqKioqKioqKioqL1xuLnRleHQtd2hpdGUge1xuICBjb2xvcjogI2ZmZjtcbn1cblxuLnNlY29uZGFyeS1iZyB7XG4gIGJhY2tncm91bmQ6ICMwMDA7XG59XG5cbi5wcmltYXJ5LWNvbG9yIHtcbiAgY29sb3I6ICNhN2YxMDg7XG59XG5cbi5uby1tYXJnaW4ge1xuICBtYXJnaW46IDA7XG59XG5cbi5uby1wYWRkaW5nIHtcbiAgcGFkZGluZzogMDtcbn1cblxuLm1hcmdpbi1hdXRvIHtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5mb250LWJvbGQge1xuICBmb250LXdlaWdodDogODAwO1xufVxuXG4uZm9udC1zZW1pLWJvbGQge1xuICBmb250LXdlaWdodDogNjAwO1xufVxuXG4ubWItMzAge1xuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xufVxuXG4ubWItNTAge1xuICBtYXJnaW4tYm90dG9tOiA1MHB4O1xufVxuXG4ucHItMjAge1xuICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xufVxuXG4uYmctd2hpdGUge1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xufVxuXG4ubmV3cy1sZXR0ZXItc2VjdGlvbiB7XG4gIGJhY2tncm91bmQ6IHVybChcIi4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2xpZGVpbWFnZS5qcGdcIik7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nOiA1JSAxMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cbi5uZXdzLWxldHRlci1zZWN0aW9uIC50aXRsZSB7XG4gIGZvbnQtc2l6ZTogMzRweDtcbiAgZm9udC1mYW1pbHk6IFwiUm9ib3RvLU1lZGl1bVwiO1xuICBtYXJnaW46IDAgYXV0bztcbiAgY29sb3I6ICNmZmZmZmY7XG59XG4ubmV3cy1sZXR0ZXItc2VjdGlvbiAudGl0bGU6OmFmdGVyIHtcbiAgY29udGVudDogXCJcIjtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICB3aWR0aDogOCU7XG4gIHBhZGRpbmctdG9wOiAxNXB4O1xuICBib3JkZXItYm90dG9tOiA0cHggc29saWQgI2E3ZjEwODtcbn1cblxuLnNpZ24tdXAtaW5wdXQtZmllbGQgaW5wdXQge1xuICBwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtO1xuICBib3JkZXItcmFkaXVzOiA1cHggMHB4IDBweCA1cHg7XG4gIGJvcmRlcjogMnB4IHNvbGlkICNmZmZmZmY7XG4gIHdpZHRoOiA1MCU7XG59XG4uc2lnbi11cC1pbnB1dC1maWVsZCAuc2lnbnVwLWJ0biB7XG4gIGNvbG9yOiAjMDAwMDAwO1xuICBwYWRkaW5nOiAwLjM3NXJlbSAwLjc1cmVtO1xuICBiYWNrZ3JvdW5kOiAjYTdmMTA4O1xuICBib3JkZXI6IDJweCBzb2xpZCAjYTdmMTA4O1xuICBib3JkZXItcmFkaXVzOiAwIDVweCA1cHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59IiwiQGltcG9ydCAnLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzcyc7XHJcbi5uZXdzLWxldHRlci1zZWN0aW9uIHtcclxuICBiYWNrZ3JvdW5kOiB1cmwoJy4uLy4uLy4uLy4uL2Fzc2V0cy9pbWFnZXMvc2xpZGVpbWFnZS5qcGcnKTtcclxuICB3aWR0aDogMTAwJTtcclxuICBwYWRkaW5nOiA1JSAxMCU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIC50aXRsZSB7XHJcbiAgICBmb250LXNpemU6IDM0cHg7XHJcbiAgICBmb250LWZhbWlseTogJ1JvYm90by1NZWRpdW0nO1xyXG4gICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICBjb2xvcjokd2hpdGU7XHJcbiAgICAmOjphZnRlciB7XHJcbiAgICAgIGNvbnRlbnQ6ICcnO1xyXG4gICAgICBkaXNwbGF5OiBibG9jaztcclxuICAgICAgbWFyZ2luOiAwIGF1dG87XHJcbiAgICAgIHdpZHRoOiA4JTtcclxuICAgICAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgICAgIGJvcmRlci1ib3R0b206IDRweCBzb2xpZCAkcHJpbWFyeS1jb2xvcjtcclxuICAgIH1cclxuICB9XHJcbn1cclxuXHJcbi5zaWduLXVwLWlucHV0LWZpZWxke1xyXG4gIGlucHV0e1xyXG4gICAgcGFkZGluZzogMC4zNzVyZW0gMC43NXJlbTtcclxuICAgIGJvcmRlci1yYWRpdXM6IDVweCAwcHggMHB4IDVweDtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkICR3aGl0ZTtcclxuICAgIHdpZHRoOiA1MCU7XHJcbiAgfVxyXG4gIC5zaWdudXAtYnRue1xyXG4gICAgY29sb3I6ICRibGFjaztcclxuICAgIHBhZGRpbmc6IDAuMzc1cmVtIDAuNzVyZW07XHJcbiAgICBiYWNrZ3JvdW5kOiAkcHJpbWFyeS1jb2xvcjtcclxuICAgIGJvcmRlcjogMnB4IHNvbGlkICRwcmltYXJ5LWNvbG9yO1xyXG4gICAgYm9yZGVyLXJhZGl1czogMCA1cHggNXB4IDA7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbn0iXX0= */";
    /***/
  },

  /***/
  "./src/app/shared/molecules/sign-up-molecule/sign-up.component.ts":
  /*!************************************************************************!*\
    !*** ./src/app/shared/molecules/sign-up-molecule/sign-up.component.ts ***!
    \************************************************************************/

  /*! exports provided: SignUpComponent */

  /***/
  function srcAppSharedMoleculesSignUpMoleculeSignUpComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SignUpComponent", function () {
      return SignUpComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../../../../environments/environment */
    "./src/environments/environment.ts");

    let SignUpComponent = class SignUpComponent {
      constructor(route) {
        this.route = route;
        this.hostName = _environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].hostName;
      }

      ngOnInit() {}

    };

    SignUpComponent.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
    }];

    SignUpComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-cx-sign-up',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./sign-up.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/molecules/sign-up-molecule/sign-up.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./sign-up.component.scss */
      "./src/app/shared/molecules/sign-up-molecule/sign-up.component.scss")).default]
    })], SignUpComponent);
    /***/
  },

  /***/
  "./src/app/shared/organisms/footer-organism/footer-organism.component.scss":
  /*!*********************************************************************************!*\
    !*** ./src/app/shared/organisms/footer-organism/footer-organism.component.scss ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedOrganismsFooterOrganismFooterOrganismComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\nheader {\n  background: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL29yZ2FuaXNtcy9mb290ZXItb3JnYW5pc20vQzpcXGRlcGxveV92MS4wXFxqcy1zdG9yZWZyb250XFxzcGFydGFjdXNzdG9yZS9zcmNcXGFzc2V0c1xcY3NzXFx2YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2Zvb3Rlci1vcmdhbmlzbS9mb290ZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvZm9vdGVyLW9yZ2FuaXNtL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhcHBcXHNoYXJlZFxcb3JnYW5pc21zXFxmb290ZXItb3JnYW5pc21cXGZvb3Rlci1vcmdhbmlzbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDSSxtQkFBQTtBRGlESiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvZm9vdGVyLW9yZ2FuaXNtL2Zvb3Rlci1vcmdhbmlzbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMDAwMDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xyXG5oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogJGJsYWNrO1xyXG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/organisms/footer-organism/footer-organism.component.ts":
  /*!*******************************************************************************!*\
    !*** ./src/app/shared/organisms/footer-organism/footer-organism.component.ts ***!
    \*******************************************************************************/

  /*! exports provided: FooterOrganismComponent */

  /***/
  function srcAppSharedOrganismsFooterOrganismFooterOrganismComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "FooterOrganismComponent", function () {
      return FooterOrganismComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../services/common-utility-service */
    "./src/app/services/common-utility-service.ts");
    /* harmony import */


    var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/environments/environment */
    "./src/environments/environment.ts");

    let FooterOrganismComponent = class FooterOrganismComponent {
      constructor(utilityService) {
        this.utilityService = utilityService;
        this.footerColumnSlot = [];
        this.copyRightSlot = [];
      }

      ngOnInit() {
        const footerUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].footerEndPoint;
        this.utilityService.getRequest(footerUrl, '').subscribe(data => {
          const response = JSON.parse(JSON.stringify(data));

          if (response && response.contentSlots && response.contentSlots.contentSlot) {
            var _iteratorNormalCompletion3 = true;
            var _didIteratorError3 = false;
            var _iteratorError3 = undefined;

            try {
              for (var _iterator3 = response.contentSlots.contentSlot[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
                const content = _step3.value;

                if (content.slotId === 'CopyRightsSlot') {
                  this.copyRightSlot = content.components.component[0];
                }
              }
            } catch (err) {
              _didIteratorError3 = true;
              _iteratorError3 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion3 && _iterator3.return != null) {
                  _iterator3.return();
                }
              } finally {
                if (_didIteratorError3) {
                  throw _iteratorError3;
                }
              }
            }

            this.footerColumnSlot = response.contentSlots.contentSlot.filter(each => each.slotId !== "CopyRightsSlot");
          }
        });
      }

    };

    FooterOrganismComponent.ctorParameters = () => [{
      type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"]
    }];

    FooterOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-footer-organism',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./footer-organism.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/footer-organism/footer-organism.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./footer-organism.component.scss */
      "./src/app/shared/organisms/footer-organism/footer-organism.component.scss")).default]
    })], FooterOrganismComponent);
    /***/
  },

  /***/
  "./src/app/shared/organisms/header-organism/header-organism.component.scss":
  /*!*********************************************************************************!*\
    !*** ./src/app/shared/organisms/header-organism/header-organism.component.scss ***!
    \*********************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedOrganismsHeaderOrganismHeaderOrganismComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "/********************Custom Variables **************************/\n.text-white {\n  color: #fff;\n}\n.secondary-bg {\n  background: #000;\n}\n.primary-color {\n  color: #a7f108;\n}\n.no-margin {\n  margin: 0;\n}\n.no-padding {\n  padding: 0;\n}\n.margin-auto {\n  margin: 0 auto;\n}\n.font-bold {\n  font-weight: 800;\n}\n.font-semi-bold {\n  font-weight: 600;\n}\n.mb-30 {\n  margin-bottom: 30px;\n}\n.mb-50 {\n  margin-bottom: 50px;\n}\n.pr-20 {\n  padding-right: 20px;\n}\n.bg-white {\n  background: #fff;\n}\nheader {\n  background: #000000;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2hhcmVkL29yZ2FuaXNtcy9oZWFkZXItb3JnYW5pc20vQzpcXGRlcGxveV92MS4wXFxqcy1zdG9yZWZyb250XFxzcGFydGFjdXNzdG9yZS9zcmNcXGFzc2V0c1xcY3NzXFx2YXJpYWJsZS5zY3NzIiwic3JjL2FwcC9zaGFyZWQvb3JnYW5pc21zL2hlYWRlci1vcmdhbmlzbS9oZWFkZXItb3JnYW5pc20uY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvaGVhZGVyLW9yZ2FuaXNtL0M6XFxkZXBsb3lfdjEuMFxcanMtc3RvcmVmcm9udFxcc3BhcnRhY3Vzc3RvcmUvc3JjXFxhcHBcXHNoYXJlZFxcb3JnYW5pc21zXFxoZWFkZXItb3JnYW5pc21cXGhlYWRlci1vcmdhbmlzbS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQSxnRUFBQTtBQU1BO0VBQ0ksV0FBQTtBQ0pKO0FETUU7RUFDRSxnQkFBQTtBQ0hKO0FES0U7RUFDRSxjQUFBO0FDRko7QURJRTtFQUNFLFNBQUE7QUNESjtBREdFO0VBQ0UsVUFBQTtBQ0FKO0FERUU7RUFDRSxjQUFBO0FDQ0o7QURDRTtFQUNFLGdCQUFBO0FDRUo7QURBRTtFQUNFLGdCQUFBO0FDR0o7QURERTtFQUNFLG1CQUFBO0FDSUo7QURGRTtFQUNFLG1CQUFBO0FDS0o7QURIRTtFQUNFLG1CQUFBO0FDTUo7QURKRTtFQUNFLGdCQUFBO0FDT0o7QUM5Q0E7RUFDSSxtQkFBQTtBRGlESiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvaGVhZGVyLW9yZ2FuaXNtL2hlYWRlci1vcmdhbmlzbS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qKioqKioqKioqKioqKioqKioqKkN1c3RvbSBWYXJpYWJsZXMgKioqKioqKioqKioqKioqKioqKioqKioqKiovXHJcbiR3aGl0ZTogI2ZmZmZmZjtcclxuJGJsYWNrOiAjMDAwMDAwO1xyXG4kcHJpbWFyeS1jb2xvcjogI2E3ZjEwODtcclxuJGJvcmRlci1jb2xvcjogIHJnYmEoMjU1LCAyNTUsIDI1NSwgMC42KTtcclxuJGZvcm0tYmc6ICNmN2Y3Zjc7XHJcbi50ZXh0LXdoaXRlIHtcclxuICAgIGNvbG9yOiAjZmZmO1xyXG4gIH1cclxuICAuc2Vjb25kYXJ5LWJnIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgfVxyXG4gIC5wcmltYXJ5LWNvbG9yIHtcclxuICAgIGNvbG9yOiAjYTdmMTA4O1xyXG4gIH1cclxuICAubm8tbWFyZ2luIHtcclxuICAgIG1hcmdpbjogMDtcclxuICB9XHJcbiAgLm5vLXBhZGRpbmcge1xyXG4gICAgcGFkZGluZzogMDtcclxuICB9XHJcbiAgLm1hcmdpbi1hdXRvIHtcclxuICAgIG1hcmdpbjogMCBhdXRvO1xyXG4gIH1cclxuICAuZm9udC1ib2xkIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA4MDA7XHJcbiAgfVxyXG4gIC5mb250LXNlbWktYm9sZCB7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gIH1cclxuICAubWItMzAge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMzBweDtcclxuICB9XHJcbiAgLm1iLTUwIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDUwcHg7XHJcbiAgfVxyXG4gIC5wci0yMCB7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gIH1cclxuICAuYmctd2hpdGUge1xyXG4gICAgYmFja2dyb3VuZDogI2ZmZjtcclxuICB9IiwiLyoqKioqKioqKioqKioqKioqKioqQ3VzdG9tIFZhcmlhYmxlcyAqKioqKioqKioqKioqKioqKioqKioqKioqKi9cbi50ZXh0LXdoaXRlIHtcbiAgY29sb3I6ICNmZmY7XG59XG5cbi5zZWNvbmRhcnktYmcge1xuICBiYWNrZ3JvdW5kOiAjMDAwO1xufVxuXG4ucHJpbWFyeS1jb2xvciB7XG4gIGNvbG9yOiAjYTdmMTA4O1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwO1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5tYXJnaW4tYXV0byB7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4uZm9udC1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDgwMDtcbn1cblxuLmZvbnQtc2VtaS1ib2xkIHtcbiAgZm9udC13ZWlnaHQ6IDYwMDtcbn1cblxuLm1iLTMwIHtcbiAgbWFyZ2luLWJvdHRvbTogMzBweDtcbn1cblxuLm1iLTUwIHtcbiAgbWFyZ2luLWJvdHRvbTogNTBweDtcbn1cblxuLnByLTIwIHtcbiAgcGFkZGluZy1yaWdodDogMjBweDtcbn1cblxuLmJnLXdoaXRlIHtcbiAgYmFja2dyb3VuZDogI2ZmZjtcbn1cblxuaGVhZGVyIHtcbiAgYmFja2dyb3VuZDogIzAwMDAwMDtcbn0iLCJAaW1wb3J0IFwiLi4vLi4vLi4vLi4vYXNzZXRzL2Nzcy9jb25zdGFudHMuc2Nzc1wiO1xyXG5oZWFkZXIge1xyXG4gICAgYmFja2dyb3VuZDogJGJsYWNrO1xyXG59Il19 */";
    /***/
  },

  /***/
  "./src/app/shared/organisms/header-organism/header-organism.component.ts":
  /*!*******************************************************************************!*\
    !*** ./src/app/shared/organisms/header-organism/header-organism.component.ts ***!
    \*******************************************************************************/

  /*! exports provided: HeaderOrganismComponent */

  /***/
  function srcAppSharedOrganismsHeaderOrganismHeaderOrganismComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HeaderOrganismComponent", function () {
      return HeaderOrganismComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../../services/common-utility-service */
    "./src/app/services/common-utility-service.ts");
    /* harmony import */


    var src_environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! src/environments/environment */
    "./src/environments/environment.ts");

    let HeaderOrganismComponent = class HeaderOrganismComponent {
      constructor(utilityService) {
        this.utilityService = utilityService;
      }

      ngOnInit() {
        const headerUrl = src_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].headerEndpoint;
        this.utilityService.getRequest(headerUrl, '').subscribe(data => {
          const response = JSON.parse(JSON.stringify(data));

          if (response && response.contentSlots && response.contentSlots.contentSlot) {
            var _iteratorNormalCompletion4 = true;
            var _didIteratorError4 = false;
            var _iteratorError4 = undefined;

            try {
              for (var _iterator4 = response.contentSlots.contentSlot[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
                const content = _step4.value;

                if (content.slotId === 'HeaderInfoSlot') {
                  this.headerLinks = content.components.component;
                } else if (content.slotId === 'SiteLogoSlot') {
                  this.logoUrl = content.components.component[0].media.url;
                } else if (content.slotId === 'NavigationBarSlot') {
                  this.headerNavLinks = content.components.component;
                }
              }
            } catch (err) {
              _didIteratorError4 = true;
              _iteratorError4 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion4 && _iterator4.return != null) {
                  _iterator4.return();
                }
              } finally {
                if (_didIteratorError4) {
                  throw _iteratorError4;
                }
              }
            }
          }
        });
      }

    };

    HeaderOrganismComponent.ctorParameters = () => [{
      type: _services_common_utility_service__WEBPACK_IMPORTED_MODULE_2__["CommonUtilityService"]
    }];

    HeaderOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-header-organism',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./header-organism.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/header-organism/header-organism.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./header-organism.component.scss */
      "./src/app/shared/organisms/header-organism/header-organism.component.scss")).default]
    })], HeaderOrganismComponent);
    /***/
  },

  /***/
  "./src/app/shared/organisms/home-organism/home-organism.component.scss":
  /*!*****************************************************************************!*\
    !*** ./src/app/shared/organisms/home-organism/home-organism.component.scss ***!
    \*****************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedOrganismsHomeOrganismHomeOrganismComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvaG9tZS1vcmdhbmlzbS9ob21lLW9yZ2FuaXNtLmNvbXBvbmVudC5zY3NzIn0= */";
    /***/
  },

  /***/
  "./src/app/shared/organisms/home-organism/home-organism.component.ts":
  /*!***************************************************************************!*\
    !*** ./src/app/shared/organisms/home-organism/home-organism.component.ts ***!
    \***************************************************************************/

  /*! exports provided: HomeOrganismComponent */

  /***/
  function srcAppSharedOrganismsHomeOrganismHomeOrganismComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "HomeOrganismComponent", function () {
      return HomeOrganismComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/router */
    "./node_modules/@angular/router/fesm2015/router.js");

    let HomeOrganismComponent = class HomeOrganismComponent {
      constructor(route) {
        this.route = route;
      }

      ngOnInit() {
        this.route.data.subscribe(data => {
          const response = data.homeData;

          if (response && response.contentSlots && response.contentSlots.contentSlot) {
            var _iteratorNormalCompletion5 = true;
            var _didIteratorError5 = false;
            var _iteratorError5 = undefined;

            try {
              for (var _iterator5 = response.contentSlots.contentSlot[Symbol.iterator](), _step5; !(_iteratorNormalCompletion5 = (_step5 = _iterator5.next()).done); _iteratorNormalCompletion5 = true) {
                const content = _step5.value;

                if (content.slotId === 'HomePageCarouselSlot') {
                  this.homeContent = content.components.component;
                }
              }
            } catch (err) {
              _didIteratorError5 = true;
              _iteratorError5 = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion5 && _iterator5.return != null) {
                  _iterator5.return();
                }
              } finally {
                if (_didIteratorError5) {
                  throw _iteratorError5;
                }
              }
            }
          }
        });
      }

    };

    HomeOrganismComponent.ctorParameters = () => [{
      type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"]
    }];

    HomeOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-home-organism',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./home-organism.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/home-organism/home-organism.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./home-organism.component.scss */
      "./src/app/shared/organisms/home-organism/home-organism.component.scss")).default]
    })], HomeOrganismComponent);
    /***/
  },

  /***/
  "./src/app/shared/organisms/login-organism/login-organism.component.scss":
  /*!*******************************************************************************!*\
    !*** ./src/app/shared/organisms/login-organism/login-organism.component.scss ***!
    \*******************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedOrganismsLoginOrganismLoginOrganismComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvbG9naW4tb3JnYW5pc20vbG9naW4tb3JnYW5pc20uY29tcG9uZW50LnNjc3MifQ== */";
    /***/
  },

  /***/
  "./src/app/shared/organisms/login-organism/login-organism.component.ts":
  /*!*****************************************************************************!*\
    !*** ./src/app/shared/organisms/login-organism/login-organism.component.ts ***!
    \*****************************************************************************/

  /*! exports provided: LoginOrganismComponent */

  /***/
  function srcAppSharedOrganismsLoginOrganismLoginOrganismComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "LoginOrganismComponent", function () {
      return LoginOrganismComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    let LoginOrganismComponent = class LoginOrganismComponent {
      constructor() {}

      ngOnInit() {}

    };
    LoginOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-login-organism',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./login-organism.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/login-organism/login-organism.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./login-organism.component.scss */
      "./src/app/shared/organisms/login-organism/login-organism.component.scss")).default]
    })], LoginOrganismComponent);
    /***/
  },

  /***/
  "./src/app/shared/organisms/not-found-organism/not-found-organism.component.scss":
  /*!***************************************************************************************!*\
    !*** ./src/app/shared/organisms/not-found-organism/not-found-organism.component.scss ***!
    \***************************************************************************************/

  /*! exports provided: default */

  /***/
  function srcAppSharedOrganismsNotFoundOrganismNotFoundOrganismComponentScss(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony default export */


    __webpack_exports__["default"] = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3NoYXJlZC9vcmdhbmlzbXMvbm90LWZvdW5kLW9yZ2FuaXNtL25vdC1mb3VuZC1vcmdhbmlzbS5jb21wb25lbnQuc2NzcyJ9 */";
    /***/
  },

  /***/
  "./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts":
  /*!*************************************************************************************!*\
    !*** ./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts ***!
    \*************************************************************************************/

  /*! exports provided: NotFoundOrganismComponent */

  /***/
  function srcAppSharedOrganismsNotFoundOrganismNotFoundOrganismComponentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "NotFoundOrganismComponent", function () {
      return NotFoundOrganismComponent;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");

    let NotFoundOrganismComponent = class NotFoundOrganismComponent {
      constructor() {}

      ngOnInit() {}

    };
    NotFoundOrganismComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
      selector: 'app-not-found-organism',
      template: tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! raw-loader!./not-found-organism.component.html */
      "./node_modules/raw-loader/dist/cjs.js!./src/app/shared/organisms/not-found-organism/not-found-organism.component.html")).default,
      styles: [tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"](__webpack_require__(
      /*! ./not-found-organism.component.scss */
      "./src/app/shared/organisms/not-found-organism/not-found-organism.component.scss")).default]
    })], NotFoundOrganismComponent);
    /***/
  },

  /***/
  "./src/app/shared/organisms/organisms.module.ts":
  /*!******************************************************!*\
    !*** ./src/app/shared/organisms/organisms.module.ts ***!
    \******************************************************/

  /*! exports provided: OrganismsModule */

  /***/
  function srcAppSharedOrganismsOrganismsModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "OrganismsModule", function () {
      return OrganismsModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _services_services_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! ../../services/services.module */
    "./src/app/services/services.module.ts");
    /* harmony import */


    var _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ../molecules/molecules.module */
    "./src/app/shared/molecules/molecules.module.ts");
    /* harmony import */


    var _directives_directives_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ../directives/directives.module */
    "./src/app/shared/directives/directives.module.ts");
    /* harmony import */


    var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ../pipes/pipes.module */
    "./src/app/shared/pipes/pipes.module.ts");
    /* harmony import */


    var _header_organism_header_organism_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./header-organism/header-organism.component */
    "./src/app/shared/organisms/header-organism/header-organism.component.ts");
    /* harmony import */


    var _footer_organism_footer_organism_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(
    /*! ./footer-organism/footer-organism.component */
    "./src/app/shared/organisms/footer-organism/footer-organism.component.ts");
    /* harmony import */


    var _home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(
    /*! ./home-organism/home-organism.component */
    "./src/app/shared/organisms/home-organism/home-organism.component.ts");
    /* harmony import */


    var _not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(
    /*! ./not-found-organism/not-found-organism.component */
    "./src/app/shared/organisms/not-found-organism/not-found-organism.component.ts");
    /* harmony import */


    var _login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(
    /*! ./login-organism/login-organism.component */
    "./src/app/shared/organisms/login-organism/login-organism.component.ts");

    let OrganismsModule = class OrganismsModule {};
    OrganismsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_services_services_module__WEBPACK_IMPORTED_MODULE_2__["ServicesModule"], _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_3__["MoleculesModule"], _directives_directives_module__WEBPACK_IMPORTED_MODULE_4__["DirectivesModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_5__["PipesModule"]],
      exports: [_header_organism_header_organism_component__WEBPACK_IMPORTED_MODULE_6__["HeaderOrganismComponent"], _footer_organism_footer_organism_component__WEBPACK_IMPORTED_MODULE_7__["FooterOrganismComponent"], _home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_8__["HomeOrganismComponent"], _login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_10__["LoginOrganismComponent"], _not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_9__["NotFoundOrganismComponent"]],
      declarations: [_header_organism_header_organism_component__WEBPACK_IMPORTED_MODULE_6__["HeaderOrganismComponent"], _footer_organism_footer_organism_component__WEBPACK_IMPORTED_MODULE_7__["FooterOrganismComponent"], _home_organism_home_organism_component__WEBPACK_IMPORTED_MODULE_8__["HomeOrganismComponent"], _not_found_organism_not_found_organism_component__WEBPACK_IMPORTED_MODULE_9__["NotFoundOrganismComponent"], _login_organism_login_organism_component__WEBPACK_IMPORTED_MODULE_10__["LoginOrganismComponent"]],
      providers: []
    })], OrganismsModule);
    /***/
  },

  /***/
  "./src/app/shared/pipes/pipes.module.ts":
  /*!**********************************************!*\
    !*** ./src/app/shared/pipes/pipes.module.ts ***!
    \**********************************************/

  /*! exports provided: PipesModule */

  /***/
  function srcAppSharedPipesPipesModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "PipesModule", function () {
      return PipesModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");

    let PipesModule = class PipesModule {};
    PipesModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
      exports: [],
      declarations: [],
      providers: []
    })], PipesModule);
    /***/
  },

  /***/
  "./src/app/shared/shared.module.ts":
  /*!*****************************************!*\
    !*** ./src/app/shared/shared.module.ts ***!
    \*****************************************/

  /*! exports provided: SharedModule */

  /***/
  function srcAppSharedSharedModuleTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "SharedModule", function () {
      return SharedModule;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/common */
    "./node_modules/@angular/common/fesm2015/common.js");
    /* harmony import */


    var _directives_directives_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./directives/directives.module */
    "./src/app/shared/directives/directives.module.ts");
    /* harmony import */


    var _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./molecules/molecules.module */
    "./src/app/shared/molecules/molecules.module.ts");
    /* harmony import */


    var _organisms_organisms_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(
    /*! ./organisms/organisms.module */
    "./src/app/shared/organisms/organisms.module.ts");
    /* harmony import */


    var _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(
    /*! ./pipes/pipes.module */
    "./src/app/shared/pipes/pipes.module.ts");

    let SharedModule = class SharedModule {};
    SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
      imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"]],
      exports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _molecules_molecules_module__WEBPACK_IMPORTED_MODULE_4__["MoleculesModule"], _organisms_organisms_module__WEBPACK_IMPORTED_MODULE_5__["OrganismsModule"], _pipes_pipes_module__WEBPACK_IMPORTED_MODULE_6__["PipesModule"], _directives_directives_module__WEBPACK_IMPORTED_MODULE_3__["DirectivesModule"]],
      declarations: []
    })], SharedModule);
    /***/
  },

  /***/
  "./src/environments/environment.ts":
  /*!*****************************************!*\
    !*** ./src/environments/environment.ts ***!
    \*****************************************/

  /*! exports provided: environment */

  /***/
  function srcEnvironmentsEnvironmentTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony export (binding) */


    __webpack_require__.d(__webpack_exports__, "environment", function () {
      return environment;
    });
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js"); // This file can be replaced during build by using the `fileReplacements` array.
    // `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
    // The list of file replacements can be found in `angular.json`.


    const environment = {
      production: false,
      hostName: 'https://api.c050ygx6-obeikanin2-d1-public.model-t.cc.commerce.ondemand.com/',
      headerEndpoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=headerpage',
      logoUrl: 'medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aDUyL2gxNi84Nzk2NzE3MjUyNjM4L3NpdGUtbG9nby5wbmd8MTJiMzZkYjBlZDNkMTVjMWMwOGMyMTU0NTlkZGI3NjE3NGU3MGFkZmVhNjg2M2VkY2E2ZGRkODA5MDZmNjMyOQ',
      homePageEndPoint: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=homepage',
      oAuthAPI: 'authorizationserver/oauth/token',
      registerUserAPI: 'osanedcommercewebservices/v2/osaned/users',
      footerEndPoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=footerPage',
      tokenEndpoint: 'authorizationserver/oauth/token',
      loginEndpoint: 'osanedcommercewebservices/v2/osaned/users/'
    };
    /*
     * For easier debugging in development mode, you can import the following file
     * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
     *
     * This import should be commented out in production mode because it will have a negative impact
     * on performance if an error is thrown.
     */
    // import 'zone.js/dist/zone-error';  // Included with Angular CLI.

    /***/
  },

  /***/
  "./src/main.ts":
  /*!*********************!*\
    !*** ./src/main.ts ***!
    \*********************/

  /*! no exports provided */

  /***/
  function srcMainTs(module, __webpack_exports__, __webpack_require__) {
    "use strict";

    __webpack_require__.r(__webpack_exports__);
    /* harmony import */


    var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(
    /*! tslib */
    "./node_modules/tslib/tslib.es6.js");
    /* harmony import */


    var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(
    /*! @angular/core */
    "./node_modules/@angular/core/fesm2015/core.js");
    /* harmony import */


    var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(
    /*! @angular/platform-browser-dynamic */
    "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
    /* harmony import */


    var _app_app_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(
    /*! ./app/app.module */
    "./src/app/app.module.ts");
    /* harmony import */


    var _environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(
    /*! ./environments/environment */
    "./src/environments/environment.ts");

    if (_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].production) {
      Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
    }

    document.addEventListener('DOMContentLoaded', () => {
      Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_3__["AppModule"]).catch(err => console.error(err));
    });
    /***/
  },

  /***/
  0:
  /*!***************************!*\
    !*** multi ./src/main.ts ***!
    \***************************/

  /*! no static exports found */

  /***/
  function _(module, exports, __webpack_require__) {
    module.exports = __webpack_require__(
    /*! C:\deploy_v1.0\js-storefront\spartacusstore\src\main.ts */
    "./src/main.ts");
    /***/
  }
}, [[0, "runtime", "vendor"]]]);
//# sourceMappingURL=main-es5.js.map