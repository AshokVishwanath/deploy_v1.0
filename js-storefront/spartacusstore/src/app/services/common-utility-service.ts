import { Injectable, ElementRef, Renderer2, PLATFORM_ID, Inject } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { TransferState, makeStateKey } from '@angular/platform-browser';
import { environment } from '../../environments/environment';

/**
 * Utility service for the application
 * Common utility functions that can be used throughout the application 
 * @author Mohamed Omar Farook
 */
@Injectable()
export class CommonUtilityService {

    transferedState: any;

    constructor(
        @Inject(PLATFORM_ID) private plateformId: object,
        private http: HttpClient,
        private state: TransferState) { }

    /**
     * @param input object to be validated
     * @returns true if object is undefined or empty, otherwise false
     */
    isUndefinedOrEmpty(input: any) {
        if (undefined !== input && '' !== input) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * @param input object to be validated
     * @returns true if object is undefined or null, otherwise false
     */
    isNullOrUndefined(input: any) {
        if (undefined !== input && null !== input) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Checks if application is running in browser
     */
    isBrowser(): boolean {
        if (isPlatformBrowser(this.plateformId)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Endpoint url
     * @returns Get request response from server
     */
    getRequest(url: any, header) {
        let api = this.getApi(url)

        return this.http.get(api, header)
    }

    /**
     * @param Endpoint url
     * @returns Post request response from server
     */
    postRequest(url, request, options) {
        let api = this.getApi(url)         

        return this.http.post(api, request, options)
    }

    /**
     * @param url
     * @returns generated endpoint url
     */
    getApi(url) {
        if(url === '../../assets/mockApi/home.json') {
            return url
        }
        return environment.hostName + url
    }

    getAuth() {
        const httpHeaders = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded;'});
        const url = environment.hostName + environment.oAuthAPI;
        const request  = new HttpParams()
        .set('client_id', 'trusted_client')
        .set('client_secret', 'secret')
        .set('grant_type', 'client_credentials')
        .set('scope', 'extended');
        return this.http.post(url, request.toString(), {headers: httpHeaders});
       }
}

