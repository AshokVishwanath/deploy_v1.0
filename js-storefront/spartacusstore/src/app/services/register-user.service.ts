import { Injectable } from '@angular/core';
import { HttpClient , HttpHeaders , HttpParams} from '@angular/common/http';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterUserService {
  constructor(private http: HttpClient) {
   }
   registerUserDetails(token, req) {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded;',
      'Authorization': token
    });
    const request  = new HttpParams()
    .set('name', req.name)
    .set('email', req.email)
    .set('contact', req.mobile)
    .set('company', req.company)
    .set('sizeRange', req.size)
    .set('commercialRegistration', req.commercial);
    const url = environment.hostName + environment.registerUserAPI;
    return this.http.post(url, request.toString(), {headers: httpHeaders});
   }
}
