import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-cx-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss'],
})
export class SignUpComponent implements OnInit {

  clientLogos: any;
  hostName: string = environment.hostName;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
  }
}
