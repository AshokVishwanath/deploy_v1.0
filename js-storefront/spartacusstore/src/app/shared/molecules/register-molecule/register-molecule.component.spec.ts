import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterMoleculeComponent } from './register-molecule.component';

describe('RegisterMoleculeComponent', () => {
  let component: RegisterMoleculeComponent;
  let fixture: ComponentFixture<RegisterMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
