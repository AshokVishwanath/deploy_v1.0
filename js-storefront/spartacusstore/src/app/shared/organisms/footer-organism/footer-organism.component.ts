import { Component, OnInit } from '@angular/core';
import { CommonUtilityService } from '../../../services/common-utility-service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-footer-organism',
  templateUrl: './footer-organism.component.html',
  styleUrls: ['./footer-organism.component.scss']
})
export class FooterOrganismComponent implements OnInit {
  footerColumnSlot: Array<any> = [];
  copyRightSlot: Array<any> = [];
  constructor(private utilityService: CommonUtilityService) { }

  ngOnInit() {
    const footerUrl = environment.footerEndPoint;
    this.utilityService.getRequest(footerUrl,'').subscribe(data => {
      const response = JSON.parse(JSON.stringify(data));
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'CopyRightsSlot') {
            this.copyRightSlot = content.components.component[0];
          }
        }
        this.footerColumnSlot = response.contentSlots.contentSlot.filter(each => each.slotId !== "CopyRightsSlot");
      }
    });
  }

}
